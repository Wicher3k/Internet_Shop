const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http');
const app = express();
var bcrypt = require('bcrypt');
var fs = require("fs");
var jwt = require('jsonwebtoken');
var dir = 'img/';

// var cookieParser = require('cookie-parser');

// API file for interacting with MongoDB
const api = require('./server/routes/api');

// Parsers
app.use(express.static('public'));
// app.use(bodyParser.urlencoded({ extended: false}));
// app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({
    parameterLimit: 100000,
    limit: '50mb',
    extended: true
  }));
app.use(bodyParser.json({limit: '50mb'}));

// Angular DIST output folder
app.use(express.static(path.join(__dirname, 'dist'))); // ppolaczenie do dista.

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});


app.get('/products', function (req, res) {
    ProductModel.find({}).select("name description price category imageExist").exec(function(err, products){
        if(err) return res.end(JSON.stringify(err));
        res.end( JSON.stringify(products) );
    });
     
})

app.get('/categories', function (req, res) {
    CategoryModel.find(function(err,categories){
        if(err) return res.end(JSON.stringify(err));
        res.end( JSON.stringify(categories) );
    });
})

app.post('/orders', function(req,res){
    var order = new OrdersModel(req.body);
    order.save(function(err) {
        if (err) return res.end(JSON.stringify(err));
    });
    res.end(JSON.stringify('True'));
})

app.get('/products/photo/:productId', function (req, res) {
    ProductModel.findById(req.params.productId).select("image").exec(function(err, products){
        if(err) return res.end(JSON.stringify(err));
        res.end( JSON.stringify(products) );
    });
})

/////// weryfikacja tokenu
const apiRoutes = require('./server/middleware/administrator');

app.use('/orders', apiRoutes);
app.use('/products', apiRoutes);
/////

app.get('/orderstatuses', function (req, res) {
    OrderStatusModel.find(function(err,statuses){
        if(err) return res.end(JSON.stringify(err));
        res.end( JSON.stringify(statuses) );
    });
})

app.get('/orders', function (req, res) {
    OrdersModel.find(function(err,orders){
        if(err) return res.end(JSON.stringify(err));
        res.end( JSON.stringify(orders) );
    });
})

app.get('/orders/user/:userId', function (req, res) {
    OrdersModel.find({userId:req.params.userId},function(err,orders){
        if(err) return res.end(JSON.stringify(err));
        res.end( JSON.stringify(orders) );
    });
})

app.get('/products/:productId', function (req, res) {
    ProductModel.findById(req.params.productId,function(err,products){
        if(err) return res.end(JSON.stringify(err));
        res.end( JSON.stringify(products) );
    });
})


app.post('/products',function(req,res){
    var product = new ProductModel(req.body);
    product.save(function(err,product) {
        if (err) {
            return res.end(JSON.stringify(err));
        }
        else{
            res.end(JSON.stringify('True'));
        }
    });
})

app.delete('/products/:productId',function(req,res){
    ProductModel.findByIdAndRemove(req.params.productId,function(err){
        if(err) return res.end(JSON.stringify(err));
        io.emit('deleteProduct',{ massage: req.params.productId });
        removeSaleIfExists(req.params.productId);
        res.end(JSON.stringify('True'));
    });
})

app.put('/products/:productId',function(req,res){
    var productPUT = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        category: req.body.category,
        imageExist: req.body.imageExist,
        image: req.body.image,
    }
    ProductModel.findByIdAndUpdate(req.params.productId,productPUT,function(err,product){
        if(err) return res.end(JSON.stringify(err));
        else{
            if(req.body.sale){
                addSale(req.params.productId,req.body.optionPromotion);
            }else{
                removeSaleIfExists(req.params.productId);
            }
            res.end(JSON.stringify('True'));
        }
    });
})

app.put('/orders/:orderId',function(req,res){
    var statusPUT = {
        status: req.body.status
    }
    OrdersModel.findByIdAndUpdate(req.params.orderId,statusPUT,function(err,order){
        if(err) return res.end(JSON.stringify(err));
        else{
            res.end(JSON.stringify('True'));
        }
    });
})

app.post('/users',function(req,res){
    var user = new UsersModel(req.body);
    user.save(function(err) {
        if (err) return res.end(JSON.stringify(err));
        else res.end(JSON.stringify('True'));
    });
});

app.put('/users/basket/:userId',function(req,res){
    var basketPUT = {
        basketSaved: req.body.basketSaved
    }
    UsersModel.findByIdAndUpdate(req.params.userId,basketPUT,function(err,user){
        if(err) return res.end(JSON.stringify(err));
        else{
            res.end(JSON.stringify('True'));
           
        }
    });

});

app.post('/users/login',function(req,res){ 
    UsersModel.findOne({ username: req.body.username }, function(err, user) {
        if (err) throw err;
        if(user==null){
            res.sendStatus(401);
        }else{
            user.comparePassword(req.body.password, function(err, isMatch) {
                if (err) throw err;
                if(isMatch){
                    const payload = {
                        admin: user.admin 
                      };
                    var token = jwt.sign(payload,'tokentokentoken', {
                        expiresIn: "24h" // token na 24h
                      });
                    res.json({
                        success: true,
                        token: token,
                        function: user.function,
                        basketSaved:user.basketSaved,
                        userId:user.id
                      });
                }else{
                    res.sendStatus(401);
                }
            });
        }
    });
})

app.post('/users/logout',function(req,res){
    res.end(JSON.stringify('True' ));
});


//Set Port

const port = process.env.PORT || '3000';
app.set('port', port);

const server = http.createServer(app);
server.listen(port, () => console.log(`Running on localhost:${port}`));


//sockety
const io = require('socket.io')(server);

function endSaleFunc(productId) {
    return function() {
        io.emit('salesEnd' , { promocja: productId });
    }
}

function addSale(productId,optionPromotion){
    salesArray.push(productId);
    optionPromotionArray.push(optionPromotion.saleValue/100);
    io.emit('sales', { promocja: [productId], saleValue: [optionPromotion.saleValue/100] });
    timeoutsArray[productId] = setTimeout(endSaleFunc(productId), optionPromotion.saleTime*1000);
    
}

function removeSaleIfExists(productId){
    var indexSale = salesArray.indexOf(productId)
    if ( indexSale > -1) {
        salesArray.splice(indexSale, 1);
        optionPromotionArray.splice(indexSale, 1);
        clearTimeout(timeoutsArray[productId])
        io.emit('salesEnd', { promocja: productId });
    }
}

var timeoutsArray=[];
var salesArray=[];
var optionPromotionArray=[];

io.on('connection' , function(client) {
    //console.log('Client connected...');
    client.emit('sales' , { promocja: salesArray, saleValue: optionPromotionArray});
    client.on('disconnect', function () {
        //console.log('A user disconnected');
    });
});

