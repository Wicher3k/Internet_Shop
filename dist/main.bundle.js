webpackJsonp(["main"],{

/***/ "../../../../../src/$$_gendir lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_gendir lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var forms_1 = __webpack_require__("../../../forms/@angular/forms.es5.js");
var http_1 = __webpack_require__("../../../http/@angular/http.es5.js");
var ng2_nouislider_1 = __webpack_require__("../../../../ng2-nouislider/src/nouislider.js");
var index_1 = __webpack_require__("../../../../angular-4-data-table/src/index.ts");
var http_2 = __webpack_require__("../../../common/@angular/common/http.es5.js");
var sklepInternetowyMain_component_1 = __webpack_require__("../../../../../src/app/sklepInternetowyMain.component.ts");
var header_component_1 = __webpack_require__("../../../../../src/app/skeleton/header/header.component.ts");
var filter_bar_component_1 = __webpack_require__("../../../../../src/app/skeleton/filter-bar/filter-bar.component.ts");
var shop_center_component_1 = __webpack_require__("../../../../../src/app/skeleton/shop-center/shop-center.component.ts");
var nav_bar_component_1 = __webpack_require__("../../../../../src/app/skeleton/nav-bar/nav-bar.component.ts");
var item_component_1 = __webpack_require__("../../../../../src/app/skeleton/shop-center/item/item.component.ts");
var pagination_component_component_1 = __webpack_require__("../../../../../src/app/skeleton/shop-center/pagination-component/pagination-component.component.ts");
var item_service_1 = __webpack_require__("../../../../../src/app/services/item.service.ts");
var categoryservice_service_1 = __webpack_require__("../../../../../src/app/services/categoryservice.service.ts");
var basket_service_1 = __webpack_require__("../../../../../src/app/services/basket.service.ts");
var user_service_1 = __webpack_require__("../../../../../src/app/services/user.service.ts");
var basket_center_component_1 = __webpack_require__("../../../../../src/app/skeleton/basket-center/basket-center.component.ts");
var order_center_component_1 = __webpack_require__("../../../../../src/app/skeleton/order-center/order-center.component.ts");
var account_component_1 = __webpack_require__("../../../../../src/app/skeleton/account/account.component.ts");
var admin_start_view_component_1 = __webpack_require__("../../../../../src/app/skeleton/account/admin-start-view/admin-start-view.component.ts");
var orders_component_1 = __webpack_require__("../../../../../src/app/skeleton/account/admin-start-view/orders/orders.component.ts");
var products_component_1 = __webpack_require__("../../../../../src/app/skeleton/account/admin-start-view/products/products.component.ts");
var products_edit_component_1 = __webpack_require__("../../../../../src/app/skeleton/account/admin-start-view/products/products-edit/products-edit.component.ts");
var account_register_component_1 = __webpack_require__("../../../../../src/app/skeleton/account/account-register/account-register.component.ts");
var user_start_view_component_1 = __webpack_require__("../../../../../src/app/skeleton/account/user-start-view/user-start-view.component.ts");
var user_orders_component_1 = __webpack_require__("../../../../../src/app/skeleton/account/user-start-view/user-orders/user-orders.component.ts");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        declarations: [
            sklepInternetowyMain_component_1.SklepInternetowyMainComponent,
            header_component_1.HeaderComponent,
            filter_bar_component_1.FilterBarComponent,
            shop_center_component_1.ShopCenterComponent,
            nav_bar_component_1.NavBarComponent,
            item_component_1.ItemComponent,
            pagination_component_component_1.PaginationComponentComponent,
            basket_center_component_1.BasketCenterComponent,
            order_center_component_1.OrderCenterComponent,
            account_component_1.AccountComponent,
            admin_start_view_component_1.AdminStartViewComponent,
            orders_component_1.OrdersComponent,
            products_component_1.ProductsComponent,
            products_edit_component_1.ProductsEditComponent,
            account_register_component_1.AccountRegisterComponent,
            user_start_view_component_1.UserStartViewComponent,
            user_orders_component_1.UserOrdersComponent,
        ],
        imports: [
            platform_browser_1.BrowserModule,
            forms_1.FormsModule,
            http_1.HttpModule,
            ng2_nouislider_1.NouisliderModule,
            index_1.DataTableModule,
            http_2.HttpClientModule
        ],
        providers: [item_service_1.ItemService, categoryservice_service_1.CategoryserviceService, basket_service_1.BasketService, user_service_1.UserService],
        bootstrap: [sklepInternetowyMain_component_1.SklepInternetowyMainComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/services/basket.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var Subject_1 = __webpack_require__("../../../../rxjs/_esm5/Subject.js");
var BasketService = (function () {
    function BasketService() {
        this.caseNumberBasket = new Subject_1.Subject();
        this.caseNumberBasket$ = this.caseNumberBasket.asObservable();
    }
    BasketService.prototype.publishData = function (data, option) {
        if (option === void 0) { option = 0; }
        this.caseNumberBasket.next([data, option]);
    };
    return BasketService;
}());
BasketService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [])
], BasketService);
exports.BasketService = BasketService;
//# sourceMappingURL=basket.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/categoryservice.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var Subject_1 = __webpack_require__("../../../../rxjs/_esm5/Subject.js");
var http_1 = __webpack_require__("../../../common/@angular/common/http.es5.js");
var CategoryserviceService = (function () {
    function CategoryserviceService(http) {
        this.http = http;
        this.caseNumber = new Subject_1.Subject();
        this.caseFilterString = new Subject_1.Subject();
        this.caseFilterPrice = new Subject_1.Subject();
        this.caseNumber$ = this.caseNumber.asObservable();
        this.caseFilterString$ = this.caseFilterString.asObservable();
        this.caseFilterPrice$ = this.caseFilterPrice.asObservable();
        this.categories = [];
        this.categoriesGroups = [];
        this.categoriesGroupsNames = [];
        this.baseUrlCategories = "/categories";
        this.getCategories();
    }
    CategoryserviceService.prototype.publishData = function (data) {
        this.caseNumber.next(data);
    };
    CategoryserviceService.prototype.publishDataCategoryString = function (data) {
        this.caseFilterString.next(data);
    };
    CategoryserviceService.prototype.publishFilterPrice = function (data) {
        this.caseFilterPrice.next(data);
    };
    CategoryserviceService.prototype.getCategories = function () {
        var _this = this;
        this.http.get(this.baseUrlCategories, { withCredentials: true }).subscribe(function (res) {
            for (var i in res) {
                if (res[i]['group'] == undefined) {
                    _this.categories.push(res[i]['name']);
                }
                else {
                    var index = _this.categoriesGroupsNames.indexOf(res[i]['group']);
                    if (index > -1) {
                        _this.categoriesGroups[index].push(res[i]['name']);
                    }
                    else {
                        _this.categoriesGroupsNames.push(res[i]['group']);
                        _this.categoriesGroups.push([res[i]['name']]);
                    }
                }
            }
        }, function (error) {
            console.log(error);
        });
    };
    return CategoryserviceService;
}());
CategoryserviceService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof http_1.HttpClient !== "undefined" && http_1.HttpClient) === "function" && _a || Object])
], CategoryserviceService);
exports.CategoryserviceService = CategoryserviceService;
var _a;
//# sourceMappingURL=categoryservice.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/item.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var itemClass_1 = __webpack_require__("../../../../../src/app/skeleton/shop-center/item/itemClass.ts");
var user_service_1 = __webpack_require__("../../../../../src/app/services/user.service.ts");
var http_1 = __webpack_require__("../../../http/@angular/http.es5.js");
var Subject_1 = __webpack_require__("../../../../rxjs/_esm5/Subject.js");
__webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
var http_2 = __webpack_require__("../../../common/@angular/common/http.es5.js");
var ItemService = (function () {
    function ItemService(http, http2, userService) {
        this.http = http;
        this.http2 = http2;
        this.userService = userService;
        this.promocjaProduct = new itemClass_1.Item();
        this.salesIdArray = [];
        this.items = [];
        this.itemsReturn = [];
        this.countItems = 0;
        this.baseUrlProducts = '/products';
        this.baseUrlOrders = '/orders';
        this.baseUrlAuthentication = '/users/login';
        this.baseUrlOrderStatuses = '/orderstatuses';
        this.endInitialization = new Subject_1.Subject();
        this.endInitialization$ = this.endInitialization.asObservable();
        this.sale = new Subject_1.Subject();
        this.sale$ = this.sale.asObservable();
        this.loadedbasket = new Subject_1.Subject();
        this.loadedbasket$ = this.loadedbasket.asObservable();
        this.socket = io.connect('http://localhost:3000');
        //var socket = io.connect('https://peaceful-ocean-60282.herokuapp.com' );
        this.getProductJsonAndSetItems(true);
        this.anotherSockets();
    }
    ;
    ItemService.prototype.getProductJsonAndSetItems = function (connectServer) {
        var _this = this;
        if (connectServer === void 0) { connectServer = false; }
        this.items = [];
        this.http.get(this.baseUrlProducts).map(function (res) { return res.json(); }).subscribe(function (res) {
            for (var _i = 0, res_1 = res; _i < res_1.length; _i++) {
                var row = res_1[_i];
                _this.items.push(new itemClass_1.Item(row['_id'], row['name'], row['description'], row['price'], row['category'], 0, row['imageExist'], false));
            }
            _this.endInitialization.next();
            if (connectServer = true) {
                var klasa = _this;
                _this.socket.on('sales', function (data) {
                    for (var k in data.promocja) {
                        for (var i in klasa.items) {
                            if (data.promocja[k] == klasa.items[i].id) {
                                var j = parseInt(i);
                                klasa.items[j].priceOld = klasa.items[j].price;
                                klasa.items[j].price = klasa.items[j].price * (1 - data.saleValue[k]);
                                klasa.items[j].sale = true;
                                klasa.promocjaProduct = klasa.items[j];
                                klasa.salesIdArray.push(data.promocja[k]);
                                break;
                            }
                        }
                    }
                    klasa.sale.next(klasa.promocjaProduct);
                });
                _this.socket.on('salesEnd', function (data) {
                    for (var i in klasa.items) {
                        if (data.promocja == klasa.items[i].id) {
                            var j = parseInt(i);
                            klasa.items[j].sale = false;
                            klasa.items[j].price = klasa.items[j].priceOld;
                            break;
                        }
                    }
                    var index = klasa.salesIdArray.indexOf(data.promocja);
                    if (index > -1) {
                        klasa.salesIdArray.splice(index, 1);
                    }
                    if (klasa.salesIdArray.length < 1) {
                        klasa.promocjaProduct = new itemClass_1.Item();
                    }
                    else {
                        klasa.promocjaProduct = klasa.searchProductByid(klasa.salesIdArray[klasa.salesIdArray.length - 1]);
                    }
                    klasa.sale.next(klasa.promocjaProduct);
                });
            }
        }, function (error) {
            _this.endInitialization.next("error : " + error);
        });
    };
    ItemService.prototype.searchProductByid = function (idProduct) {
        for (var i in this.items) {
            if (this.items[i].id == idProduct) {
                return this.items[i];
            }
        }
        return new itemClass_1.Item();
    };
    ItemService.prototype.ifProductSale = function (idProduct) {
        var index = this.salesIdArray.indexOf(idProduct);
        if (index > -1) {
            return true;
        }
        else {
            return false;
        }
    };
    ItemService.prototype.getItemsService = function (option) {
        var itemsfiltred = this.filterItems(option['filter'], option['filterRe'], option['filterPrice']);
        this.itemsReturn = [];
        for (var i = 0; i < option['limit']; i++) {
            if (i + (option['page'] - 1) * option['limit'] < this.countItems)
                this.itemsReturn.push(itemsfiltred[i + (option['page'] - 1) * option['limit']]);
        }
        return { itemsObjects: this.itemsReturn, total: this.countItems };
    };
    ItemService.prototype.getBasketService = function () {
        var itemsReturn = [];
        for (var _i = 0, _a = this.items; _i < _a.length; _i++) {
            var item = _a[_i];
            if (item.order > 0) {
                itemsReturn.push(item);
            }
        }
        return itemsReturn;
    };
    ItemService.prototype.getBasket2Save = function () {
        var itemsReturn = {};
        for (var _i = 0, _a = this.items; _i < _a.length; _i++) {
            var item = _a[_i];
            if (item.order > 0) {
                itemsReturn[item.id] = item.order;
            }
        }
        return itemsReturn;
    };
    ItemService.prototype.filterItems = function (filtersCategoryChecked, filterRe, filterPrice) {
        this.countItems = 0;
        var itemsfiltred = [], itemsfiltredReturn = [];
        if (filtersCategoryChecked.length > 0) {
            for (var _i = 0, filtersCategoryChecked_1 = filtersCategoryChecked; _i < filtersCategoryChecked_1.length; _i++) {
                var filter = filtersCategoryChecked_1[_i];
                for (var _a = 0, _b = this.items; _a < _b.length; _a++) {
                    var item = _b[_a];
                    if (filtersCategoryChecked.length > 0 && item.category == filter && itemsfiltred.indexOf(item) < 0) {
                        itemsfiltred.push(item);
                    }
                }
            }
        }
        else {
            itemsfiltred = this.items;
        }
        if (filterRe.length > 0) {
            var regexp = new RegExp('.*' + filterRe.toLowerCase() + '.*');
            for (var _c = 0, itemsfiltred_1 = itemsfiltred; _c < itemsfiltred_1.length; _c++) {
                var item = itemsfiltred_1[_c];
                if (regexp.test(item.name.toLowerCase())) {
                    itemsfiltredReturn.push(item);
                }
            }
        }
        else {
            itemsfiltredReturn = itemsfiltred;
        }
        itemsfiltred = itemsfiltredReturn;
        itemsfiltredReturn = [];
        for (var _d = 0, itemsfiltred_2 = itemsfiltred; _d < itemsfiltred_2.length; _d++) {
            var item = itemsfiltred_2[_d];
            if (item.price >= filterPrice[0] && item.price <= filterPrice[1]) {
                itemsfiltredReturn.push(item);
            }
        }
        this.countItems = itemsfiltredReturn.length;
        return itemsfiltredReturn;
    };
    ItemService.prototype.getPromotion = function () {
        return this.promocjaProduct;
    };
    ItemService.prototype.anotherSockets = function () {
        var klasa = this;
        this.socket.on('deleteProduct', function (idProd) {
            klasa.deleteProductView(idProd);
            klasa.reloadSiteHeader();
            klasa.endInitialization.next();
        });
    };
    ItemService.prototype.resetShopContent = function () {
        this.getProductJsonAndSetItems();
    };
    ItemService.prototype.setBasket = function (basket) {
        for (var id in basket) {
            for (var _i = 0, _a = this.items; _i < _a.length; _i++) {
                var item = _a[_i];
                if (id == item.id) {
                    item.order = basket[id];
                    break;
                }
            }
        }
        this.loadedbasket.next();
    };
    ItemService.prototype.reloadSiteHeader = function () {
        this.loadedbasket.next();
    };
    ItemService.prototype.getHeader = function () {
        var token = this.userService.getToken();
        return new http_2.HttpHeaders({ 'x-access-token': token });
    };
    ItemService.prototype.deleteProductView = function (idProd) {
        for (var i in this.items) {
            if (this.items[i].id == idProd.massage) {
                this.items.splice(parseInt(i), 1);
                break;
            }
        }
    };
    //CRUD
    //orders
    ItemService.prototype.getOrders = function () {
        var myHeaders = this.getHeader();
        return this.http2.get(this.baseUrlOrders, { headers: myHeaders, withCredentials: true });
    };
    ItemService.prototype.getUserOrders = function () {
        var myHeaders = this.getHeader();
        var userLoggedId = this.userService.getUserLoggedId();
        return this.http2.get(this.baseUrlOrders + '/user/' + userLoggedId, { headers: myHeaders, withCredentials: true });
    };
    ItemService.prototype.sendOrder = function (model, itemsSum, itemsOrder) {
        var userLoggedId = this.userService.getUserLoggedId();
        var d = new Date, dformat = [d.getMonth() + 1,
            d.getDate(),
            d.getFullYear()].join('/') + ' ' +
            [d.getHours(),
                d.getMinutes(),
                d.getSeconds()].join(':');
        return this.http2.post(this.baseUrlOrders, { "name": model.name, "street": model.street, "sumPrice": itemsSum, "orderItems": itemsOrder, "userId": userLoggedId, "dateOrder": dformat, "status": 0 }, { withCredentials: true });
    };
    ItemService.prototype.putOrder = function (id, statusId) {
        var myHeaders = this.getHeader();
        return this.http2.put(this.baseUrlOrders + '/' + id, { "status": statusId,
        }, { headers: myHeaders, withCredentials: true });
    };
    ItemService.prototype.getOrderStatuses = function () {
        var myHeaders = this.getHeader();
        return this.http2.get(this.baseUrlOrderStatuses, { headers: myHeaders, withCredentials: true });
    };
    // products
    ItemService.prototype.getProducts = function () {
        return this.http.get(this.baseUrlProducts).map(function (res) { return res.json(); });
    };
    ItemService.prototype.deleteProduct = function (id) {
        var myHeaders = this.getHeader();
        return this.http2.delete(this.baseUrlProducts + '/' + id, { headers: myHeaders, withCredentials: true });
    };
    ItemService.prototype.addOrEditProduct = function (product, option, image) {
        if (option == 'Dodaj') {
            return this.addProduct(product, image);
        }
        else if (option == 'Edytuj') {
            return this.editProduct(product, image);
        }
    };
    ItemService.prototype.addProduct = function (product, image) {
        if (image != undefined) {
            var imageExist = true;
        }
        var myHeaders = this.getHeader();
        myHeaders.append('Content-Type', 'multipart/form-data');
        return this.http2.post(this.baseUrlProducts, { "name": product.name,
            "description": product.description,
            "price": product.price,
            "category": product.category,
            "imageExist": imageExist,
            "image": image,
            "sale": product.sale,
            "optionPromotion": { saleTime: product.saleTime, saleValue: product.saleValue } }, { headers: myHeaders, withCredentials: true });
    };
    ItemService.prototype.editProduct = function (product, image) {
        if (image != undefined) {
            var imageExist = true;
        }
        var myHeaders = this.getHeader();
        return this.http2.put(this.baseUrlProducts + '/' + product.id, { "name": product.name,
            "description": product.description,
            "price": product.price,
            "category": product.category,
            "imageExist": imageExist,
            "image": image,
            "sale": product.sale,
            "optionPromotion": { saleTime: product.saleTime, saleValue: product.saleValue } }, { headers: myHeaders, withCredentials: true });
    };
    ItemService.prototype.getPhoto = function (idProduct) {
        var myHeaders = this.getHeader();
        return this.http2.get(this.baseUrlProducts + '/photo/' + idProduct, { headers: myHeaders, withCredentials: true });
    };
    ItemService.prototype.getItemsFromId = function (id) {
        var myHeaders = this.getHeader();
        return this.http2.get(this.baseUrlProducts + '/' + id, { headers: myHeaders, withCredentials: true });
    };
    return ItemService;
}());
ItemService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _a || Object, typeof (_b = typeof http_2.HttpClient !== "undefined" && http_2.HttpClient) === "function" && _b || Object, typeof (_c = typeof user_service_1.UserService !== "undefined" && user_service_1.UserService) === "function" && _c || Object])
], ItemService);
exports.ItemService = ItemService;
var UserAuthenticate = (function () {
    function UserAuthenticate(username, password) {
        this.username = username;
        this.password = password;
    }
    return UserAuthenticate;
}());
exports.UserAuthenticate = UserAuthenticate;
var _a, _b, _c;
//# sourceMappingURL=item.service.js.map

/***/ }),

/***/ "../../../../../src/app/services/user.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var http_1 = __webpack_require__("../../../common/@angular/common/http.es5.js");
var UserService = (function () {
    function UserService(http) {
        this.http = http;
        this.userLogged = new ModelAuthenticate();
        this.token = "";
        this.baseUrlUser = '/users';
        this.baseUrlAuthentication = '/users/login';
        this.baseUrlLogout = '/users/logout';
    }
    UserService.prototype.setUser = function (model, functionAccount, token, userId) {
        this.userLogged = model;
        this.userLogged.userId = userId;
        this.userLogged.functionAccount = functionAccount;
        this.token = token;
    };
    UserService.prototype.register = function (userModel) {
        return this.http.post(this.baseUrlUser, { "username": userModel.name, "password": userModel.password, 'function': 1 }, { withCredentials: true });
    };
    UserService.prototype.authenticate = function (userModel) {
        return this.http.post(this.baseUrlAuthentication, { "username": userModel.username, "password": userModel.password }, { withCredentials: true });
    };
    UserService.prototype.logout = function () {
        this.token = '';
        this.userLogged = new ModelAuthenticate();
        return this.http.post(this.baseUrlLogout, '', { withCredentials: true });
    };
    UserService.prototype.getHeader = function () {
        var token = this.token;
        return new http_1.HttpHeaders({ 'x-access-token': token });
    };
    UserService.prototype.getToken = function () {
        return this.token;
    };
    UserService.prototype.getUser = function () {
        return this.userLogged;
    };
    UserService.prototype.getUserLoggedId = function () {
        return this.userLogged.userId;
    };
    UserService.prototype.ifUserLogged = function () {
        return this.userLogged.userId != '';
    };
    UserService.prototype.saveBasket = function (basket) {
        var myHeaders = this.getHeader();
        this.http.put(this.baseUrlUser + '/basket/' + this.userLogged.userId, { "basketSaved": basket,
        }, { headers: myHeaders, withCredentials: true }).subscribe(function (res) { });
    };
    return UserService;
}());
UserService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof http_1.HttpClient !== "undefined" && http_1.HttpClient) === "function" && _a || Object])
], UserService);
exports.UserService = UserService;
var ModelAuthenticate = (function () {
    function ModelAuthenticate(userId, username, password, functionAccount) {
        if (userId === void 0) { userId = ''; }
        if (username === void 0) { username = ''; }
        if (password === void 0) { password = ''; }
        if (functionAccount === void 0) { functionAccount = 0; }
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.functionAccount = functionAccount;
    }
    return ModelAuthenticate;
}());
exports.ModelAuthenticate = ModelAuthenticate;
var _a;
//# sourceMappingURL=user.service.js.map

/***/ }),

/***/ "../../../../../src/app/skeleton/account/account-register/account-register.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".ng-valid[required], .ng-valid.required  {\r\n    border-left: 5px solid #42A948; \r\n  }\r\n  \r\n.ng-invalid:not(form)  {\r\n  border-left: 5px solid #a94442; \r\n}\r\n\r\n.registerContainer{\r\n  margin-top:15px;\r\n}\r\n\r\n.registerSend{\r\n  margin: 0 auto;\r\n  padding: 10px 30px;\r\n}\r\n\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/skeleton/account/account-register/account-register.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"registerContainer\" >\n    <div [hidden]=\"submitted\">\n      <h2 align=center >Rejestracja</h2>\n      <form (ngSubmit)=\"onSubmit()\" #orderForm=\"ngForm\" >\n          <div class=\"form-group\">\n            <label for=\"name\">Nick</label>\n            <input type=\"text\" class=\"form-control\" [(ngModel)]=\"model.name\" name='name' id=\"name\" minlength=\"3\" required #name=\"ngModel\">\n            <div *ngIf=\"name.invalid && (name.dirty || name.touched)\" class=\"alert alert-danger\">\n              <div *ngIf=\"name.errors.required\">\n                Pole jest wymagane.\n              </div>\n              <div *ngIf=\"name.errors.minlength\">\n                Pole musi posiadać minimum 3 znaki.\n              </div>\n            </div>\n          </div>\n\n          <div class=\"form-group\">\n              <label for=\"password\">Hasło</label>\n              <input type=\"password\" class=\"form-control\" [(ngModel)]=\"model.password\" name='password' id=\"password\" minlength=\"6\" required #password=\"ngModel\">\n              <div *ngIf=\"password.invalid && (password.dirty || password.touched)\" class=\"alert alert-danger\">\n                <div *ngIf=\"password.errors.required\">\n                  Pole jest wymagane.\n                </div>\n                <div *ngIf=\"password.errors.minlength\">\n                  Pole musi posiadać minimum 6 znaków.\n                </div>\n              </div>\n            </div>\n  \n          <div align=center>\n            <button  [disabled]=\"!orderForm.form.valid\" type=\"submit\" class=\"btn btn-success registerSend\">Rejestruj</button>\n          </div>\n      </form>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/skeleton/account/account-register/account-register.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var user_service_1 = __webpack_require__("../../../../../src/app/services/user.service.ts");
var AccountRegisterComponent = (function () {
    function AccountRegisterComponent(userService) {
        this.userService = userService;
        this.submitted = false;
        this.model = new ModelUser('', '');
        this.mainPage = new core_1.EventEmitter();
    }
    AccountRegisterComponent.prototype.ngOnInit = function () {
    };
    AccountRegisterComponent.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        this.userService.register(this.model).subscribe(function (res) {
            _this.mainPage.emit('Stworzono pomyślnie użytkownika');
        }, function (error) {
            console.log(error);
        });
    };
    return AccountRegisterComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], AccountRegisterComponent.prototype, "mainPage", void 0);
AccountRegisterComponent = __decorate([
    core_1.Component({
        selector: 'app-account-register',
        template: __webpack_require__("../../../../../src/app/skeleton/account/account-register/account-register.component.html"),
        styles: [__webpack_require__("../../../../../src/app/skeleton/account/account-register/account-register.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof user_service_1.UserService !== "undefined" && user_service_1.UserService) === "function" && _a || Object])
], AccountRegisterComponent);
exports.AccountRegisterComponent = AccountRegisterComponent;
var ModelUser = (function () {
    function ModelUser(name, password) {
        this.name = name;
        this.password = password;
    }
    return ModelUser;
}());
exports.ModelUser = ModelUser;
var _a;
//# sourceMappingURL=account-register.component.js.map

/***/ }),

/***/ "../../../../../src/app/skeleton/account/account.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".ng-valid[required], .ng-valid.required  {\r\n    border-left: 5px solid #42A948; \r\n  }\r\n  \r\n.ng-invalid:not(form)  {\r\n  border-left: 5px solid #a94442; \r\n}\r\n\r\n.loginContainer{\r\n  margin-top:15px;\r\n}\r\n\r\n.alert {\r\n  padding: 20px;\r\n  background-color: #f44336;\r\n  color: white;\r\n  opacity: 1;\r\n  transition: opacity 0.6s;\r\n  margin-bottom: 15px;\r\n}\r\n  \r\n.box:hover {\r\n  -webkit-animation-name: flash_background;\r\n          animation-name: flash_background;\r\n  -webkit-animation-duration: 10s;\r\n          animation-duration: 10s;\r\n  -webkit-animation-timing-function: linear;\r\n          animation-timing-function: linear;\r\n  -webkit-animation-iteration-count: infinite;\r\n          animation-iteration-count: infinite;\r\n}\r\n\r\n@-webkit-keyframes flash_background {\r\n  0% {\r\n    border-color: red;\r\n    background-color: red;\r\n  }\r\n  20% {\r\n    border-color: yellow;\r\n    background-color: yellow;\r\n  }\r\n  40% {\r\n    border-color: green;\r\n    background-color: green;\r\n  }\r\n  60% {\r\n    border-color: blue;\r\n    background-color: blue;\r\n  }\r\n  80% {\r\n    border-color: violet;\r\n    background-color: violet;\r\n  }\r\n  100% {\r\n    border-color: red;\r\n    background-color: red;\r\n  }\r\n}\r\n\r\n@keyframes flash_background {\r\n  0% {\r\n    border-color: red;\r\n    background-color: red;\r\n  }\r\n  20% {\r\n    border-color: yellow;\r\n    background-color: yellow;\r\n  }\r\n  40% {\r\n    border-color: green;\r\n    background-color: green;\r\n  }\r\n  60% {\r\n    border-color: blue;\r\n    background-color: blue;\r\n  }\r\n  80% {\r\n    border-color: violet;\r\n    background-color: violet;\r\n  }\r\n  100% {\r\n    border-color: red;\r\n    background-color: red;\r\n  }\r\n}\r\n\r\n.fixedAlert{\r\n  background-color: #4CAF50;\r\n  -webkit-transform: translatez(0);\r\n          transform: translatez(0);\r\n  position: fixed;\r\n  right:100px;\r\n  z-index: 2000;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/skeleton/account/account.component.html":
/***/ (function(module, exports) {

module.exports = "<div align=center style=\"display:none;\" class=\"success alert fixedAlert\">{{information}}</div>\n<div *ngIf=\"login=='login'\" class=\"loginContainer\" >\n  <form  (ngSubmit)=\"onSubmit()\" #orderForm=\"ngForm\" class=\"form-horizontal\">\n    <div class=\"form-group\">\n      <label class=\"control-label col-sm-2\" for=\"email\">Nick: (Admin)</label> \n      <div class=\"col-md-12 col-lg-10\">\n        <input type=\"text\" [(ngModel)]=\"model.username\" class=\"form-control\" id=\"username\" name=\"username\" placeholder=\"Podaj nick\" required  #username=\"ngModel\">\n      </div>\n    </div>\n    <div class=\"col-md-12 col-lg-10\">\n      <div *ngIf=\"username.invalid && (username.dirty || username.touched)\" class=\" alert alert-danger\">\n        <div *ngIf=\"username.errors.required\">\n          Pole jest wymagane.\n        </div>\n      </div>\n    </div>\n\n    <div class=\"form-group\">\n      <label class=\"control-label col-sm-2\" for=\"pwd\">Hasło: (secret)</label> \n      <div class=\"col-md-12 col-lg-10\"> \n        <input type=\"password\"  [(ngModel)]=\"model.password\" name=\"password\" class=\"form-control\" id=\"pwd\" placeholder=\"Podaj hasło\" required #password=\"ngModel\">\n      </div>\n    </div>\n    <div class=\"col-md-12 col-lg-10\">\n      <div  *ngIf=\"password.invalid && (password.dirty || password.touched)\" class=\"alert alert-danger\">\n        <div *ngIf=\"password.errors.required\">\n            Pole jest wymagane.\n        </div>\n      </div>\n    </div>\n\n    <div class=\"form-group\"> \n      <div align=center class=\"col-lg-offset-2 col-md-12 col-lg-10\">\n        <button [disabled]=\"!orderForm.form.valid\" type=\"submit\" class=\"btn btn-success orderSend\">Zaloguj</button>\n      </div>\n    </div>\n  </form>\n  <div class=\"col-md-12 col-lg-10\">\n    <div *ngIf=\"error\" class=\"alert error\">\n        Podany użytkownik nie istnieje\n    </div>\n  </div>\n  <div class=\"col-md-12 col-lg-10\" align=\"center\">\n      <p>Nie posiadasz konta?</p>\n      <button (click)=\"registerPage()\" class=\"box btn\">Zarejestruj się!</button>\n  </div>\n</div>\n<app-admin-start-view *ngIf=\"login=='loggedAdmin'\" (logoutPage)=\"displayFormPage($event)\"></app-admin-start-view>\n<app-user-start-view *ngIf=\"login=='loggedUser'\" (logoutPage)=\"displayFormPage($event)\"></app-user-start-view>\n<app-account-register *ngIf=\"login=='register'\" (mainPage)=\"displayFormPage($event)\"></app-account-register>\n"

/***/ }),

/***/ "../../../../../src/app/skeleton/account/account.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var item_service_1 = __webpack_require__("../../../../../src/app/services/item.service.ts");
var user_service_1 = __webpack_require__("../../../../../src/app/services/user.service.ts");
var AccountComponent = (function () {
    function AccountComponent(itemService, userService) {
        this.itemService = itemService;
        this.userService = userService;
        this.model = new user_service_1.ModelAuthenticate('', '');
        this.error = false;
        this.login = 'login';
    }
    AccountComponent.prototype.ngOnInit = function () {
        this.model = this.userService.getUser();
        if (this.model.username != '' && this.model.functionAccount > 0) {
            if (this.model.functionAccount < 10) {
                this.login = 'loggedUser';
            }
            else {
                this.login = 'loggedAdmin';
            }
        }
    };
    AccountComponent.prototype.onSubmit = function () {
        var _this = this;
        var ifAutenticated = this.userService.authenticate(this.model);
        ifAutenticated.subscribe(function (data) {
            _this.userService.setUser(_this.model, data['function'], data['token'], data['userId']);
            _this.itemService.setBasket(data['basketSaved']);
            if (data['function'] < 10) {
                _this.login = 'loggedUser';
            }
            else {
                _this.login = 'loggedAdmin';
            }
        }, function (err) {
            _this.error = true;
            console.log("error", err);
        });
    };
    AccountComponent.prototype.displayFormPage = function (event) {
        this.login = 'login';
        if (event != undefined) {
            this.information = event;
            $('.fixedAlert.success').show();
            $('.fixedAlert.success').fadeOut(4000);
        }
    };
    AccountComponent.prototype.registerPage = function () {
        this.login = 'register';
    };
    return AccountComponent;
}());
AccountComponent = __decorate([
    core_1.Component({
        selector: 'app-administrator',
        //providers:[ItemService],
        template: __webpack_require__("../../../../../src/app/skeleton/account/account.component.html"),
        styles: [__webpack_require__("../../../../../src/app/skeleton/account/account.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof item_service_1.ItemService !== "undefined" && item_service_1.ItemService) === "function" && _a || Object, typeof (_b = typeof user_service_1.UserService !== "undefined" && user_service_1.UserService) === "function" && _b || Object])
], AccountComponent);
exports.AccountComponent = AccountComponent;
var _a, _b;
//# sourceMappingURL=account.component.js.map

/***/ }),

/***/ "../../../../../src/app/skeleton/account/accounts.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, " button.newButton {\r\n    background: none;\r\n    border: 0;\r\n    box-sizing: border-box;\r\n    margin: 1em;\r\n    padding: 1em 2em;\r\n\r\n    box-shadow: inset 0 0 0 2px gray;\r\n    color: gray;\r\n    font-size: inherit;\r\n    font-weight: 700;\r\n\r\n    position: relative;\r\n    vertical-align: middle;\r\n}\r\nbutton.tablebtn.newButton{\r\n    padding: 0.7em 1.4em;\r\n    margin: 0.5em;\r\n}\r\nbutton.newButton::before,\r\nbutton.newButton::after {\r\n    box-sizing: inherit;\r\n    content: '';\r\n    position: absolute;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n\r\n.center:hover {\r\n    color:#009ce0;\r\n}\r\n\r\n.center::before,\r\n.center::after {\r\n    top: 0;\r\n    left: 0;\r\n    height: 100%;\r\n    width: 100%;\r\n    -webkit-transform-origin: center;\r\n            transform-origin: center;\r\n}\r\n\r\n.center::before {\r\n    border-top: 2px solid lightskyblue;\r\n    border-bottom: 2px solid lightskyblue;\r\n    -webkit-transform: scale3d(0,0,1);\r\n            transform: scale3d(0,0,1); \r\n}\r\n\r\n.center::after {\r\n    border-left: 2px solid lightskyblue;\r\n    border-right: 2px solid lightskyblue;\r\n    -webkit-transform: scale3d(0,0,1);\r\n            transform: scale3d(0,0,1); \r\n}\r\n\r\n.center:hover::before,\r\n.center:hover::after {\r\n    -webkit-transform: scale3d(1,1,1);\r\n            transform: scale3d(1,1,1); \r\n    transition: -webkit-transform 0.5s; \r\n    transition: transform 0.5s; \r\n    transition: transform 0.5s, -webkit-transform 0.5s;\r\n}\r\n\r\n.spin:hover {\r\n    color: #009ce0;\r\n  }\r\n\r\n.spin::before,\r\n.spin::after {\r\n    top: 0;\r\n    left: 0;\r\n}\r\n\r\n.spin::before{\r\n    border: 2px solid transparent; \r\n}\r\n\r\n.spin:hover::before {\r\n    border-top-color: lightskyblue; \r\n    border-right-color: lightskyblue;\r\n    border-bottom-color: lightskyblue;\r\n\r\n    transition:\r\n        border-top-color 0.15s linear, \r\n        border-right-color 0.15s linear 0.10s,\r\n        border-bottom-color 0.15s linear 0.20s;\r\n}\r\n\r\n.spin::after {\r\n  border: 0 solid transparent; \r\n}\r\n\r\n.spin:hover::after {\r\n    border-top: 2px solid lightskyblue; \r\n    border-left-width: 2px; \r\n    border-right-width: 2px;\r\n    -webkit-transform: rotate(270deg);\r\n            transform: rotate(270deg);\r\n    transition:\r\n        border-left-width 0s linear 0.35s,\r\n        -webkit-transform 0.4s linear 0s;\r\n    transition:\r\n        transform 0.4s linear 0s,\r\n        border-left-width 0s linear 0.35s;\r\n    transition:\r\n        transform 0.4s linear 0s,\r\n        border-left-width 0s linear 0.35s,\r\n        -webkit-transform 0.4s linear 0s;\r\n}\r\n\r\n.spin {\r\n    width: 9em;\r\n    height: 9em;\r\n    padding: 0;\r\n}\r\n\r\n.circle {\r\n    border-radius: 100%;\r\n    box-shadow: none;\r\n}\r\n\r\n.circle::before,\r\n.circle::after {\r\n    border-radius: 100%;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/skeleton/account/admin-start-view/admin-start-view.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "button {\r\n  background: none;\r\n  border: 0;\r\n  box-sizing: border-box;\r\n  margin: 2em;\r\n  padding: 2em 3em;\r\n  \r\n  box-shadow: inset 0 0 0 2px red;\r\n  color: red;\r\n  font-size: 14pt;\r\n  font-weight: 700;\r\n\r\n  position: relative;\r\n  vertical-align: middle;\r\n}\r\n\r\nbutton::before,\r\nbutton::after {\r\n  box-sizing: inherit;\r\n  content: '';\r\n  position: absolute;\r\n  width: 100%;\r\n  height: 100%;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/skeleton/account/admin-start-view/admin-start-view.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"pageAdmin == 'viewAdmin'\" class=\"viewContainer\" align=\"center\">\n  <section class=\"buttons\">\n    <div>\n      <button class=\"spin circle\" (click)=\"pageAdmin='products'\">Produkty</button>\n      <button class=\"spin circle\" (click)=\"pageAdmin='orders'\">Zamówienia</button>\n    </div>\n      <button class=\"spin circle\" (click)=\"logout()\">Wyloguj</button>\n  </section>\n</div>\n\n<app-orders *ngIf=\"pageAdmin == 'orders'\" (pageAdmin)=\"changePage($event)\"></app-orders>\n<app-products *ngIf=\"pageAdmin == 'products'\" (pageAdmin)=\"changePage($event)\" ></app-products>\n"

/***/ }),

/***/ "../../../../../src/app/skeleton/account/admin-start-view/admin-start-view.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var user_service_1 = __webpack_require__("../../../../../src/app/services/user.service.ts");
var AdminStartViewComponent = (function () {
    function AdminStartViewComponent(userService) {
        this.userService = userService;
        this.logoutPage = new core_1.EventEmitter();
        this.pageAdmin = "viewAdmin";
    }
    AdminStartViewComponent.prototype.ngOnInit = function () { };
    AdminStartViewComponent.prototype.changePage = function (page) {
        this.pageAdmin = page;
    };
    AdminStartViewComponent.prototype.logout = function () {
        var _this = this;
        this.userService.logout().subscribe(function (res) {
            _this.logoutPage.emit();
        }, function (error) {
            console.log(error);
        });
    };
    return AdminStartViewComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], AdminStartViewComponent.prototype, "logoutPage", void 0);
AdminStartViewComponent = __decorate([
    core_1.Component({
        selector: 'app-admin-start-view',
        template: __webpack_require__("../../../../../src/app/skeleton/account/admin-start-view/admin-start-view.component.html"),
        styles: [__webpack_require__("../../../../../src/app/skeleton/account/admin-start-view/admin-start-view.component.css"), __webpack_require__("../../../../../src/app/skeleton/account/accounts.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof user_service_1.UserService !== "undefined" && user_service_1.UserService) === "function" && _a || Object])
], AdminStartViewComponent);
exports.AdminStartViewComponent = AdminStartViewComponent;
var _a;
//# sourceMappingURL=admin-start-view.component.js.map

/***/ }),

/***/ "../../../../../src/app/skeleton/account/admin-start-view/orders/orders.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".hidden{\r\n    display:none;\r\n}\r\n\r\n.alert {\r\n    padding: 20px;\r\n    background-color: #f44336;\r\n    color: white;\r\n    opacity: 1;\r\n    transition: opacity 0.6s;\r\n    margin-bottom: 15px;\r\n    \r\n}\r\n\r\n.alert.success {-webkit-transform: translatez(0);transform: translatez(0);background-color: #4CAF50;}\r\n\r\n\r\n.row-odd{\r\n    background-color:#EDFAFF !important;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/skeleton/account/admin-start-view/orders/orders.component.html":
/***/ (function(module, exports) {

module.exports = "<div align=center>\n    <div align=center style=\"display:none\" class=\"success alert\">{{information}}</div>\n    <button class=\"center newButton\" (click)=\"changePage()\">Powrót</button>\n    <div #tableChild style=\"margin: auto; max-width: 1000px; margin-bottom: 50px;\">\n        <data-table id=\"products-grid\"\n            headerTitle=\"Zamówienia\"\n            [items]=\"items\"\n            [itemCount]=\"itemCount\"\n            (reload)=\"reloadItems($event)\"\n            >\n            \n            <data-table-column\n                [property]=\"'name'\"\n                header=\"Nazwa\"\n                [sortable]=\"true\"\n                [resizable]=\"true\">\n            </data-table-column>\n            <data-table-column\n                [property]=\"'sumPrice'\"\n                header=\"Suma\"\n                [sortable]=\"true\">\n            </data-table-column>\n            <data-table-column\n                [property]=\"'status'\"\n                header=\"Status\"\n                [sortable]=\"true\">\n            </data-table-column>\n            <data-table-column\n                [property]=\"'dateOrder'\"\n                header=\"Data zamówienia\"\n                [sortable]=\"true\">\n            </data-table-column>\n            <data-table-column\n                [property]=\"'id'\"\n                [header]=\"'id'\"\n                [visible]=\"false\">\n            </data-table-column>\n            \n            <data-table-column\n                width=\"140px\"\n                header=\"Edycja\">\n                <ng-template #dataTableHeader let-item=\"item\">\n                <i></i>\n                </ng-template>\n                <ng-template #dataTableCell let-item=\"item\">\n                <button (click)=\"showMoreInfo(item)\" class=\"btn btn-sm btn-default center  newButton tablebtn\">Pokaż</button>\n                </ng-template>\n            \n            </data-table-column>\n        </data-table>\n    </div>\n\n    <div *ngIf=\"orderActive && displayMoreInfo\">\n        <section class=\"card card-outline w-100\" style=\"width: 20rem;\">\n            <h4 class=\"card-header\">\n                {{orderActive.name}}\n            </h4>\n            <div class=\"card-block row\">\n                <div class=\"col-12\">\n                    <p class=\"card-text\">Ulica: <b> {{orderActive.street | uppercase}} </b></p>\n                    <p class=\"card-text\">Cena zamówienia: <b> {{orderActive.sumPrice | currency:'USD':true}} </b></p>\n                    <p class=\"card-text\">Szczegóły zamówienia:</p>\n                    <table>\n                        <tr>\n                            <th>\n                                Nazwa\n                            </th>\n                            <th style=\"padding-right: 15px;\">\n                                Liczba [szt.]\n                            </th>\n                            <th>\n                                Cena\n                            </th>\n                        </tr>\n                        <tr *ngFor=\"let entry of objectKeys(orderActive.orderItems)\">\n                            <td style=\"padding-right: 15px;\"class=\"card-text\">\n                                {{productsOrder[entry].name}}\n                            </td>\n                            <td class=\"card-text\">\n                                {{orderActive.orderItems[entry]['number']}}\n                            </td>\n                            <td class=\"card-text\">\n                                {{orderActive.orderItems[entry]['price'] * orderActive.orderItems[entry]['number'] | currency:'USD':true}}\n                            </td>\n                        </tr>\n                    </table>\n                </div>\n            </div>\n            <div class=\"card-footer\">\n                <select  [(ngModel)]=\"chosenOptionStatus\" class=\"form-control col-4\" name=\"status\">\n                    <option *ngFor=\"let statusId of objectKeys(statusArray)\" [value]=\"statusId\">{{statusArray[statusId]}}</option>\n                </select>\n                <button class=\"center newButton\" (click)=\"changeStatus(orderActive.id)\">Zmień status</button>\n            </div>\n        </section>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/skeleton/account/admin-start-view/orders/orders.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var angular_4_data_table_1 = __webpack_require__("../../../../angular-4-data-table/dist/index.js");
var item_service_1 = __webpack_require__("../../../../../src/app/services/item.service.ts");
var itemClass_1 = __webpack_require__("../../../../../src/app/skeleton/shop-center/item/itemClass.ts");
var OrdersComponent = (function () {
    function OrdersComponent(itemService) {
        this.itemService = itemService;
        this.pageAdmin = new core_1.EventEmitter();
        this.items = [];
        this.itemCount = 0;
        this.edited = false;
        this.objectKeys = Object.keys;
        this.statusArray = {}; // poprawić  i w urzytkowniku
        this.orders = [];
        this.displayMoreInfo = false;
        this.information = '';
        this.itemResource = new angular_4_data_table_1.DataTableResource(this.orders);
    }
    OrdersComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.itemService.getOrderStatuses().subscribe(function (res) {
            for (var i in res) {
                _this.statusArray[res[i]['id']] = res[i]['name'];
            }
        }, function (error) {
            console.log(error);
        });
        var orderObservable = this.itemService.getOrders();
        orderObservable.subscribe(function (res) {
            for (var i in res) {
                _this.orders.push(new Order(res[i]['_id'], res[i]['name'], res[i]['street'], res[i]['sumPrice'], res[i]['orderItems'], _this.statusArray[res[i]['status']], res[i]['status'], res[i]['dateOrder']));
            }
            _this.itemResource.count().then(function (count) { return _this.itemCount = count; });
            _this.reloadItems({ sortBy: "name", sortAsc: true, offset: 0, limit: 10 });
        }, function (error) {
            console.log(error);
        });
    };
    OrdersComponent.prototype.ngAfterViewChecked = function () {
        var sidePanel = this.tableChild.nativeElement.querySelectorAll('.panel-default');
        if (sidePanel.length > 0) {
            sidePanel[0].style.backgroundColor = 'white';
        }
        var icons = this.tableChild.nativeElement.querySelectorAll('.glyphicon-refresh');
        if (icons.length > 0) {
            icons[0].style.backgroundColor = 'white';
        }
        if (!this.edited) {
            $('.refresh-button').append('<i class="fa fa-refresh" aria-hidden="true"></i>');
            $('.column-selector-button').append('<i class="fa fa-list" aria-hidden="true"></i>');
            this.edited = true;
        }
    };
    OrdersComponent.prototype.showMoreInfo = function (order) {
        var _this = this;
        this.displayMoreInfo = false;
        this.orderActive = undefined;
        var index = 0;
        var count = Object.keys(order.orderItems).length;
        this.productsOrder = {};
        for (var id in order.orderItems) {
            var orderItems = this.itemService.getItemsFromId(id);
            orderItems.subscribe(function (res) {
                _this.productsOrder[res['_id']] = new itemClass_1.Item(res['_id'], res['name'], res['description'], res['price'], res['category'], 0, res['imageExist']);
                index++;
                if (index == count) {
                    _this.displayMoreInfo = true;
                }
            }, function (error) {
                console.log(error);
            });
        }
        this.chosenOptionStatus = order.statusId;
        this.orderActive = order;
        window.scrollTo(0, document.body.scrollHeight + 100);
    };
    OrdersComponent.prototype.changePage = function () {
        this.pageAdmin.emit('viewAdmin');
    };
    OrdersComponent.prototype.reloadItems = function (params) {
        var _this = this;
        this.itemResource.query(params).then(function (items) { return _this.items = items; });
    };
    OrdersComponent.prototype.changeStatus = function (orderId) {
        var _this = this;
        this.itemService.putOrder(orderId, this.chosenOptionStatus).subscribe(function (res) {
            //this.pageReturn.emit('Proces zakończono pomyślnie');
            $('html,body').scrollTop(0);
            $('.alert.fadeBlock').show();
            _this.information = 'Proces zakończono pomyślnie';
            $('.alert.fadeBlock').fadeOut(4000);
        }, function (error) { console.log(error); });
        this.orderActive.status = this.statusArray[this.chosenOptionStatus];
    };
    return OrdersComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], OrdersComponent.prototype, "pageAdmin", void 0);
__decorate([
    core_1.ViewChild('tableChild', { read: core_1.ElementRef }),
    __metadata("design:type", typeof (_a = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _a || Object)
], OrdersComponent.prototype, "tableChild", void 0);
OrdersComponent = __decorate([
    core_1.Component({
        selector: 'app-orders',
        template: __webpack_require__("../../../../../src/app/skeleton/account/admin-start-view/orders/orders.component.html"),
        styles: [__webpack_require__("../../../../../src/app/skeleton/account/admin-start-view/orders/orders.component.css"), __webpack_require__("../../../../../src/app/skeleton/account/accounts.css")],
        encapsulation: core_1.ViewEncapsulation.None // css dla potomków
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof item_service_1.ItemService !== "undefined" && item_service_1.ItemService) === "function" && _b || Object])
], OrdersComponent);
exports.OrdersComponent = OrdersComponent;
var Order = (function () {
    function Order(id, name, street, sumPrice, orderItems, status, statusId, dateOrder) {
        this.id = id;
        this.name = name;
        this.street = street;
        this.sumPrice = sumPrice;
        this.orderItems = orderItems;
        this.status = status;
        this.statusId = statusId;
        this.dateOrder = dateOrder;
    }
    return Order;
}());
exports.Order = Order;
var _a, _b;
//# sourceMappingURL=orders.component.js.map

/***/ }),

/***/ "../../../../../src/app/skeleton/account/admin-start-view/products/products-edit/products-edit.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".ng-valid[required], .ng-valid.required  {\r\n    border-left: 5px solid #42A948; \r\n  }\r\n  \r\n.ng-invalid:not(form)  {\r\n  border-left: 5px solid #a94442; \r\n} \r\n\r\n.filebtn{\r\n  margin-left:15px;\r\n  margin-right:15px;\r\n  border-color: lightskyblue;\r\n}\r\n\r\n.filebtn:hover{\r\n  background-color: lightskyblue;\r\n  border-color: #7DA8F6;\r\n}\r\n\r\n.imageproduct{\r\n  width:auto; \r\n  max-height:400px;\r\n}\r\n\r\ninput[type=checkbox] {\r\n  -webkit-transform: scale(1.5);\r\n          transform: scale(1.5);\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/skeleton/account/admin-start-view/products/products-edit/products-edit.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"EditProductContainer\" >\n    <form  (ngSubmit)=\"onSubmit()\" #productForm=\"ngForm\" class=\"form-horizontal\">\n      <div class=\"form-group\">\n        <label class=\"control-label col-sm-2\" for=\"name\">Nazwa:</label>\n        <div class=\"col-sm-10\">\n          <input type=\"text\" [(ngModel)]=\"product.name\" class=\"form-control\" name=\"name\" id=\"name\" placeholder=\"Nazwa\" required  #name=\"ngModel\">\n        </div>\n      </div>\n      <div class=\"col-sm-10\">\n        <div *ngIf=\"name.invalid && (name.dirty || name.touched)\" class=\" alert alert-danger\">\n          <div *ngIf=\"name.errors.required\">\n            Pole jest wymagane.\n          </div>\n        </div>\n      </div>\n  \n      <div class=\"form-group\">\n        <label class=\"control-label col-sm-2\" for=\"description\">Opis:</label>\n        <div class=\"col-sm-10\"> \n          <input type=\"text\"  [(ngModel)]=\"product.description\" name=\"description\" class=\"form-control\" id=\"description\" placeholder=\"Opis\" required #description=\"ngModel\">\n        </div>\n      </div>\n      <div class=\"col-sm-10\">\n        <div  *ngIf=\"description.invalid && (description.dirty || description.touched)\" class=\"alert alert-danger\" >\n          <div *ngIf=\"description.errors.required\">\n              Pole jest wymagane.\n          </div>\n        </div>\n      </div>\n\n      <div class=\"form-group\">\n          <label class=\"control-label col-sm-2\" for=\"price\">Cena:</label>\n          <div class=\"col-sm-10\"> \n            <input type=\"number\" min=0 pattern=\"[0-9,.]*\" [(ngModel)]=\"product.price\" name=\"price\" class=\"form-control\" id=\"price\" placeholder=\"Cena\" #price=\"ngModel\" required>\n          </div>\n        </div>\n        <div class=\"col-sm-10\">\n          <div  *ngIf=\"price.invalid && (price.dirty || price.touched)\" class=\"alert alert-danger\">\n            <div *ngIf=\"price.errors.pattern\">\n                Pole musi posiadać być liczbą nieujemną.\n            </div>\n            <div *ngIf=\"price.errors.required\">\n                Podana nawtość nie jest liczbą.\n            </div>\n          </div>\n      </div>\n\n      <div class=\"form-group\">\n          <label class=\"control-label col-sm-2\" for=\"category\">Kategoria:</label>\n          <div class=\"col-sm-10\"> \n            <select  [(ngModel)]=\"product.category\" class=\"form-control\" id=\"category\" name=\"category\" required #category=\"ngModel\">\n              <option *ngFor=\"let category of categories\" [value]=\"category\">{{category}}</option>\n            </select>\n          </div>\n        </div>\n        <div class=\"col-sm-10\">\n          <div *ngIf=\"category.invalid && (category.dirty || category.touched)\" class=\"alert alert-danger\">\n            <div *ngIf=\"category.errors.required\">\n                Pole jest wymagane.\n            </div>\n          </div>\n        </div>\n\n      <div *ngIf='buttonName==\"Edytuj\"' class=\"form-group\">\n        <label class=\"control-label checkbox-inline col-sm-4\" for=\"sale\">Promocja:\n           <input class=\"checkbox\" type=\"checkbox\" [(ngModel)]=\"product.sale\" name=\"sale\" class=\"form-control\" id=\"sale\" #sale=\"ngModel\" (selected)=\"product.sale\"> \n        </label>\n      </div>\n      <div class=\"form-group\">\n        <label class=\"control-label col-sm-4\" for=\"saleValue\">Obniżka o [%]:</label>\n        <div class=\"col-sm-4\"> \n          <input [(ngModel)]=\"product.saleValue\" #saleValue=\"ngModel\" class=\"form-control\" id=\"saleValue\" name=\"saleValue\" type=\"number\" min=\"0\" max=\"100\" step=\"0.01\" value=0 />\n        </div>\n      </div>\n      <div class=\"form-group\">\n        <label class=\"control-label col-sm-4\" for=\"saleValue\">Czas [s]:</label>\n        <div class=\"col-sm-4\"> \n          <input [(ngModel)]=\"product.saleTime\" #saleTime=\"ngModel\"  class=\"form-control\" id=\"saleTime\" name=\"saleTime\" type=\"number\" min=\"0\" value=0 />\n        </div>\n      </div>\n\n      <div class=\"form-group\">\n        <label class=\" btn btn-default col-11 col-sm-4 col-md-2 filebtn\"> \n          Załaduj plik\n          <input hidden class=\"btn\" type=\"file\" #fileInput placeholder=\"Załaduj plik\" />\n        </label>\n      </div>\n        <div *ngIf=\"product.imageExist\" class=\"col-sm-10\">\n          <img class=\"imageproduct\" alt=\"zdjęcie\" src={{image}}/>\n        </div>\n\n        \n      <div style=\"margin-top:10px;\" class=\"form-group\"> \n        <div class=\"col-sm-offset-2 col-sm-10\">\n          <button [disabled]=\"!productForm.form.valid\" type=\"submit\" class=\"btn btn-success orderSend\">{{buttonName}}</button>\n          <button (click)=goBack() type=\"button\" class=\"btn btn-default\">Powrót</button>\n        </div>\n      </div>\n\n    </form>\n  </div>\n  "

/***/ }),

/***/ "../../../../../src/app/skeleton/account/admin-start-view/products/products-edit/products-edit.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var forms_1 = __webpack_require__("../../../forms/@angular/forms.es5.js");
var item_service_1 = __webpack_require__("../../../../../src/app/services/item.service.ts");
var categoryservice_service_1 = __webpack_require__("../../../../../src/app/services/categoryservice.service.ts");
var ProductsEditComponent = (function () {
    function ProductsEditComponent(itemService, categoryserviceService) {
        this.itemService = itemService;
        this.categoryserviceService = categoryserviceService;
        this.pageReturn = new core_1.EventEmitter();
        this.categories = [];
        this.buttonName = "Dodaj";
        this.imageSrc = '';
        this.image = '';
        this.categories = [];
        this.categories = this.categoryserviceService.categories.slice();
        for (var _i = 0, _a = this.categoryserviceService.categoriesGroups; _i < _a.length; _i++) {
            var categoriesArray = _a[_i];
            for (var _b = 0, categoriesArray_1 = categoriesArray; _b < categoriesArray_1.length; _b++) {
                var category = categoriesArray_1[_b];
                this.categories.push(category);
            }
        }
    }
    ;
    ProductsEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.product.saleTime = 0;
        this.product.saleValue = 0;
        this.product.sale = this.itemService.ifProductSale(this.product.id);
        if (this.product.name != '') {
            this.buttonName = "Edytuj";
        }
        this.itemService.getPhoto(this.product.id).subscribe(function (data) {
            _this.image = data['image'];
        });
        this.productForm = new forms_1.FormGroup({
            'name': new forms_1.FormControl(this.product.name, [
                forms_1.Validators.required,
            ]),
            'description': new forms_1.FormControl(this.product.description, forms_1.Validators.required),
            'price': new forms_1.FormControl(this.product.price, forms_1.Validators.required),
            'category': new forms_1.FormControl(this.product.category, forms_1.Validators.required)
        });
    };
    ProductsEditComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.product.saleValue > 100) {
            this.product.saleValue = 100;
        }
        else if (this.product.saleValue < 0 || this.product.saleValue == undefined) {
            this.product.saleValue = 0;
        }
        if (this.product.saleTime < 0 || this.product.saleTime == undefined) {
            this.product.saleTime = 0;
        }
        var reader = new FileReader();
        var fileBrowser = this.fileInput.nativeElement;
        if (fileBrowser.files && fileBrowser.files[0]) {
            reader.readAsDataURL(fileBrowser.files[0]);
            reader.onload = function () {
                _this.imageSrc = reader.result;
                _this.itemService.addOrEditProduct(_this.product, _this.buttonName, _this.imageSrc).subscribe(function (res) {
                    _this.pageReturn.emit('Proces zakończono pomyślnie');
                }, function (error) { console.log(error); });
            };
        }
        else {
            this.itemService.addOrEditProduct(this.product, this.buttonName, undefined).subscribe(function (res) {
                _this.pageReturn.emit('Proces zakończono pomyślnie');
            }, function (error) { console.log(error); });
        }
    };
    ProductsEditComponent.prototype.goBack = function () {
        this.pageReturn.emit();
    };
    return ProductsEditComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], ProductsEditComponent.prototype, "product", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], ProductsEditComponent.prototype, "pageReturn", void 0);
__decorate([
    core_1.ViewChild('fileInput'),
    __metadata("design:type", typeof (_a = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _a || Object)
], ProductsEditComponent.prototype, "fileInput", void 0);
ProductsEditComponent = __decorate([
    core_1.Component({
        selector: 'app-products-edit',
        template: __webpack_require__("../../../../../src/app/skeleton/account/admin-start-view/products/products-edit/products-edit.component.html"),
        styles: [__webpack_require__("../../../../../src/app/skeleton/account/admin-start-view/products/products-edit/products-edit.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof item_service_1.ItemService !== "undefined" && item_service_1.ItemService) === "function" && _b || Object, typeof (_c = typeof categoryservice_service_1.CategoryserviceService !== "undefined" && categoryservice_service_1.CategoryserviceService) === "function" && _c || Object])
], ProductsEditComponent);
exports.ProductsEditComponent = ProductsEditComponent;
var _a, _b, _c;
//# sourceMappingURL=products-edit.component.js.map

/***/ }),

/***/ "../../../../../src/app/skeleton/account/admin-start-view/products/products.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".hidden{\r\n    display:none;\r\n}\r\n\r\n.alert {\r\n    padding: 20px;\r\n    background-color: #f44336;\r\n    color: white;\r\n    opacity: 1;\r\n    transition: opacity 0.6s;\r\n    margin-bottom: 15px;\r\n}\r\n\r\n.alert.success {-webkit-transform: translatez(0);transform: translatez(0); background-color: #4CAF50;}\r\n\r\n\r\n.row-odd{\r\n    background-color:#EDFAFF !important;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/skeleton/account/admin-start-view/products/products.component.html":
/***/ (function(module, exports) {

module.exports = "<app-products-edit *ngIf='subPageAdmin==\"edit\"' [product]='editProduct' (pageReturn)=\"showInfoAndReload($event)\"></app-products-edit>\n<div align=center style=\"display:none\" class=\"success alert fadeBlock\">{{information}}</div>\n<div align=center [ngClass]=\"{'hidden':subPageAdmin!='table'}\" >\n  <button class=\"center newButton\" (click)=\"changePage()\">Powrót</button>\n  <div #tableChild style=\"margin: auto; max-width: 1000px; margin-bottom: 50px;\">\n      <data-table id=\"products-grid\"\n          headerTitle=\"Produkty\"\n          [items]=\"items\"\n          [itemCount]=\"itemCount\"\n          \n          (reload)=\"reloadItems($event)\"\n          [selectColumn]=\"true\"\n          >\n          \n          <data-table-column\n              [property]=\"'name'\"\n              header=\"Nazwa\"\n              [sortable]=\"true\"\n              [resizable]=\"true\">\n          </data-table-column>\n          <data-table-column\n              [property]=\"'description'\"\n              header=\"Opis\"\n              [sortable]=\"true\">\n          </data-table-column>\n          <data-table-column\n              [property]=\"'price'\"\n              header=\"Cena\"\n              [sortable]=\"true\">\n          </data-table-column>\n          <data-table-column\n              [property]=\"'category'\"\n              header=\"Kategoria\"\n              [sortable]=\"true\">\n          </data-table-column>\n          <data-table-column\n              [property]=\"'id'\"\n              [header]=\"'id'\"\n              [visible]=\"false\">\n          </data-table-column>\n          <data-table-column\n            width=\"140px\"\n            header=\"Edycja\">\n            <ng-template #dataTableHeader let-item=\"item\">\n                <i></i>\n            </ng-template>\n            <ng-template #dataTableCell let-item=\"item\">\n                <button (click)=\"productEdit(item)\" class=\"btn btn-sm btn-default newButton center tablebtn\">Edytuj</button>\n            </ng-template>\n          </data-table-column>\n      </data-table>\n  </div>\n  <button class=\"center newButton\" (click)=\"deleteChecked()\">Usuń zaznaczone</button>\n  <button class=\"center newButton\" (click)=\"productNew()\">Dodaj produkt</button>\n</div>"

/***/ }),

/***/ "../../../../../src/app/skeleton/account/admin-start-view/products/products.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var itemClass_1 = __webpack_require__("../../../../../src/app/skeleton/shop-center/item/itemClass.ts");
var item_service_1 = __webpack_require__("../../../../../src/app/services/item.service.ts");
var index_1 = __webpack_require__("../../../../angular-4-data-table/src/index.ts");
var ProductsComponent = (function () {
    function ProductsComponent(itemService) {
        this.itemService = itemService;
        this.pageAdmin = new core_1.EventEmitter();
        this.items = [];
        this.itemCount = 0;
        this.subPageAdmin = 'table';
        this.information = '';
        this.edited = false;
        this.products = [];
        this.itemResource = new index_1.DataTableResource(this.products);
    }
    ProductsComponent.prototype.ngOnInit = function () {
        var _this = this;
        var productObservable = this.itemService.getProducts();
        this.itemResource = new index_1.DataTableResource(this.products);
        productObservable.subscribe(function (res) {
            for (var _i = 0, res_1 = res; _i < res_1.length; _i++) {
                var row = res_1[_i];
                _this.products.push(new itemClass_1.Item(row['_id'], row['name'], row['description'], row['price'], row['category'], 0, row['imageExist']));
            }
            _this.itemResource.count().then(function (count) { return _this.itemCount = count; });
            _this.reloadItems({ sortBy: "name", sortAsc: true, offset: 0, limit: 10 });
        }, function (error) {
            console.log(error);
        });
    };
    ProductsComponent.prototype.ngAfterViewChecked = function () {
        var sidePanel = this.tableChild.nativeElement.querySelectorAll('.panel-default');
        if (sidePanel.length > 0) {
            sidePanel[0].style.backgroundColor = 'white';
        }
        var icons = this.tableChild.nativeElement.querySelectorAll('.glyphicon-refresh');
        if (icons.length > 0) {
            icons[0].style.backgroundColor = 'white';
        }
        if (!this.edited) {
            $('.refresh-button').append('<i class="fa fa-refresh" aria-hidden="true"></i>');
            $('.column-selector-button').append('<i class="fa fa-list" aria-hidden="true"></i>');
            this.edited = true;
        }
    };
    ProductsComponent.prototype.changePage = function () {
        this.pageAdmin.emit('viewAdmin');
    };
    ProductsComponent.prototype.showInfoAndReload = function (event) {
        if (event != '' && event != undefined) {
            $('.alert.fadeBlock').show();
            this.information = event;
            this.subPageAdmin = 'table';
            $('.alert.fadeBlock').fadeOut(4000);
        }
        this.subPageAdmin = 'table';
        this.products = [];
        this.ngOnInit();
    };
    ProductsComponent.prototype.reloadItems = function (params) {
        var _this = this;
        this.itemResource.query(params).then(function (items) { return _this.items = items; });
    };
    ProductsComponent.prototype.productEdit = function (product) {
        this.subPageAdmin = "edit";
        this.editProduct = product;
    };
    ProductsComponent.prototype.productNew = function () {
        this.subPageAdmin = "edit";
        this.editProduct = new itemClass_1.Item(0, '', '', 0, '', 0, false);
    };
    ProductsComponent.prototype.deleteChecked = function () {
        var _this = this;
        var idDeleteArray = [];
        for (var _i = 0, _a = this.productTable.selectedRows; _i < _a.length; _i++) {
            var selected = _a[_i];
            idDeleteArray.push(selected.item.id);
        }
        var arrlen = idDeleteArray.length;
        var index = 0;
        if (idDeleteArray.length < 1) {
            return;
        }
        for (var _b = 0, idDeleteArray_1 = idDeleteArray; _b < idDeleteArray_1.length; _b++) {
            var row = idDeleteArray_1[_b];
            var observer = this.itemService.deleteProduct(row).subscribe(function (res) {
                index++;
                if (arrlen == index) {
                    _this.showInfoAndReload('Proces usuwania zakończono pomyślnie');
                }
            }, function (error) {
                console.log(error);
            });
        }
    };
    return ProductsComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], ProductsComponent.prototype, "pageAdmin", void 0);
__decorate([
    core_1.ViewChild(index_1.DataTable),
    __metadata("design:type", typeof (_a = typeof index_1.DataTable !== "undefined" && index_1.DataTable) === "function" && _a || Object)
], ProductsComponent.prototype, "productTable", void 0);
__decorate([
    core_1.ViewChild('tableChild', { read: core_1.ElementRef }),
    __metadata("design:type", typeof (_b = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _b || Object)
], ProductsComponent.prototype, "tableChild", void 0);
ProductsComponent = __decorate([
    core_1.Component({
        selector: 'app-products',
        template: __webpack_require__("../../../../../src/app/skeleton/account/admin-start-view/products/products.component.html"),
        styles: [__webpack_require__("../../../../../src/app/skeleton/account/admin-start-view/products/products.component.css"), __webpack_require__("../../../../../src/app/skeleton/account/accounts.css")],
        encapsulation: core_1.ViewEncapsulation.None // css dla potomków
    }),
    __metadata("design:paramtypes", [typeof (_c = typeof item_service_1.ItemService !== "undefined" && item_service_1.ItemService) === "function" && _c || Object])
], ProductsComponent);
exports.ProductsComponent = ProductsComponent;
var _a, _b, _c;
//# sourceMappingURL=products.component.js.map

/***/ }),

/***/ "../../../../../src/app/skeleton/account/user-start-view/user-orders/user-orders.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".viewContainer{\r\n    margin-top:10px;\r\n}\r\n\r\n.row-odd{\r\n    background-color:#EDFAFF !important;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/skeleton/account/user-start-view/user-orders/user-orders.component.html":
/***/ (function(module, exports) {

module.exports = "<div align=center>\n    <div align=center style=\"display:none\" class=\"success alert\">{{information}}</div>\n    <button class=\"center newButton\" (click)=\"changePage()\">Powrót</button>\n    <div #tableChild style=\"margin: auto; max-width: 1000px; margin-bottom: 50px;\">\n        <data-table id=\"products-grid\"\n            headerTitle=\"Zamówienia\"\n            [limit]=\"3\"\n            [items]=\"items\"\n            [itemCount]=\"itemCount\"\n            [expandableRows]=\"true\"\n            (reload)=\"reloadItems($event)\"\n            >\n            <ng-template #dataTableExpand let-item=\"item\">\n                <ul style=\"margin:0\">\n                    <li *ngFor=\"let itemId of objectKeys(item.orderItems)\">\n                        {{item.orderItems[itemId].name}} x {{item.orderItems[itemId].number}} =  {{item.orderItems[itemId].price}} zł\n                    </li>\n                </ul>   \n            </ng-template>\n            <data-table-column\n                [property]=\"'date'\"\n                header=\"Data\"\n                [sortable]=\"true\"\n                [resizable]=\"true\">\n            </data-table-column>\n            <data-table-column\n                [property]=\"'sumPrice'\"\n                header=\"Suma\"\n                [sortable]=\"true\">\n            </data-table-column>\n            <data-table-column\n                [property]=\"'status'\"\n                header=\"Status\"\n                [sortable]=\"true\">\n            </data-table-column>\n        </data-table>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/skeleton/account/user-start-view/user-orders/user-orders.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var item_service_1 = __webpack_require__("../../../../../src/app/services/item.service.ts");
var angular_4_data_table_1 = __webpack_require__("../../../../angular-4-data-table/dist/index.js");
var UserOrdersComponent = (function () {
    function UserOrdersComponent(itemService) {
        this.itemService = itemService;
        this.pageAdmin = new core_1.EventEmitter();
        this.objectKeys = Object.keys;
        this.edited = false;
        this.edited2 = false;
        this.myOrders = [];
        this.statusArray = {};
        this.items = [];
        this.itemCount = 0;
        this.itemResource = new angular_4_data_table_1.DataTableResource(this.myOrders);
    }
    UserOrdersComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.itemService.getOrderStatuses().subscribe(function (res) {
            for (var i in res) {
                _this.statusArray[res[i]['id']] = res[i]['name'];
            }
        }, function (error) {
            console.log(error);
        });
        var orderObservable = this.itemService.getUserOrders();
        orderObservable.subscribe(function (res) {
            for (var i in res) {
                _this.myOrders.push(new myOrders(res[i]['dateOrder'], res[i]['sumPrice'], res[i]['orderItems'], _this.statusArray[res[i]['status']]));
            }
            var _loop_1 = function (order) {
                var _loop_2 = function (id) {
                    var orderItems = _this.itemService.getItemsFromId(id);
                    orderItems.subscribe(function (res) {
                        order.orderItems[id].name = res['name'];
                    }, function (error) {
                        console.log(error);
                    });
                };
                for (var id in order.orderItems) {
                    _loop_2(id);
                }
            };
            for (var _i = 0, _a = _this.myOrders; _i < _a.length; _i++) {
                var order = _a[_i];
                _loop_1(order);
            }
            _this.itemResource.count().then(function (count) { return _this.itemCount = count; });
            _this.reloadItems({});
        }, function (error) {
            console.log(error);
        });
    };
    UserOrdersComponent.prototype.ngAfterViewChecked = function () {
        var sidePanel = this.tableChild.nativeElement.querySelectorAll('.panel-default');
        if (sidePanel.length > 0) {
            sidePanel[0].style.backgroundColor = 'white';
        }
        var icons = this.tableChild.nativeElement.querySelectorAll('.glyphicon-refresh');
        if (icons.length > 0) {
            icons[0].style.backgroundColor = 'white';
        }
        if (!this.edited) {
            $('.refresh-button').append('<i class="fa fa-refresh" aria-hidden="true"></i>');
            $('.column-selector-button').append('<i class="fa fa-list" aria-hidden="true"></i>');
            this.edited = true;
        }
        var icons2 = this.tableChild.nativeElement.querySelectorAll('.glyphicon-triangle-right');
        if (icons2.length > 0) {
            icons2[0].style.backgroundColor = 'white';
            if (!this.edited2) {
                $('.row-expand-button').append('<i class="fa fa-caret-down" aria-hidden="true"></i>');
                this.edited2 = true;
            }
        }
    };
    UserOrdersComponent.prototype.reloadItems = function (params) {
        var _this = this;
        this.itemResource.query(params).then(function (items) { return _this.items = items; });
    };
    UserOrdersComponent.prototype.changePage = function () {
        this.pageAdmin.emit('viewPanel');
    };
    return UserOrdersComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], UserOrdersComponent.prototype, "pageAdmin", void 0);
__decorate([
    core_1.ViewChild('tableChild', { read: core_1.ElementRef }),
    __metadata("design:type", typeof (_a = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _a || Object)
], UserOrdersComponent.prototype, "tableChild", void 0);
UserOrdersComponent = __decorate([
    core_1.Component({
        selector: 'app-user-orders',
        template: __webpack_require__("../../../../../src/app/skeleton/account/user-start-view/user-orders/user-orders.component.html"),
        styles: [__webpack_require__("../../../../../src/app/skeleton/account/user-start-view/user-orders/user-orders.component.css"), __webpack_require__("../../../../../src/app/skeleton/account/accounts.css")],
        encapsulation: core_1.ViewEncapsulation.None
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof item_service_1.ItemService !== "undefined" && item_service_1.ItemService) === "function" && _b || Object])
], UserOrdersComponent);
exports.UserOrdersComponent = UserOrdersComponent;
var myOrders = (function () {
    function myOrders(date, sumPrice, orderItems, status) {
        this.date = date;
        this.sumPrice = sumPrice;
        this.orderItems = orderItems;
        this.status = status;
    }
    return myOrders;
}());
exports.myOrders = myOrders;
var _a, _b;
//# sourceMappingURL=user-orders.component.js.map

/***/ }),

/***/ "../../../../../src/app/skeleton/account/user-start-view/user-start-view.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".viewContainer{\r\n    margin-top:10px;\r\n}\r\nbutton {\r\n    background: none;\r\n    border: 0;\r\n    box-sizing: border-box;\r\n    margin: 2em;\r\n    padding: 2em 3em;\r\n    \r\n    box-shadow: inset 0 0 0 2px red;\r\n    color: red;\r\n    font-size: 14pt;\r\n    font-weight: 700;\r\n  \r\n    position: relative;\r\n    vertical-align: middle;\r\n  }\r\n  \r\n  button::before,\r\n  button::after {\r\n    box-sizing: inherit;\r\n    content: '';\r\n    position: absolute;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n  \r\n  ", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/skeleton/account/user-start-view/user-start-view.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"pageUser == 'viewPanel'\" class=\"viewContainer\" align=\"center\">\n    <section class=\"buttons\">\n      <div>\n        <button class=\"spin circle\" (click)=\"pageUser='orders'\">Historia zamówień</button>\n        <!-- <button class=\"spin circle\" (click)=\"pageAdmin='orders'\">Zamówienia</button> -->\n      </div>\n        <button class=\"spin circle\" (click)=\"logout()\">Wyloguj</button>\n  </section>\n</div>\n\n<app-user-orders *ngIf=\"pageUser == 'orders'\" (pageAdmin)=\"changePage($event)\"></app-user-orders>"

/***/ }),

/***/ "../../../../../src/app/skeleton/account/user-start-view/user-start-view.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var user_service_1 = __webpack_require__("../../../../../src/app/services/user.service.ts");
var UserStartViewComponent = (function () {
    function UserStartViewComponent(userService) {
        this.userService = userService;
        this.logoutPage = new core_1.EventEmitter();
        this.pageUser = "viewPanel";
    }
    UserStartViewComponent.prototype.ngOnInit = function () {
    };
    UserStartViewComponent.prototype.changePage = function (page) {
        this.pageUser = page;
    };
    UserStartViewComponent.prototype.logout = function () {
        var _this = this;
        this.userService.logout().subscribe(function (res) {
            _this.logoutPage.emit();
        }, function (error) {
            console.log(error);
        });
    };
    return UserStartViewComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], UserStartViewComponent.prototype, "logoutPage", void 0);
UserStartViewComponent = __decorate([
    core_1.Component({
        selector: 'app-user-start-view',
        template: __webpack_require__("../../../../../src/app/skeleton/account/user-start-view/user-start-view.component.html"),
        styles: [__webpack_require__("../../../../../src/app/skeleton/account/user-start-view/user-start-view.component.css"), __webpack_require__("../../../../../src/app/skeleton/account/accounts.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof user_service_1.UserService !== "undefined" && user_service_1.UserService) === "function" && _a || Object])
], UserStartViewComponent);
exports.UserStartViewComponent = UserStartViewComponent;
var _a;
//# sourceMappingURL=user-start-view.component.js.map

/***/ }),

/***/ "../../../../../src/app/skeleton/basket-center/basket-center.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".basketContainer{\r\n    margin-top:15px;\r\n}\r\n\r\n.basketTable td{\r\n    vertical-align: middle;\r\n}\r\n\r\n.continueShopping:hover, .ordering:not([disabled]):hover{\r\n    background-color: lightskyblue;\r\n}\r\n\r\n.continueShopping, .ordering:not([disabled]){\r\n    border-color: lightskyblue;\r\n}\r\n\r\n.deleteItem:hover{\r\n    border:1px solid red;\r\n    background-color:lightcoral;\r\n}\r\n\r\n.table-striped tbody tr:nth-of-type(odd) {\r\n    background-color: #EDFAFF;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/skeleton/basket-center/basket-center.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"basketContainer\">\n  <h2 align=\"center\">Twój koszyk na zakupy</h2>\n  <table class=\"table table-striped basketTable\">\n    <thead>\n      <th>Ilość</th>\n      <th>Produkt</th>\n      <th>Cena</th>\n      <th>Wartość</th>\n      <th></th>\n    </thead>\n    <tbody>\n      <tr *ngFor=\"let itemBasket of itemsBasket;\">\n        <td>{{itemBasket.order}}</td>\n        <td>{{itemBasket.name}}</td>\n        <td>{{itemBasket.price | currency:'USD':true}}</td>\n        <td>{{itemBasket.order * itemBasket.price | currency:'USD':true}}</td>\n        <td><button (click)=\"deleteItemInBasket(itemBasket)\" class=\"btn deleteItem\">Usuń</button></td>\n      </tr>\n    </tbody>\n    <tfoot>\n      <th></th>\n      <th></th>\n      <th>Wartość całkowita:</th>\n      <th>{{itemsSum | currency:'USD':true}}</th>\n      <th></th>\n    </tfoot>\n  </table>\n  <div align='center'>\n    <button (click)=\"changePage('shop')\" class=\"btn continueShopping\">Kontynuuj Zakupy</button>\n    <button (click)=\"changePage('ordering')\" [disabled]='itemsSum<=0' class=\"btn ordering\">Złóż zamówienie</button>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/skeleton/basket-center/basket-center.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var item_service_1 = __webpack_require__("../../../../../src/app/services/item.service.ts");
var basket_service_1 = __webpack_require__("../../../../../src/app/services/basket.service.ts");
var user_service_1 = __webpack_require__("../../../../../src/app/services/user.service.ts");
var BasketCenterComponent = (function () {
    function BasketCenterComponent(userService, itemService, basketService) {
        var _this = this;
        this.userService = userService;
        this.itemService = itemService;
        this.basketService = basketService;
        this.pageMain = new core_1.EventEmitter();
        this.itemsSum = 0;
        this.itemService.sale$.subscribe(function (data) {
            _this.getbasket();
        });
    }
    BasketCenterComponent.prototype.ngOnInit = function () {
        this.getbasket();
    };
    BasketCenterComponent.prototype.getbasket = function () {
        this.itemsBasket = this.itemService.getBasketService();
        for (var _i = 0, _a = this.itemsBasket; _i < _a.length; _i++) {
            var item = _a[_i];
            this.itemsSum += item.order * item.price;
        }
        this.saveBasket();
    };
    BasketCenterComponent.prototype.deleteItemInBasket = function (itemBasket) {
        this.basketService.publishData(itemBasket.price, -itemBasket.order);
        this.itemsSum -= itemBasket.order * itemBasket.price;
        itemBasket.order = 0;
        this.itemsBasket.splice(this.itemsBasket.indexOf(itemBasket), 1);
        this.itemService.reloadSiteHeader();
    };
    BasketCenterComponent.prototype.changePage = function (page) {
        this.pageMain.emit(page);
    };
    BasketCenterComponent.prototype.saveBasket = function () {
        if (this.userService.ifUserLogged()) {
            var basket = this.itemService.getBasket2Save();
            this.userService.saveBasket(basket);
        }
    };
    return BasketCenterComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], BasketCenterComponent.prototype, "pageMain", void 0);
BasketCenterComponent = __decorate([
    core_1.Component({
        selector: 'app-basket-center',
        template: __webpack_require__("../../../../../src/app/skeleton/basket-center/basket-center.component.html"),
        styles: [__webpack_require__("../../../../../src/app/skeleton/basket-center/basket-center.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof user_service_1.UserService !== "undefined" && user_service_1.UserService) === "function" && _a || Object, typeof (_b = typeof item_service_1.ItemService !== "undefined" && item_service_1.ItemService) === "function" && _b || Object, typeof (_c = typeof basket_service_1.BasketService !== "undefined" && basket_service_1.BasketService) === "function" && _c || Object])
], BasketCenterComponent);
exports.BasketCenterComponent = BasketCenterComponent;
var _a, _b, _c;
//# sourceMappingURL=basket-center.component.js.map

/***/ }),

/***/ "../../../../../src/app/skeleton/filter-bar/filter-bar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".list-group.panel  .list-group-item{\r\n  border-bottom-right-radius: 4px;\r\n  border-bottom-left-radius: 4px;\r\n  background: #f7f7f7;\r\n  color: #666;\r\n}\r\n\r\n.list-group.panel  .list-group-item.groupname{\r\n  background-color: white;\r\n}\r\n\r\n.list-group.panel .list-group-item:hover{\r\n  background-color: lightblue;\r\n}\r\n\r\n.list-group.panel .list-group-item.actives{\r\n  background-color: lightblue;\r\n  border-left: 3px solid #7DA8F6;\r\n}\r\n\r\n.list-group.panel .list-group-item.actives:hover{\r\n  background-color: #BDE8EF;\r\n}\r\n\r\n.strong { font-weight: bold; }\r\n\r\n.btnplus{\r\n  border-color: lightskyblue\r\n}\r\n\r\n.btnplus:hover{\r\n  background-color: lightskyblue;\r\n  border-color: #7DA8F6;\r\n}\r\n\r\n.promo{\r\n  font-weight: bold;\r\n  color:darkred;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/skeleton/filter-bar/filter-bar.component.html":
/***/ (function(module, exports) {

module.exports = "<aside class=\"list-group panel\">\n    \n    <a class=\"list-group-item list-group-item strong text-center categoryTitle\" style=\"background: #009ce0; color: white;\" >Kategorie</a>\n    <a *ngFor=\"let category of categories;\"  (click)=\"send(category)\" [ngClass]=\"{'actives' : filters.indexOf(category) > -1}\" class=\"list-group-item list-group-item-success strong\" >{{category}} </a>\n    \n    <div *ngFor=\"let categoriesGroup of categoriesGroups;let i = index\">\n        <a  href=\"#{{categoriesGroupsNames[i]}}\"  class=\"list-group-item list-group-item groupname strong\" data-toggle=\"collapse\" > {{categoriesGroupsNames[i]}} </a>\n        <div class=\"collapse list-group-submenu\" id=\"{{categoriesGroupsNames[i]}}\">\n            <a *ngFor=\"let category of categoriesGroup;\" (click)=\"send(category)\" [ngClass]=\"{'actives' : filters.indexOf(category) > -1}\" class=\"list-group-item\"> {{category}}</a>\n        </div>\n    </div>\n    <div style=\"margin-top:15px\" align=\"center\">\n        <span>Filtr cenowy</span>\n        <div style=\"margin-top:40px\" >\n            <nouislider  #slider [config]=\"someRange2config\" [(ngModel)]=\"someRange2\" (ngModelChange)=\"onChange($event)\" (ngModelChange)=\"onChange($event)\" ></nouislider>\n        </div>\n    </div>\n</aside>\n\n<div *ngIf=\"saleProduct.name\" class=\"box\">\n    <section class=\"card card-outline w-100 \" style=\"width: 20rem;\" align=center>\n        <h4 class=\"card-header\"> \n            Najnowsza Promocja\n        </h4>\n        <h5 class=\"card-header\">\n            {{saleProduct.name | uppercase}}\n        </h5>\n        <div class=\"card-block\">\n            <p align=\"center\" class=\"card-text text-center\">Cena: <span class=\"promo\"> {{saleProduct.price | currency:'USD':true}}</span></p>\n            <div class=\"col-12 text-center\">\n                <button (click)=\"add(saleProduct)\" class='btn btnplus'>Kup teraz!</button>\n            </div>\n            <p class=\"card-text\" *ngIf=\"saleProduct.order\">W koszyku: <span class=\"itemOrderNr\">{{saleProduct.order}} </span> szt.</p>\n        </div>\n      </section>\n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/skeleton/filter-bar/filter-bar.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var categoryservice_service_1 = __webpack_require__("../../../../../src/app/services/categoryservice.service.ts");
var basket_service_1 = __webpack_require__("../../../../../src/app/services/basket.service.ts");
var user_service_1 = __webpack_require__("../../../../../src/app/services/user.service.ts");
var item_service_1 = __webpack_require__("../../../../../src/app/services/item.service.ts");
var itemClass_1 = __webpack_require__("../../../../../src/app/skeleton/shop-center/item/itemClass.ts");
var FilterBarComponent = (function () {
    function FilterBarComponent(userService, basketService, categoryserviceService, itemService) {
        var _this = this;
        this.userService = userService;
        this.basketService = basketService;
        this.categoryserviceService = categoryserviceService;
        this.itemService = itemService;
        this.addOut = new core_1.EventEmitter();
        this.filters = [];
        this.categories = [];
        this.categoriesGroups = [];
        this.categoriesGroupsNames = [];
        this.saleProduct = new itemClass_1.Item();
        this.someRange2config = {
            behaviour: 'drag',
            connect: true,
            start: [0, 2000],
            margin: 1,
            keyboard: true,
            step: 1,
            range: {
                min: 0,
                max: 2000
            },
            tooltips: true,
        };
        this.itemService.sale$.subscribe(function (data) {
            _this.saleProduct = data;
        });
    }
    FilterBarComponent.prototype.onChange = function (event) {
        this.categoryserviceService.publishFilterPrice(this.someRange2);
    };
    FilterBarComponent.prototype.ngOnInit = function () {
        this.saleProduct = this.itemService.getPromotion();
        this.categories = this.categoryserviceService.categories;
        this.categoriesGroups = this.categoryserviceService.categoriesGroups;
        this.categoriesGroupsNames = this.categoryserviceService.categoriesGroupsNames;
    };
    FilterBarComponent.prototype.ngAfterViewInit = function () {
        this.setColors();
    };
    FilterBarComponent.prototype.add = function (itemObject) {
        itemObject.order += 1;
        this.addOut.emit(itemObject);
        this.basketService.publishData(itemObject.price, 1);
        this.saveBasket();
    };
    FilterBarComponent.prototype.saveBasket = function () {
        if (this.userService.ifUserLogged()) {
            var basket = this.itemService.getBasket2Save();
            this.userService.saveBasket(basket);
        }
    };
    FilterBarComponent.prototype.setColors = function () {
        var connect = this.slider.nativeElement.querySelectorAll('.noUi-connect');
        connect[0].classList.add('skyBackground');
        var connectTooltips = this.slider.nativeElement.querySelectorAll('.noUi-tooltip');
        for (var _i = 0, connectTooltips_1 = connectTooltips; _i < connectTooltips_1.length; _i++) {
            var connectTooltip = connectTooltips_1[_i];
            connectTooltip.classList.add('skyBorder');
        }
    };
    FilterBarComponent.prototype.send = function (category) {
        var index = this.filters.indexOf(category);
        if (index > -1) {
            this.filters.splice(index, 1);
        }
        else {
            this.filters.push(category);
        }
        this.categoryserviceService.publishData(this.filters);
    };
    return FilterBarComponent;
}());
__decorate([
    core_1.ViewChild('slider', { read: core_1.ElementRef }),
    __metadata("design:type", typeof (_a = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _a || Object)
], FilterBarComponent.prototype, "slider", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], FilterBarComponent.prototype, "addOut", void 0);
FilterBarComponent = __decorate([
    core_1.Component({
        selector: 'app-filter-bar',
        template: __webpack_require__("../../../../../src/app/skeleton/filter-bar/filter-bar.component.html"),
        styles: [__webpack_require__("../../../../../src/app/skeleton/filter-bar/filter-bar.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof user_service_1.UserService !== "undefined" && user_service_1.UserService) === "function" && _b || Object, typeof (_c = typeof basket_service_1.BasketService !== "undefined" && basket_service_1.BasketService) === "function" && _c || Object, typeof (_d = typeof categoryservice_service_1.CategoryserviceService !== "undefined" && categoryservice_service_1.CategoryserviceService) === "function" && _d || Object, typeof (_e = typeof item_service_1.ItemService !== "undefined" && item_service_1.ItemService) === "function" && _e || Object])
], FilterBarComponent);
exports.FilterBarComponent = FilterBarComponent;
var _a, _b, _c, _d, _e;
//# sourceMappingURL=filter-bar.component.js.map

/***/ }),

/***/ "../../../../../src/app/skeleton/header/header.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "header{\r\n    background-color:lightskyblue;\r\n    padding: 5px 0;\r\n}\r\n\r\n.firmName{\r\n    font-size:1.2em;\r\n}\r\n\r\n.logo{\r\n    width:30px;\r\n    height:30px;\r\n}\r\n\r\n.tobasket{\r\n    background-color:#009ce0;\r\n}\r\n\r\n.tobasket:hover{\r\n    border:1px solid blue;\r\n}\r\n\r\n.shop{\r\n    cursor: pointer;\r\n}\r\n\r\n\r\n@media only screen and (max-width: 768px) {\r\n    header > div{\r\n        text-align:center;\r\n    }\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/skeleton/header/header.component.html":
/***/ (function(module, exports) {

module.exports = "<header class='row' >\n  <div class=\"col-sm-12 col-md-6 shop\" align=\"left\" (click)=\"changePage('shop')\">\n      <img  style=\"vertical-align: top;\" class=\"logo\" [src]=\"'assets/illenium.png'\" /> \n      <span style=\"display: inline-block; margin-top:7px;\"><span class=\"firmName\">Illenium</span>  <span style=\"margin-left:15px;\"> Sklep Internetowy</span></span>\n  </div>\n  <div class=\"col-sm-12 col-md-6\" align=\"right\">\n    <span style=\"display: inline-block; margin-top:7px;\"><b>Koszyk:</b> {{basketNumber}} produkt(ów), <span>{{ basketValue  | currency:'USD':true}}</span></span>\n    <button style=\"vertical-align: top;\" (click)=\"changePage('basket')\" class=\"btn tobasket\">Do kasy</button>\n  </div>\n</header>\n"

/***/ }),

/***/ "../../../../../src/app/skeleton/header/header.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var basket_service_1 = __webpack_require__("../../../../../src/app/services/basket.service.ts");
var item_service_1 = __webpack_require__("../../../../../src/app/services/item.service.ts");
var HeaderComponent = (function () {
    function HeaderComponent(itemService, basketService) {
        var _this = this;
        this.itemService = itemService;
        this.basketService = basketService;
        this.pageMain = new core_1.EventEmitter();
        this.basketValue = 0;
        this.basketNumber = 0;
        this.basketService.caseNumberBasket$.subscribe(function (data) {
            if (data[0] == 'reset') {
                _this.basketValue = 0;
                _this.basketNumber = 0;
            }
            else {
                _this.getbasket();
            }
        });
        this.itemService.sale$.subscribe(function (data) {
            _this.getbasket();
        });
        this.itemService.loadedbasket$.subscribe(function (data) {
            _this.getbasket();
        });
    }
    HeaderComponent.prototype.getbasket = function () {
        var itemsBasket = this.itemService.getBasketService();
        this.basketNumber = 0;
        this.basketValue = 0;
        for (var _i = 0, itemsBasket_1 = itemsBasket; _i < itemsBasket_1.length; _i++) {
            var item = itemsBasket_1[_i];
            this.basketNumber += item.order;
            this.basketValue += item.order * item.price;
        }
    };
    HeaderComponent.prototype.ngOnInit = function () {
        this.getbasket();
    };
    HeaderComponent.prototype.changePage = function (page) {
        this.pageMain.emit(page);
    };
    return HeaderComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], HeaderComponent.prototype, "pageMain", void 0);
HeaderComponent = __decorate([
    core_1.Component({
        selector: 'app-header',
        template: __webpack_require__("../../../../../src/app/skeleton/header/header.component.html"),
        styles: [__webpack_require__("../../../../../src/app/skeleton/header/header.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof item_service_1.ItemService !== "undefined" && item_service_1.ItemService) === "function" && _a || Object, typeof (_b = typeof basket_service_1.BasketService !== "undefined" && basket_service_1.BasketService) === "function" && _b || Object])
], HeaderComponent);
exports.HeaderComponent = HeaderComponent;
var _a, _b;
//# sourceMappingURL=header.component.js.map

/***/ }),

/***/ "../../../../../src/app/skeleton/nav-bar/nav-bar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".nav-item{\r\n    font-size:1.1em;\r\n}\r\n\r\n@media only screen and (max-width: 992px) {\r\n    /* centered navigation */\r\n    input[type=\"text\"] {\r\n        display: block;\r\n        margin : 0 auto;\r\n   }\r\n    nav li, nav input {\r\n      text-align: center;\r\n    }\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/skeleton/nav-bar/nav-bar.component.html":
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-toggleable-md navbar-light bg-faded\">\n    <button class=\"navbar-toggler navbar-toggler-right\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n      <span class=\"navbar-toggler-icon\"></span>\n    </button>\n    <a class=\"navbar-brand\" href=\"#\">&nbsp;</a>\n  \n    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\n      <ul class=\"navbar-nav mr-auto\">\n        <li class=\"nav-item\" [ngClass]=\"{'active':page=='shop'}\">\n          <a class=\"nav-link\" (click)=\"changePage('shop')\" href=\"#\">Sklep </a>\n        </li>\n        <li class=\"nav-item\" [ngClass]=\"{'active':page=='basket'}\">\n          <a class=\"nav-link\" (click)=\"changePage('basket')\" href=\"#\">Koszyk</a>\n        </li>\n        <li class=\"nav-item\" [ngClass]=\"{'active':page=='admin'}\">\n          <a class=\"nav-link\" (click)=\"changePage('admin')\" href=\"#\">Konto</a>\n        </li>\n      </ul>\n      <form class=\"form-inline my-2 my-lg-0\">\n        <input (keyup)=\"categorySend($event)\" class=\"form-control\" type=\"text\" placeholder=\"Szukaj produktu\">\n      </form>\n    </div>\n</nav>\n"

/***/ }),

/***/ "../../../../../src/app/skeleton/nav-bar/nav-bar.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var categoryservice_service_1 = __webpack_require__("../../../../../src/app/services/categoryservice.service.ts");
var NavBarComponent = (function () {
    function NavBarComponent(categoryserviceService) {
        this.categoryserviceService = categoryserviceService;
        this.pageMain = new core_1.EventEmitter();
    }
    NavBarComponent.prototype.ngOnInit = function () { };
    NavBarComponent.prototype.categorySend = function (event) {
        this.categoryserviceService.publishDataCategoryString(event.target.value);
    };
    NavBarComponent.prototype.changePage = function (page) {
        this.pageMain.emit(page);
    };
    return NavBarComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], NavBarComponent.prototype, "pageMain", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", String)
], NavBarComponent.prototype, "page", void 0);
NavBarComponent = __decorate([
    core_1.Component({
        selector: 'app-nav-bar',
        template: __webpack_require__("../../../../../src/app/skeleton/nav-bar/nav-bar.component.html"),
        styles: [__webpack_require__("../../../../../src/app/skeleton/nav-bar/nav-bar.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof categoryservice_service_1.CategoryserviceService !== "undefined" && categoryservice_service_1.CategoryserviceService) === "function" && _a || Object])
], NavBarComponent);
exports.NavBarComponent = NavBarComponent;
var _a;
//# sourceMappingURL=nav-bar.component.js.map

/***/ }),

/***/ "../../../../../src/app/skeleton/order-center/order-center.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".ng-valid[required], .ng-valid.required  {\r\n    border-left: 5px solid #42A948; \r\n  }\r\n  \r\n.ng-invalid:not(form)  {\r\n  border-left: 5px solid #a94442; \r\n}\r\n\r\n.orderContainer{\r\n  margin-top:15px;\r\n}\r\n\r\n.orderSend{\r\n  margin: 0 auto;\r\n  padding: 10px 30px;\r\n}\r\n\r\n.btn:hover{\r\n  background-color: lightskyblue;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/skeleton/order-center/order-center.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"orderContainer\" >\n  <div [hidden]=\"submitted\">\n    <h2 align=center >Informacje dotyczące wysyłki</h2>\n    <form (ngSubmit)=\"onSubmit()\" #orderForm=\"ngForm\" >\n        <div class=\"form-group\">\n          <h4>Odbiorca</h4>\n          <label for=\"name\">Imię i nazwisko</label>\n          <input type=\"text\" class=\"form-control\" [(ngModel)]=\"model.name\" name='name' id=\"name\" minlength=\"3\" required #name=\"ngModel\">\n          <div *ngIf=\"name.invalid && (name.dirty || name.touched)\" class=\"alert alert-danger\">\n            <div *ngIf=\"name.errors.required\">\n              Pole jest wymagane.\n            </div>\n            <div *ngIf=\"name.errors.minlength\">\n              Pole musi posiadać minimum 3 znaki.\n            </div>\n          </div>\n        </div>\n\n        <div class=\"form-group\">\n          <h4>Adres</h4>\n          <label for=\"street\">Nazwa ulicy</label>\n          <input type=\"text\" class=\"form-control\" id=\"street\" [(ngModel)]=\"model.street\" name='street' required #street=\"ngModel\">\n        </div>\n        \n        <div *ngIf=\"street.invalid && (street.dirty || street.touched)\" class=\"alert alert-danger\">\n            <div *ngIf=\"street.errors.required\">\n                Pole jest wymagane.\n            </div>\n        </div>\n        <div align=center>\n          <button  [disabled]=\"!orderForm.form.valid\" type=\"submit\" class=\"btn btn-success orderSend\">Wyślij</button>\n        </div>\n    </form>\n  </div>\n  \n  <div align=center [hidden]=\"!submitted\">\n    <h2>Podano następujący adres:</h2>\n    <div class=\"card card-outline-primary w-75\" >\n        <div class=\"card-block row\">\n          <div class=\"col-xs-12 col-sm-7 \">\n            <p align=left class=\"card-text\">\n              <span class=\"col-xs-3\">Imię i nazwisko:</span>\n              <span class=\"col-xs-9 \">{{ model.name }}</span>\n            </p>\n          </div>\n        <div class=\"col-xs-12 col-sm-7 \">\n          <p align=left class=\"card-text\">\n            <span class=\"col-xs-3\">Nazwa ulicy:</span>\n            <span class=\"col-xs-9\">{{ model.street }}</span>\n          </p>\n        </div>\n      </div>\n    </div>\n    <button class=\"btn btn-default\" (click)=\"backPage()\">Powrót do sklepu</button>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/skeleton/order-center/order-center.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var item_service_1 = __webpack_require__("../../../../../src/app/services/item.service.ts");
var basket_service_1 = __webpack_require__("../../../../../src/app/services/basket.service.ts");
var OrderCenterComponent = (function () {
    function OrderCenterComponent(itemService, basketService) {
        this.itemService = itemService;
        this.basketService = basketService;
        this.pageMain = new core_1.EventEmitter();
        this.submitted = false;
        this.model = new Model('', '');
        this.itemsOrder = {};
        this.itemsSum = 0;
    }
    OrderCenterComponent.prototype.ngOnInit = function () {
        this.getbasket();
    };
    OrderCenterComponent.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        this.itemService.sendOrder(this.model, this.itemsSum, this.itemsOrder).subscribe(function (res) {
            _this.itemService.resetShopContent();
            _this.basketService.publishData('reset');
        }, function (error) {
            console.log(error);
        });
    };
    OrderCenterComponent.prototype.getbasket = function () {
        this.itemsBasket = this.itemService.getBasketService();
        for (var _i = 0, _a = this.itemsBasket; _i < _a.length; _i++) {
            var item = _a[_i];
            this.itemsOrder[item.id.toString()] = {};
            this.itemsOrder[item.id.toString()]['number'] = item.order;
            this.itemsOrder[item.id.toString()]['price'] = item.price;
            this.itemsSum += item.order * item.price;
        }
    };
    OrderCenterComponent.prototype.backPage = function () {
        this.pageMain.emit('shop');
    };
    return OrderCenterComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], OrderCenterComponent.prototype, "pageMain", void 0);
OrderCenterComponent = __decorate([
    core_1.Component({
        selector: 'app-order-center',
        template: __webpack_require__("../../../../../src/app/skeleton/order-center/order-center.component.html"),
        styles: [__webpack_require__("../../../../../src/app/skeleton/order-center/order-center.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof item_service_1.ItemService !== "undefined" && item_service_1.ItemService) === "function" && _a || Object, typeof (_b = typeof basket_service_1.BasketService !== "undefined" && basket_service_1.BasketService) === "function" && _b || Object])
], OrderCenterComponent);
exports.OrderCenterComponent = OrderCenterComponent;
var Model = (function () {
    function Model(name, street) {
        this.name = name;
        this.street = street;
    }
    return Model;
}());
exports.Model = Model;
var _a, _b;
//# sourceMappingURL=order-center.component.js.map

/***/ }),

/***/ "../../../../../src/app/skeleton/shop-center/item/item.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".btnplus{\r\n    width:90px;\r\n    background-color:lightgreen;\r\n}\r\n.btnplus:hover{\r\n    background-color:#A0FEA0 ;\r\n    border:1px solid green;\r\n}\r\n\r\n.btnminus{\r\n    width:90px;\r\n    background-color:lightcoral;\r\n}\r\n\r\n.btnminus:hover{\r\n    background-color:#FF9090 ;\r\n    border:1px solid red;\r\n}   \r\n\r\n.card{\r\n    margin: 5px 0;\r\n}\r\n\r\n.card button{\r\n    margin: 5px;\r\n} \r\n\r\n.card-outline{\r\n    background-color: transparent;\r\n    border-color: #009ce0;\r\n}\r\n.cardDescription{\r\n    height:3em;\r\n}\r\n\r\n.hidden{\r\n    visibility: hidden;\r\n}\r\n\r\n.imagebtn:hover{\r\n    background-color: lightskyblue;\r\n    border-color: #7DA8F6;\r\n}\r\n\r\n.imagebtn{\r\n    border-color: lightskyblue;\r\n}\r\n\r\n.promo{\r\n    font-weight: bold;\r\n    color:darkred;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/skeleton/shop-center/item/item.component.html":
/***/ (function(module, exports) {

module.exports = "<section class=\"card card-outline w-100\" style=\"width: 20rem;\">\n  <h4 class=\"card-header\">\n      {{itemObject.name | uppercase}}\n  </h4>\n  <div class=\"card-block row\">\n    <div class=\"col-xs-12 col-sm-7 \">\n      <p class=\"card-text cardDescription\">{{itemObject.description | uppercase}}</p>\n      <p class=\"card-text\">Cena: <span [ngClass]=\"{'promo':itemObject.sale}\">{{itemObject.price | currency:'USD':true}} </span> <span *ngIf=\"itemObject.sale\" class=\"promo\"> Promocja !!</span></p>\n      <p class=\"card-text\" *ngIf=\"itemObject.order\">W koszyku: <span class=\"itemOrderNr\">{{itemObject.order}} </span> szt.</p>\n    </div>\n    <div class=\"col-xs-12 col-sm-5 text-center\">\n      <div class=col-sm-12>\n        <button [ngClass]=\"{'hidden':itemObject.order<=0}\"  (click)=\"subtract(itemObject)\" class='btn btnminus'>Usuń</button>\n        <button (click)=\"add(itemObject)\" class='btn btnplus'>Dodaj</button>\n      </div> \n      <div *ngIf=\"itemObject.imageExist\" class=col-sm-12 align=left>\n        <button (click)=\"openBox({'id':itemObject.id,'name':itemObject.name})\" class='btn btn-xs imagebtn'><i class=\"fa fa-picture-o\" aria-hidden=\"true\"></i></button>\n      </div> \n    </div>\n  </div>\n</section>"

/***/ }),

/***/ "../../../../../src/app/skeleton/shop-center/item/item.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var itemClass_1 = __webpack_require__("../../../../../src/app/skeleton/shop-center/item/itemClass.ts");
var basket_service_1 = __webpack_require__("../../../../../src/app/services/basket.service.ts");
var ItemComponent = (function () {
    function ItemComponent(basketService) {
        this.basketService = basketService;
        this.addOut = new core_1.EventEmitter();
        this.subtractOut = new core_1.EventEmitter();
        this.openWindow = new core_1.EventEmitter();
    }
    ItemComponent.prototype.ngOnInit = function () {
    };
    ItemComponent.prototype.add = function (Item) {
        this.addOut.emit(Item);
        this.blink();
        this.basketService.publishData(Item.price, 1);
    };
    ItemComponent.prototype.blink = function () {
        for (var i = 0; i < 2; i++) {
            $('.itemOrderNr').fadeTo('fast', 0.5).fadeTo('fast', 1.0);
        }
    };
    ItemComponent.prototype.subtract = function (Item) {
        if (Item.order > 0) {
            this.subtractOut.emit(Item);
            this.blink();
            this.basketService.publishData(Item.price, -1);
        }
    };
    ItemComponent.prototype.openBox = function (data) {
        this.openWindow.emit(data);
    };
    return ItemComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", typeof (_a = typeof itemClass_1.Item !== "undefined" && itemClass_1.Item) === "function" && _a || Object)
], ItemComponent.prototype, "itemObject", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], ItemComponent.prototype, "addOut", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], ItemComponent.prototype, "subtractOut", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], ItemComponent.prototype, "openWindow", void 0);
ItemComponent = __decorate([
    core_1.Component({
        selector: 'app-item',
        template: __webpack_require__("../../../../../src/app/skeleton/shop-center/item/item.component.html"),
        styles: [__webpack_require__("../../../../../src/app/skeleton/shop-center/item/item.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof basket_service_1.BasketService !== "undefined" && basket_service_1.BasketService) === "function" && _b || Object])
], ItemComponent);
exports.ItemComponent = ItemComponent;
var _a, _b;
//# sourceMappingURL=item.component.js.map

/***/ }),

/***/ "../../../../../src/app/skeleton/shop-center/item/itemClass.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Item = (function () {
    function Item(id, name, description, price, category, order, imageExist, sale, oldPrice) {
        if (id === void 0) { id = 0; }
        if (name === void 0) { name = ""; }
        if (description === void 0) { description = ""; }
        if (price === void 0) { price = 0; }
        if (category === void 0) { category = ""; }
        if (order === void 0) { order = 0; }
        if (imageExist === void 0) { imageExist = false; }
        if (sale === void 0) { sale = false; }
        if (oldPrice === void 0) { oldPrice = 0; }
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.category = category;
        this.order = order;
        this.imageExist = imageExist;
        this.sale = sale;
        this.oldPrice = oldPrice;
    }
    return Item;
}());
exports.Item = Item;
//# sourceMappingURL=itemClass.js.map

/***/ }),

/***/ "../../../../../src/app/skeleton/shop-center/pagination-component/pagination-component.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".pagination{\r\n    margin-bottom:20px;\r\n}\r\n.pagination button{\r\n    margin:0px 2px;\r\n}\r\n\r\n.pagination button:hover:not(.btn-primary){\r\n    background-color: lightskyblue;\r\n}\r\n\r\n.pagination button.disabled:hover{\r\n    background-color: #FFAAAA;\r\n}\r\n\r\n.btn-primary{\r\n    background-color: #009ce0;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/skeleton/shop-center/pagination-component/pagination-component.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"pagination\" *ngIf=\"count > 0\">\n\t<div class=\"numbers\">\n\t\t<button class=\"btn\" (click)=\"onPrev()\" [disabled]=\"page === 1\" [ngClass]=\"{ 'disabled': page === 1}\">&lt; Previous</button>\n\t\t<button\n\t\t\tclass='btn'\n\t\t\t*ngFor=\"let pageNum of getPages()\"\n\t\t\t(click)=\"onPage(pageNum)\"\n\t\t\t[ngClass]=\"{'btn-primary': pageNum === page}\">{{ pageNum }}</button>\n\t\t<button class=\"btn\" (click)=\"onNext(true)\" [disabled]=\"lastPage()\" [ngClass]=\"{ 'disabled': lastPage()}\">Next &gt;</button>\n\t</div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/skeleton/shop-center/pagination-component/pagination-component.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var PaginationComponentComponent = (function () {
    function PaginationComponentComponent() {
        this.goPrev = new core_1.EventEmitter();
        this.goNext = new core_1.EventEmitter();
        this.goPage = new core_1.EventEmitter();
    }
    PaginationComponentComponent.prototype.ngOnInit = function () {
    };
    PaginationComponentComponent.prototype.getMin = function () {
        return ((this.perPage * this.page) - this.perPage) + 1;
    };
    PaginationComponentComponent.prototype.getMax = function () {
        var max = this.perPage * this.page;
        if (max > this.count) {
            max = this.count;
        }
        return max;
    };
    PaginationComponentComponent.prototype.onPage = function (n) {
        this.goPage.emit(n);
    };
    PaginationComponentComponent.prototype.onPrev = function () {
        this.goPrev.emit(true);
    };
    PaginationComponentComponent.prototype.onNext = function (next) {
        this.goNext.emit(next);
    };
    PaginationComponentComponent.prototype.totalPages = function () {
        return Math.ceil(this.count / this.perPage) || 0;
    };
    PaginationComponentComponent.prototype.lastPage = function () {
        return this.perPage * this.page >= this.count;
    };
    PaginationComponentComponent.prototype.getPages = function () {
        var c = Math.ceil(this.count / this.perPage);
        var p = this.page || 1;
        var pagesToShow = this.pagesToShow || 9;
        var pages = [];
        pages.push(p);
        var times = pagesToShow - 1;
        for (var i = 0; i < times; i++) {
            if (pages.length < pagesToShow) {
                if (Math.min.apply(null, pages) > 1) {
                    pages.push(Math.min.apply(null, pages) - 1);
                }
            }
            if (pages.length < pagesToShow) {
                if (Math.max.apply(null, pages) < c) {
                    pages.push(Math.max.apply(null, pages) + 1);
                }
            }
        }
        pages.sort(function (a, b) { return a - b; });
        return pages;
    };
    return PaginationComponentComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], PaginationComponentComponent.prototype, "page", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], PaginationComponentComponent.prototype, "count", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], PaginationComponentComponent.prototype, "perPage", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], PaginationComponentComponent.prototype, "pagesToShow", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], PaginationComponentComponent.prototype, "goPrev", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], PaginationComponentComponent.prototype, "goNext", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], PaginationComponentComponent.prototype, "goPage", void 0);
PaginationComponentComponent = __decorate([
    core_1.Component({
        selector: 'app-pagination-component',
        template: __webpack_require__("../../../../../src/app/skeleton/shop-center/pagination-component/pagination-component.component.html"),
        styles: [__webpack_require__("../../../../../src/app/skeleton/shop-center/pagination-component/pagination-component.component.css")]
    }),
    __metadata("design:paramtypes", [])
], PaginationComponentComponent);
exports.PaginationComponentComponent = PaginationComponentComponent;
//# sourceMappingURL=pagination-component.component.js.map

/***/ }),

/***/ "../../../../../src/app/skeleton/shop-center/shop-center.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".modal {\r\n    display: none;\r\n    position: fixed; \r\n    z-index: 1; \r\n    padding-top: 100px;\r\n    left: 0;\r\n    top: 0;\r\n    width: 100%; \r\n    height: 100%; \r\n    overflow: auto; \r\n    background-color: rgb(0,0,0); \r\n    background-color: rgba(0,0,0,0.4); \r\n}\r\n\r\n.modal-content {\r\n    background-color: #fefefe;\r\n    margin: 15% auto;\r\n    padding: 20px;\r\n    border: 1px solid #888;\r\n    width: 80%; \r\n}\r\n\r\n.modal-header {\r\n    padding: 10px 16px 5px 16px;\r\n    background-color: lightskyblue;\r\n    color: white;\r\n}\r\n\r\n.modal-body {padding: 2px 16px;}\r\n\r\n.modal-content {\r\n    position: relative;\r\n    background-color: #fefefe;\r\n    margin: auto;\r\n    padding: 0;\r\n    border: 1px solid #888;\r\n    width: 80%;\r\n    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);\r\n    -webkit-animation-name: animatetop;\r\n    -webkit-animation-duration: 0.4s;\r\n    animation-name: animatetop;\r\n    animation-duration: 0.4s\r\n}\r\n\r\n.headerModalText{\r\n    font-size: 1.1em;\r\n}\r\n\r\n@-webkit-keyframes animatetop {\r\n    from {top: -300px; opacity: 0} \r\n    to {top: 0; opacity: 1}\r\n}\r\n\r\n@keyframes animatetop {\r\n    from {top: -300px; opacity: 0}\r\n    to {top: 0; opacity: 1}\r\n}\r\n\r\n.close {\r\n    color: #aaa;\r\n    float: right;\r\n    font-size: 28px;\r\n    font-weight: bold;\r\n}\r\n\r\n.close:hover,\r\n.close:focus {\r\n    color: black;\r\n    text-decoration: none;\r\n    cursor: pointer;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/skeleton/shop-center/shop-center.component.html":
/***/ (function(module, exports) {

module.exports = "<p *ngIf=\"errorShow!=''\" >{{errorShow}}</p>\n<app-item *ngFor=\"let itemObject of itemsObjects;\" [itemObject]='itemObject' (openWindow)=\"open($event)\"  (subtractOut)=\"subtract($event)\" (addOut)=\"add($event)\">\n</app-item>\n<app-pagination-component  (goPage)=\"goToPage($event)\"\n    (goNext)=\"onNext()\"\n    (goPrev)=\"onPrev()\"\n    [pagesToShow]=\"2\"\n    [page]=\"page\"\n    [perPage]=\"3\"\n    [count]=\"total\">\n</app-pagination-component>\n\n\n<div id=\"myModal\" class=\"modal\">\n    <div class=\"modal-content\">\n        <div class=\"modal-header\">\n            <span (click)=\"close()\" class=\"close\">&times;</span>\n            <span class=\"headerModalText\">{{nameProduct}}</span>\n        </div>\n        <div class=\"modal-body\">\n            <img style=\"width:100%; height:auto;\" alt=\"Zdjęcie podglądowe\" src={{image}}/>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/skeleton/shop-center/shop-center.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var item_service_1 = __webpack_require__("../../../../../src/app/services/item.service.ts");
var categoryservice_service_1 = __webpack_require__("../../../../../src/app/services/categoryservice.service.ts");
var user_service_1 = __webpack_require__("../../../../../src/app/services/user.service.ts");
__webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
var ShopCenterComponent = (function () {
    function ShopCenterComponent(userService, itemService, categoryserviceService) {
        var _this = this;
        this.userService = userService;
        this.itemService = itemService;
        this.categoryserviceService = categoryserviceService;
        this.itemsObjects = [];
        this.total = 0;
        this.page = 1;
        this.limit = 3;
        this.filterString = '';
        this.filter = [];
        this.filterPrice = [0, 1000000];
        this.errorShow = '';
        this.categoryserviceService.caseNumber$.subscribe(function (data) {
            _this.page = 1;
            _this.filter = data;
            _this.getItems();
        });
        this.categoryserviceService.caseFilterString$.subscribe(function (data) {
            _this.page = 1;
            _this.filterString = data;
            _this.getItems();
        });
        this.categoryserviceService.caseFilterPrice$.subscribe(function (data) {
            _this.page = 1;
            _this.filterPrice = data;
            _this.getItems();
        });
        this.itemService.endInitialization$.subscribe(function (dataError) {
            _this.page = 1;
            _this.getItems();
            if (dataError) {
                _this.errorShow = "Błąd serwera";
            }
        });
    }
    ShopCenterComponent.prototype.ngOnInit = function () {
        this.getItems();
    };
    ShopCenterComponent.prototype.getItems = function () {
        var itemsObjectsTemp = this.itemService.getItemsService({ page: this.page, limit: this.limit, filter: this.filter, filterRe: this.filterString, filterPrice: this.filterPrice });
        this.itemsObjects = itemsObjectsTemp['itemsObjects'];
        this.total = itemsObjectsTemp['total']; // liczba znalezionych produktów pasujących
    };
    ShopCenterComponent.prototype.goToPage = function (n) {
        this.page = n;
        this.getItems();
    };
    ShopCenterComponent.prototype.onNext = function () {
        this.page++;
        this.getItems();
    };
    ShopCenterComponent.prototype.onPrev = function () {
        this.page--;
        this.getItems();
    };
    ShopCenterComponent.prototype.add = function (itemObject) {
        itemObject.order += 1;
        this.saveBasket();
    };
    ShopCenterComponent.prototype.subtract = function (itemObject) {
        itemObject.order -= 1;
        this.saveBasket();
    };
    ShopCenterComponent.prototype.saveBasket = function () {
        if (this.userService.ifUserLogged()) {
            var basket = this.itemService.getBasket2Save();
            this.userService.saveBasket(basket);
        }
    };
    ShopCenterComponent.prototype.open = function (event) {
        var _this = this;
        this.nameProduct = event['name'];
        $('#myModal').show();
        this.itemService.getPhoto(event['id']).subscribe(function (data) {
            _this.image = data['image'];
        });
    };
    ShopCenterComponent.prototype.close = function () {
        $('#myModal').hide();
    };
    return ShopCenterComponent;
}());
ShopCenterComponent = __decorate([
    core_1.Component({
        selector: 'app-shop-center',
        template: __webpack_require__("../../../../../src/app/skeleton/shop-center/shop-center.component.html"),
        styles: [__webpack_require__("../../../../../src/app/skeleton/shop-center/shop-center.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof user_service_1.UserService !== "undefined" && user_service_1.UserService) === "function" && _a || Object, typeof (_b = typeof item_service_1.ItemService !== "undefined" && item_service_1.ItemService) === "function" && _b || Object, typeof (_c = typeof categoryservice_service_1.CategoryserviceService !== "undefined" && categoryservice_service_1.CategoryserviceService) === "function" && _c || Object])
], ShopCenterComponent);
exports.ShopCenterComponent = ShopCenterComponent;
var _a, _b, _c;
//# sourceMappingURL=shop-center.component.js.map

/***/ }),

/***/ "../../../../../src/app/sklepInternetowyMain.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/sklepInternetowyMain.component.html":
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<div class=\"container-fluid\">\n  <app-header (pageMain)=\"changePage($event)\"></app-header>\n  <app-nav-bar (pageMain)=\"changePage($event)\" [page]='pageMain'></app-nav-bar>\n  <div *ngIf='pageMain==\"shop\"' class=\"row\">\n    <app-filter-bar class=\"col-xs-12 col-sm-12 col-md-3\"></app-filter-bar>\n    <app-shop-center class=\"col-xs-12 col-sm-12 col-md-9\" ></app-shop-center>\n  </div>\n  <div *ngIf='pageMain==\"basket\"' class=\"row\">\n      <app-basket-center class=\"col-12\" (pageMain)=\"changePage($event)\"></app-basket-center>\n  </div>\n  <div *ngIf='pageMain==\"ordering\"' class=\"row\">\n      <app-order-center class=\"col-12\" (pageMain)=\"changePage($event)\"></app-order-center>\n  </div>\n  <div *ngIf='pageMain==\"admin\"' class=\"row\">\n    <app-administrator class=\"col-12\" (pageMain)=\"changePage($event)\"></app-administrator>\n</div>\n</div>\n\n\n"

/***/ }),

/***/ "../../../../../src/app/sklepInternetowyMain.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var SklepInternetowyMainComponent = (function () {
    //pageMain = 'ordering';
    //pageMain = 'basket';
    //pageMain = 'admin';
    function SklepInternetowyMainComponent() {
        this.pageMain = 'shop';
    }
    SklepInternetowyMainComponent.prototype.changePage = function (page) {
        this.pageMain = page;
    };
    return SklepInternetowyMainComponent;
}());
SklepInternetowyMainComponent = __decorate([
    core_1.Component({
        selector: 'main',
        template: __webpack_require__("../../../../../src/app/sklepInternetowyMain.component.html"),
        styles: [__webpack_require__("../../../../../src/app/sklepInternetowyMain.component.css")]
    }),
    __metadata("design:paramtypes", [])
], SklepInternetowyMainComponent);
exports.SklepInternetowyMainComponent = SklepInternetowyMainComponent;
//# sourceMappingURL=sklepInternetowyMain.component.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/@angular/core.es5.js");
var platform_browser_dynamic_1 = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
var app_module_1 = __webpack_require__("../../../../../src/app/app.module.ts");
var environment_1 = __webpack_require__("../../../../../src/environments/environment.ts");
if (environment_1.environment.production) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(app_module_1.AppModule)
    .catch(function (err) { return console.log(err); });
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map