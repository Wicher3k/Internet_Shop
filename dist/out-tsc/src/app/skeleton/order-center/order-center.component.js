"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var item_service_1 = require("./../../services/item.service");
var basket_service_1 = require("./../../services/basket.service");
var OrderCenterComponent = (function () {
    function OrderCenterComponent(itemService, basketService) {
        this.itemService = itemService;
        this.basketService = basketService;
        this.pageMain = new core_1.EventEmitter();
        this.submitted = false;
        this.model = new Model('', '');
        this.itemsOrder = {};
        this.itemsSum = 0;
    }
    OrderCenterComponent.prototype.ngOnInit = function () {
        this.getbasket();
    };
    OrderCenterComponent.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        this.itemService.sendOrder(this.model, this.itemsSum, this.itemsOrder).subscribe(function (res) {
            _this.itemService.resetShopContent();
            _this.basketService.publishData('reset');
        }, function (error) {
            console.log(error);
        });
    };
    OrderCenterComponent.prototype.getbasket = function () {
        this.itemsBasket = this.itemService.getBasketService();
        for (var _i = 0, _a = this.itemsBasket; _i < _a.length; _i++) {
            var item = _a[_i];
            this.itemsOrder[item.id.toString()] = {};
            this.itemsOrder[item.id.toString()]['number'] = item.order;
            this.itemsOrder[item.id.toString()]['price'] = item.price;
            this.itemsSum += item.order * item.price;
        }
    };
    OrderCenterComponent.prototype.backPage = function () {
        this.pageMain.emit('shop');
    };
    return OrderCenterComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], OrderCenterComponent.prototype, "pageMain", void 0);
OrderCenterComponent = __decorate([
    core_1.Component({
        selector: 'app-order-center',
        templateUrl: './order-center.component.html',
        styleUrls: ['./order-center.component.css']
    }),
    __metadata("design:paramtypes", [item_service_1.ItemService, basket_service_1.BasketService])
], OrderCenterComponent);
exports.OrderCenterComponent = OrderCenterComponent;
var Model = (function () {
    function Model(name, street) {
        this.name = name;
        this.street = street;
    }
    return Model;
}());
exports.Model = Model;
//# sourceMappingURL=order-center.component.js.map