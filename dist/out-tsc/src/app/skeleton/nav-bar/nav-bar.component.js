"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var categoryservice_service_1 = require("./../../services/categoryservice.service");
var NavBarComponent = (function () {
    function NavBarComponent(categoryserviceService) {
        this.categoryserviceService = categoryserviceService;
        this.pageMain = new core_1.EventEmitter();
    }
    NavBarComponent.prototype.ngOnInit = function () { };
    NavBarComponent.prototype.categorySend = function (event) {
        this.categoryserviceService.publishDataCategoryString(event.target.value);
    };
    NavBarComponent.prototype.changePage = function (page) {
        this.pageMain.emit(page);
    };
    return NavBarComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], NavBarComponent.prototype, "pageMain", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", String)
], NavBarComponent.prototype, "page", void 0);
NavBarComponent = __decorate([
    core_1.Component({
        selector: 'app-nav-bar',
        templateUrl: './nav-bar.component.html',
        styleUrls: ['./nav-bar.component.css']
    }),
    __metadata("design:paramtypes", [categoryservice_service_1.CategoryserviceService])
], NavBarComponent);
exports.NavBarComponent = NavBarComponent;
//# sourceMappingURL=nav-bar.component.js.map