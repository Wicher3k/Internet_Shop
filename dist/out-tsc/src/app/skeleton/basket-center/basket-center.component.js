"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var item_service_1 = require("./../../services/item.service");
var basket_service_1 = require("./../../services/basket.service");
var user_service_1 = require("./../../services/user.service");
var BasketCenterComponent = (function () {
    function BasketCenterComponent(userService, itemService, basketService) {
        var _this = this;
        this.userService = userService;
        this.itemService = itemService;
        this.basketService = basketService;
        this.pageMain = new core_1.EventEmitter();
        this.itemsSum = 0;
        this.itemService.sale$.subscribe(function (data) {
            _this.getbasket();
        });
    }
    BasketCenterComponent.prototype.ngOnInit = function () {
        this.getbasket();
    };
    BasketCenterComponent.prototype.getbasket = function () {
        this.itemsBasket = this.itemService.getBasketService();
        for (var _i = 0, _a = this.itemsBasket; _i < _a.length; _i++) {
            var item = _a[_i];
            this.itemsSum += item.order * item.price;
        }
        this.saveBasket();
    };
    BasketCenterComponent.prototype.deleteItemInBasket = function (itemBasket) {
        this.basketService.publishData(itemBasket.price, -itemBasket.order);
        this.itemsSum -= itemBasket.order * itemBasket.price;
        itemBasket.order = 0;
        this.itemsBasket.splice(this.itemsBasket.indexOf(itemBasket), 1);
        this.itemService.reloadSiteHeader();
    };
    BasketCenterComponent.prototype.changePage = function (page) {
        this.pageMain.emit(page);
    };
    BasketCenterComponent.prototype.saveBasket = function () {
        if (this.userService.ifUserLogged()) {
            var basket = this.itemService.getBasket2Save();
            this.userService.saveBasket(basket);
        }
    };
    return BasketCenterComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], BasketCenterComponent.prototype, "pageMain", void 0);
BasketCenterComponent = __decorate([
    core_1.Component({
        selector: 'app-basket-center',
        templateUrl: './basket-center.component.html',
        styleUrls: ['./basket-center.component.css']
    }),
    __metadata("design:paramtypes", [user_service_1.UserService, item_service_1.ItemService, basket_service_1.BasketService])
], BasketCenterComponent);
exports.BasketCenterComponent = BasketCenterComponent;
//# sourceMappingURL=basket-center.component.js.map