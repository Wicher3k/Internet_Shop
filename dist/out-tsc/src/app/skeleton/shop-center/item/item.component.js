"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var itemClass_1 = require("./itemClass");
var basket_service_1 = require("./../../../services/basket.service");
var ItemComponent = (function () {
    function ItemComponent(basketService) {
        this.basketService = basketService;
        this.addOut = new core_1.EventEmitter();
        this.subtractOut = new core_1.EventEmitter();
        this.openWindow = new core_1.EventEmitter();
    }
    ItemComponent.prototype.ngOnInit = function () {
    };
    ItemComponent.prototype.add = function (Item) {
        this.addOut.emit(Item);
        this.blink();
        this.basketService.publishData(Item.price, 1);
    };
    ItemComponent.prototype.blink = function () {
        for (var i = 0; i < 2; i++) {
            $('.itemOrderNr').fadeTo('fast', 0.5).fadeTo('fast', 1.0);
        }
    };
    ItemComponent.prototype.subtract = function (Item) {
        if (Item.order > 0) {
            this.subtractOut.emit(Item);
            this.blink();
            this.basketService.publishData(Item.price, -1);
        }
    };
    ItemComponent.prototype.openBox = function (data) {
        this.openWindow.emit(data);
    };
    return ItemComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", itemClass_1.Item)
], ItemComponent.prototype, "itemObject", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], ItemComponent.prototype, "addOut", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], ItemComponent.prototype, "subtractOut", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], ItemComponent.prototype, "openWindow", void 0);
ItemComponent = __decorate([
    core_1.Component({
        selector: 'app-item',
        templateUrl: './item.component.html',
        styleUrls: ['./item.component.css']
    }),
    __metadata("design:paramtypes", [basket_service_1.BasketService])
], ItemComponent);
exports.ItemComponent = ItemComponent;
//# sourceMappingURL=item.component.js.map