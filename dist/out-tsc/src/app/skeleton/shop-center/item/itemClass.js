"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Item = (function () {
    function Item(id, name, description, price, category, order, imageExist, sale, oldPrice) {
        if (id === void 0) { id = 0; }
        if (name === void 0) { name = ""; }
        if (description === void 0) { description = ""; }
        if (price === void 0) { price = 0; }
        if (category === void 0) { category = ""; }
        if (order === void 0) { order = 0; }
        if (imageExist === void 0) { imageExist = false; }
        if (sale === void 0) { sale = false; }
        if (oldPrice === void 0) { oldPrice = 0; }
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.category = category;
        this.order = order;
        this.imageExist = imageExist;
        this.sale = sale;
        this.oldPrice = oldPrice;
    }
    return Item;
}());
exports.Item = Item;
//# sourceMappingURL=itemClass.js.map