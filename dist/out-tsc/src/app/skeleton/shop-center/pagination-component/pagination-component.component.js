"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var PaginationComponentComponent = (function () {
    function PaginationComponentComponent() {
        this.goPrev = new core_1.EventEmitter();
        this.goNext = new core_1.EventEmitter();
        this.goPage = new core_1.EventEmitter();
    }
    PaginationComponentComponent.prototype.ngOnInit = function () {
    };
    PaginationComponentComponent.prototype.getMin = function () {
        return ((this.perPage * this.page) - this.perPage) + 1;
    };
    PaginationComponentComponent.prototype.getMax = function () {
        var max = this.perPage * this.page;
        if (max > this.count) {
            max = this.count;
        }
        return max;
    };
    PaginationComponentComponent.prototype.onPage = function (n) {
        this.goPage.emit(n);
    };
    PaginationComponentComponent.prototype.onPrev = function () {
        this.goPrev.emit(true);
    };
    PaginationComponentComponent.prototype.onNext = function (next) {
        this.goNext.emit(next);
    };
    PaginationComponentComponent.prototype.totalPages = function () {
        return Math.ceil(this.count / this.perPage) || 0;
    };
    PaginationComponentComponent.prototype.lastPage = function () {
        return this.perPage * this.page >= this.count;
    };
    PaginationComponentComponent.prototype.getPages = function () {
        var c = Math.ceil(this.count / this.perPage);
        var p = this.page || 1;
        var pagesToShow = this.pagesToShow || 9;
        var pages = [];
        pages.push(p);
        var times = pagesToShow - 1;
        for (var i = 0; i < times; i++) {
            if (pages.length < pagesToShow) {
                if (Math.min.apply(null, pages) > 1) {
                    pages.push(Math.min.apply(null, pages) - 1);
                }
            }
            if (pages.length < pagesToShow) {
                if (Math.max.apply(null, pages) < c) {
                    pages.push(Math.max.apply(null, pages) + 1);
                }
            }
        }
        pages.sort(function (a, b) { return a - b; });
        return pages;
    };
    return PaginationComponentComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], PaginationComponentComponent.prototype, "page", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], PaginationComponentComponent.prototype, "count", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], PaginationComponentComponent.prototype, "perPage", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], PaginationComponentComponent.prototype, "pagesToShow", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], PaginationComponentComponent.prototype, "goPrev", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], PaginationComponentComponent.prototype, "goNext", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], PaginationComponentComponent.prototype, "goPage", void 0);
PaginationComponentComponent = __decorate([
    core_1.Component({
        selector: 'app-pagination-component',
        templateUrl: './pagination-component.component.html',
        styleUrls: ['./pagination-component.component.css']
    }),
    __metadata("design:paramtypes", [])
], PaginationComponentComponent);
exports.PaginationComponentComponent = PaginationComponentComponent;
//# sourceMappingURL=pagination-component.component.js.map