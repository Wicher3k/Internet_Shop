"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var item_service_1 = require("./../../services/item.service");
var categoryservice_service_1 = require("./../../services/categoryservice.service");
var user_service_1 = require("./../../services/user.service");
require("rxjs/add/operator/map");
var ShopCenterComponent = (function () {
    function ShopCenterComponent(userService, itemService, categoryserviceService) {
        var _this = this;
        this.userService = userService;
        this.itemService = itemService;
        this.categoryserviceService = categoryserviceService;
        this.itemsObjects = [];
        this.total = 0;
        this.page = 1;
        this.limit = 3;
        this.filterString = '';
        this.filter = [];
        this.filterPrice = [0, 1000000];
        this.errorShow = '';
        this.categoryserviceService.caseNumber$.subscribe(function (data) {
            _this.page = 1;
            _this.filter = data;
            _this.getItems();
        });
        this.categoryserviceService.caseFilterString$.subscribe(function (data) {
            _this.page = 1;
            _this.filterString = data;
            _this.getItems();
        });
        this.categoryserviceService.caseFilterPrice$.subscribe(function (data) {
            _this.page = 1;
            _this.filterPrice = data;
            _this.getItems();
        });
        this.itemService.endInitialization$.subscribe(function (dataError) {
            _this.page = 1;
            _this.getItems();
            if (dataError) {
                _this.errorShow = "Błąd serwera";
            }
        });
    }
    ShopCenterComponent.prototype.ngOnInit = function () {
        this.getItems();
    };
    ShopCenterComponent.prototype.getItems = function () {
        var itemsObjectsTemp = this.itemService.getItemsService({ page: this.page, limit: this.limit, filter: this.filter, filterRe: this.filterString, filterPrice: this.filterPrice });
        this.itemsObjects = itemsObjectsTemp['itemsObjects'];
        this.total = itemsObjectsTemp['total']; // liczba znalezionych produktów pasujących
    };
    ShopCenterComponent.prototype.goToPage = function (n) {
        this.page = n;
        this.getItems();
    };
    ShopCenterComponent.prototype.onNext = function () {
        this.page++;
        this.getItems();
    };
    ShopCenterComponent.prototype.onPrev = function () {
        this.page--;
        this.getItems();
    };
    ShopCenterComponent.prototype.add = function (itemObject) {
        itemObject.order += 1;
        this.saveBasket();
    };
    ShopCenterComponent.prototype.subtract = function (itemObject) {
        itemObject.order -= 1;
        this.saveBasket();
    };
    ShopCenterComponent.prototype.saveBasket = function () {
        if (this.userService.ifUserLogged()) {
            var basket = this.itemService.getBasket2Save();
            this.userService.saveBasket(basket);
        }
    };
    ShopCenterComponent.prototype.open = function (event) {
        var _this = this;
        this.nameProduct = event['name'];
        $('#myModal').show();
        this.itemService.getPhoto(event['id']).subscribe(function (data) {
            _this.image = data['image'];
        });
    };
    ShopCenterComponent.prototype.close = function () {
        $('#myModal').hide();
    };
    return ShopCenterComponent;
}());
ShopCenterComponent = __decorate([
    core_1.Component({
        selector: 'app-shop-center',
        templateUrl: './shop-center.component.html',
        styleUrls: ['./shop-center.component.css']
    }),
    __metadata("design:paramtypes", [user_service_1.UserService, item_service_1.ItemService, categoryservice_service_1.CategoryserviceService])
], ShopCenterComponent);
exports.ShopCenterComponent = ShopCenterComponent;
//# sourceMappingURL=shop-center.component.js.map