"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var user_service_1 = require("./../../../services/user.service");
var AccountRegisterComponent = (function () {
    function AccountRegisterComponent(userService) {
        this.userService = userService;
        this.submitted = false;
        this.model = new ModelUser('', '');
        this.mainPage = new core_1.EventEmitter();
    }
    AccountRegisterComponent.prototype.ngOnInit = function () {
    };
    AccountRegisterComponent.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        this.userService.register(this.model).subscribe(function (res) {
            _this.mainPage.emit('Stworzono pomyślnie użytkownika');
        }, function (error) {
            console.log(error);
        });
    };
    return AccountRegisterComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], AccountRegisterComponent.prototype, "mainPage", void 0);
AccountRegisterComponent = __decorate([
    core_1.Component({
        selector: 'app-account-register',
        templateUrl: './account-register.component.html',
        styleUrls: ['./account-register.component.css']
    }),
    __metadata("design:paramtypes", [user_service_1.UserService])
], AccountRegisterComponent);
exports.AccountRegisterComponent = AccountRegisterComponent;
var ModelUser = (function () {
    function ModelUser(name, password) {
        this.name = name;
        this.password = password;
    }
    return ModelUser;
}());
exports.ModelUser = ModelUser;
//# sourceMappingURL=account-register.component.js.map