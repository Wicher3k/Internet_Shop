"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var item_service_1 = require("./../../services/item.service");
var user_service_1 = require("./../../services/user.service");
var AccountComponent = (function () {
    function AccountComponent(itemService, userService) {
        this.itemService = itemService;
        this.userService = userService;
        this.model = new user_service_1.ModelAuthenticate('', '');
        this.error = false;
        this.login = 'login';
    }
    AccountComponent.prototype.ngOnInit = function () {
        this.model = this.userService.getUser();
        if (this.model.username != '' && this.model.functionAccount > 0) {
            if (this.model.functionAccount < 10) {
                this.login = 'loggedUser';
            }
            else {
                this.login = 'loggedAdmin';
            }
        }
    };
    AccountComponent.prototype.onSubmit = function () {
        var _this = this;
        var ifAutenticated = this.userService.authenticate(this.model);
        ifAutenticated.subscribe(function (data) {
            _this.userService.setUser(_this.model, data['function'], data['token'], data['userId']);
            _this.itemService.setBasket(data['basketSaved']);
            if (data['function'] < 10) {
                _this.login = 'loggedUser';
            }
            else {
                _this.login = 'loggedAdmin';
            }
        }, function (err) {
            _this.error = true;
            console.log("error", err);
        });
    };
    AccountComponent.prototype.displayFormPage = function (event) {
        this.login = 'login';
        if (event != undefined) {
            this.information = event;
            $('.fixedAlert.success').show();
            $('.fixedAlert.success').fadeOut(4000);
        }
    };
    AccountComponent.prototype.registerPage = function () {
        this.login = 'register';
    };
    return AccountComponent;
}());
AccountComponent = __decorate([
    core_1.Component({
        selector: 'app-administrator',
        //providers:[ItemService],
        templateUrl: './account.component.html',
        styleUrls: ['./account.component.css']
    }),
    __metadata("design:paramtypes", [item_service_1.ItemService, user_service_1.UserService])
], AccountComponent);
exports.AccountComponent = AccountComponent;
//# sourceMappingURL=account.component.js.map