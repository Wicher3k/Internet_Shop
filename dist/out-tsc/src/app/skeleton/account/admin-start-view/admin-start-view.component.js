"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var user_service_1 = require("./../../../services/user.service");
var AdminStartViewComponent = (function () {
    function AdminStartViewComponent(userService) {
        this.userService = userService;
        this.logoutPage = new core_1.EventEmitter();
        this.pageAdmin = "viewAdmin";
    }
    AdminStartViewComponent.prototype.ngOnInit = function () { };
    AdminStartViewComponent.prototype.changePage = function (page) {
        this.pageAdmin = page;
    };
    AdminStartViewComponent.prototype.logout = function () {
        var _this = this;
        this.userService.logout().subscribe(function (res) {
            _this.logoutPage.emit();
        }, function (error) {
            console.log(error);
        });
    };
    return AdminStartViewComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], AdminStartViewComponent.prototype, "logoutPage", void 0);
AdminStartViewComponent = __decorate([
    core_1.Component({
        selector: 'app-admin-start-view',
        templateUrl: './admin-start-view.component.html',
        styleUrls: ['./admin-start-view.component.css', '../accounts.css']
    }),
    __metadata("design:paramtypes", [user_service_1.UserService])
], AdminStartViewComponent);
exports.AdminStartViewComponent = AdminStartViewComponent;
//# sourceMappingURL=admin-start-view.component.js.map