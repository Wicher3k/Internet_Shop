"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var item_service_1 = require("./../../../../../services/item.service");
var categoryservice_service_1 = require("./../../../../../services/categoryservice.service");
var ProductsEditComponent = (function () {
    function ProductsEditComponent(itemService, categoryserviceService) {
        this.itemService = itemService;
        this.categoryserviceService = categoryserviceService;
        this.pageReturn = new core_1.EventEmitter();
        this.categories = [];
        this.buttonName = "Dodaj";
        this.imageSrc = '';
        this.image = '';
        this.categories = [];
        this.categories = this.categoryserviceService.categories.slice();
        for (var _i = 0, _a = this.categoryserviceService.categoriesGroups; _i < _a.length; _i++) {
            var categoriesArray = _a[_i];
            for (var _b = 0, categoriesArray_1 = categoriesArray; _b < categoriesArray_1.length; _b++) {
                var category = categoriesArray_1[_b];
                this.categories.push(category);
            }
        }
    }
    ;
    ProductsEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.product.sale = this.itemService.ifProductSale(this.product.id);
        if (this.product.name != '') {
            this.buttonName = "Edytuj";
        }
        this.itemService.getPhoto(this.product.id).subscribe(function (data) {
            _this.image = data['image'];
        });
        this.productForm = new forms_1.FormGroup({
            'name': new forms_1.FormControl(this.product.name, [
                forms_1.Validators.required,
            ]),
            'description': new forms_1.FormControl(this.product.description, forms_1.Validators.required),
            'price': new forms_1.FormControl(this.product.price, forms_1.Validators.required),
            'category': new forms_1.FormControl(this.product.category, forms_1.Validators.required)
        });
    };
    ProductsEditComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.product.saleValue > 100) {
            this.product.saleValue = 100;
        }
        else if (this.product.saleValue < 0) {
            this.product.saleValue = 0;
        }
        if (this.product.saleTime < 0) {
            this.product.saleTime = 0;
        }
        var reader = new FileReader();
        var fileBrowser = this.fileInput.nativeElement;
        if (fileBrowser.files && fileBrowser.files[0]) {
            reader.readAsDataURL(fileBrowser.files[0]);
            reader.onload = function () {
                _this.imageSrc = reader.result;
                _this.itemService.addOrEditProduct(_this.product, _this.buttonName, _this.imageSrc).subscribe(function (res) {
                    _this.pageReturn.emit('Proces zakończono pomyślnie');
                }, function (error) { console.log(error); });
            };
        }
        else {
            this.itemService.addOrEditProduct(this.product, this.buttonName, undefined).subscribe(function (res) {
                _this.pageReturn.emit('Proces zakończono pomyślnie');
            }, function (error) { console.log(error); });
        }
    };
    ProductsEditComponent.prototype.goBack = function () {
        this.pageReturn.emit();
    };
    return ProductsEditComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], ProductsEditComponent.prototype, "product", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], ProductsEditComponent.prototype, "pageReturn", void 0);
__decorate([
    core_1.ViewChild('fileInput'),
    __metadata("design:type", core_1.ElementRef)
], ProductsEditComponent.prototype, "fileInput", void 0);
ProductsEditComponent = __decorate([
    core_1.Component({
        selector: 'app-products-edit',
        templateUrl: './products-edit.component.html',
        styleUrls: ['./products-edit.component.css']
    }),
    __metadata("design:paramtypes", [item_service_1.ItemService, categoryservice_service_1.CategoryserviceService])
], ProductsEditComponent);
exports.ProductsEditComponent = ProductsEditComponent;
//# sourceMappingURL=products-edit.component.js.map