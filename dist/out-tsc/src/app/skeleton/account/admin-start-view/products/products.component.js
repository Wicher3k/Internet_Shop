"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var itemClass_1 = require("./../../../shop-center/item/itemClass");
var item_service_1 = require("./../../../../services/item.service");
var index_1 = require("angular-4-data-table/src/index");
var ProductsComponent = (function () {
    function ProductsComponent(itemService) {
        this.itemService = itemService;
        this.pageAdmin = new core_1.EventEmitter();
        this.items = [];
        this.itemCount = 0;
        this.subPageAdmin = 'table';
        this.information = '';
        this.edited = false;
        this.products = [];
        this.itemResource = new index_1.DataTableResource(this.products);
    }
    ProductsComponent.prototype.ngOnInit = function () {
        var _this = this;
        var productObservable = this.itemService.getProducts();
        this.itemResource = new index_1.DataTableResource(this.products);
        productObservable.subscribe(function (res) {
            for (var _i = 0, res_1 = res; _i < res_1.length; _i++) {
                var row = res_1[_i];
                _this.products.push(new itemClass_1.Item(row['_id'], row['name'], row['description'], row['price'], row['category'], 0, row['imageExist']));
            }
            _this.itemResource.count().then(function (count) { return _this.itemCount = count; });
            _this.reloadItems({ sortBy: "name", sortAsc: true, offset: 0, limit: 10 });
        }, function (error) {
            console.log(error);
        });
    };
    ProductsComponent.prototype.ngAfterViewChecked = function () {
        var sidePanel = this.tableChild.nativeElement.querySelectorAll('.panel-default');
        if (sidePanel.length > 0) {
            sidePanel[0].style.backgroundColor = 'white';
        }
        var icons = this.tableChild.nativeElement.querySelectorAll('.glyphicon-refresh');
        if (icons.length > 0) {
            icons[0].style.backgroundColor = 'white';
        }
        if (!this.edited) {
            $('.refresh-button').append('<i class="fa fa-refresh" aria-hidden="true"></i>');
            $('.column-selector-button').append('<i class="fa fa-list" aria-hidden="true"></i>');
            this.edited = true;
        }
    };
    ProductsComponent.prototype.changePage = function () {
        this.pageAdmin.emit('viewAdmin');
    };
    ProductsComponent.prototype.showInfoAndReload = function (event) {
        if (event != '' && event != undefined) {
            $('.alert.fadeBlock').show();
            this.information = event;
            this.subPageAdmin = 'table';
            $('.alert.fadeBlock').fadeOut(4000);
        }
        this.subPageAdmin = 'table';
        this.products = [];
        this.ngOnInit();
    };
    ProductsComponent.prototype.reloadItems = function (params) {
        var _this = this;
        this.itemResource.query(params).then(function (items) { return _this.items = items; });
    };
    ProductsComponent.prototype.productEdit = function (product) {
        this.subPageAdmin = "edit";
        this.editProduct = product;
    };
    ProductsComponent.prototype.productNew = function () {
        this.subPageAdmin = "edit";
        this.editProduct = new itemClass_1.Item(0, '', '', 0, '', 0, false);
    };
    ProductsComponent.prototype.deleteChecked = function () {
        var _this = this;
        var idDeleteArray = [];
        for (var _i = 0, _a = this.productTable.selectedRows; _i < _a.length; _i++) {
            var selected = _a[_i];
            idDeleteArray.push(selected.item.id);
        }
        var arrlen = idDeleteArray.length;
        var index = 0;
        if (idDeleteArray.length < 1) {
            return;
        }
        for (var _b = 0, idDeleteArray_1 = idDeleteArray; _b < idDeleteArray_1.length; _b++) {
            var row = idDeleteArray_1[_b];
            var observer = this.itemService.deleteProduct(row).subscribe(function (res) {
                index++;
                if (arrlen == index) {
                    _this.showInfoAndReload('Proces usuwania zakończono pomyślnie');
                }
            }, function (error) {
                console.log(error);
            });
        }
    };
    return ProductsComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], ProductsComponent.prototype, "pageAdmin", void 0);
__decorate([
    core_1.ViewChild(index_1.DataTable),
    __metadata("design:type", index_1.DataTable)
], ProductsComponent.prototype, "productTable", void 0);
__decorate([
    core_1.ViewChild('tableChild', { read: core_1.ElementRef }),
    __metadata("design:type", core_1.ElementRef)
], ProductsComponent.prototype, "tableChild", void 0);
ProductsComponent = __decorate([
    core_1.Component({
        selector: 'app-products',
        templateUrl: './products.component.html',
        styleUrls: ['./products.component.css', '../../accounts.css'],
        encapsulation: core_1.ViewEncapsulation.None // css dla potomków
    }),
    __metadata("design:paramtypes", [item_service_1.ItemService])
], ProductsComponent);
exports.ProductsComponent = ProductsComponent;
//# sourceMappingURL=products.component.js.map