"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var angular_4_data_table_1 = require("angular-4-data-table");
var item_service_1 = require("./../../../../services/item.service");
var itemClass_1 = require("./../../../shop-center/item/itemClass");
var OrdersComponent = (function () {
    function OrdersComponent(itemService) {
        this.itemService = itemService;
        this.pageAdmin = new core_1.EventEmitter();
        this.items = [];
        this.itemCount = 0;
        this.edited = false;
        this.objectKeys = Object.keys;
        this.statusArray = {}; // poprawić  i w urzytkowniku
        this.orders = [];
        this.displayMoreInfo = false;
        this.information = '';
        this.itemResource = new angular_4_data_table_1.DataTableResource(this.orders);
    }
    OrdersComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.itemService.getOrderStatuses().subscribe(function (res) {
            for (var i in res) {
                _this.statusArray[res[i]['id']] = res[i]['name'];
            }
        }, function (error) {
            console.log(error);
        });
        var orderObservable = this.itemService.getOrders();
        orderObservable.subscribe(function (res) {
            for (var i in res) {
                _this.orders.push(new Order(res[i]['_id'], res[i]['name'], res[i]['street'], res[i]['sumPrice'], res[i]['orderItems'], _this.statusArray[res[i]['status']], res[i]['status'], res[i]['dateOrder']));
            }
            _this.itemResource.count().then(function (count) { return _this.itemCount = count; });
            _this.reloadItems({ sortBy: "name", sortAsc: true, offset: 0, limit: 10 });
        }, function (error) {
            console.log(error);
        });
    };
    OrdersComponent.prototype.ngAfterViewChecked = function () {
        var sidePanel = this.tableChild.nativeElement.querySelectorAll('.panel-default');
        if (sidePanel.length > 0) {
            sidePanel[0].style.backgroundColor = 'white';
        }
        var icons = this.tableChild.nativeElement.querySelectorAll('.glyphicon-refresh');
        if (icons.length > 0) {
            icons[0].style.backgroundColor = 'white';
        }
        if (!this.edited) {
            $('.refresh-button').append('<i class="fa fa-refresh" aria-hidden="true"></i>');
            $('.column-selector-button').append('<i class="fa fa-list" aria-hidden="true"></i>');
            this.edited = true;
        }
    };
    OrdersComponent.prototype.showMoreInfo = function (order) {
        var _this = this;
        this.displayMoreInfo = false;
        this.orderActive = undefined;
        var index = 0;
        var count = Object.keys(order.orderItems).length;
        this.productsOrder = {};
        for (var id in order.orderItems) {
            var orderItems = this.itemService.getItemsFromId(id);
            orderItems.subscribe(function (res) {
                _this.productsOrder[res['_id']] = new itemClass_1.Item(res['_id'], res['name'], res['description'], res['price'], res['category'], 0, res['imageExist']);
                index++;
                if (index == count) {
                    _this.displayMoreInfo = true;
                }
            }, function (error) {
                console.log(error);
            });
        }
        this.chosenOptionStatus = order.statusId;
        this.orderActive = order;
        window.scrollTo(0, document.body.scrollHeight + 100);
    };
    OrdersComponent.prototype.changePage = function () {
        this.pageAdmin.emit('viewAdmin');
    };
    OrdersComponent.prototype.reloadItems = function (params) {
        var _this = this;
        this.itemResource.query(params).then(function (items) { return _this.items = items; });
    };
    OrdersComponent.prototype.changeStatus = function (orderId) {
        var _this = this;
        this.itemService.putOrder(orderId, this.chosenOptionStatus).subscribe(function (res) {
            //this.pageReturn.emit('Proces zakończono pomyślnie');
            $('html,body').scrollTop(0);
            $('.alert.fadeBlock').show();
            _this.information = 'Proces zakończono pomyślnie';
            $('.alert.fadeBlock').fadeOut(4000);
        }, function (error) { console.log(error); });
        this.orderActive.status = this.statusArray[this.chosenOptionStatus];
    };
    return OrdersComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], OrdersComponent.prototype, "pageAdmin", void 0);
__decorate([
    core_1.ViewChild('tableChild', { read: core_1.ElementRef }),
    __metadata("design:type", core_1.ElementRef)
], OrdersComponent.prototype, "tableChild", void 0);
OrdersComponent = __decorate([
    core_1.Component({
        selector: 'app-orders',
        templateUrl: './orders.component.html',
        styleUrls: ['./orders.component.css', '../../accounts.css'],
        encapsulation: core_1.ViewEncapsulation.None // css dla potomków
    }),
    __metadata("design:paramtypes", [item_service_1.ItemService])
], OrdersComponent);
exports.OrdersComponent = OrdersComponent;
var Order = (function () {
    function Order(id, name, street, sumPrice, orderItems, status, statusId, dateOrder) {
        this.id = id;
        this.name = name;
        this.street = street;
        this.sumPrice = sumPrice;
        this.orderItems = orderItems;
        this.status = status;
        this.statusId = statusId;
        this.dateOrder = dateOrder;
    }
    return Order;
}());
exports.Order = Order;
//# sourceMappingURL=orders.component.js.map