"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var user_service_1 = require("./../../../services/user.service");
var UserStartViewComponent = (function () {
    function UserStartViewComponent(userService) {
        this.userService = userService;
        this.logoutPage = new core_1.EventEmitter();
        this.pageUser = "viewPanel";
    }
    UserStartViewComponent.prototype.ngOnInit = function () {
    };
    UserStartViewComponent.prototype.changePage = function (page) {
        this.pageUser = page;
    };
    UserStartViewComponent.prototype.logout = function () {
        var _this = this;
        this.userService.logout().subscribe(function (res) {
            _this.logoutPage.emit();
        }, function (error) {
            console.log(error);
        });
    };
    return UserStartViewComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], UserStartViewComponent.prototype, "logoutPage", void 0);
UserStartViewComponent = __decorate([
    core_1.Component({
        selector: 'app-user-start-view',
        templateUrl: './user-start-view.component.html',
        styleUrls: ['./user-start-view.component.css', '../accounts.css']
    }),
    __metadata("design:paramtypes", [user_service_1.UserService])
], UserStartViewComponent);
exports.UserStartViewComponent = UserStartViewComponent;
//# sourceMappingURL=user-start-view.component.js.map