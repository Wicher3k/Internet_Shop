"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var item_service_1 = require("./../../../../services/item.service");
var angular_4_data_table_1 = require("angular-4-data-table");
var UserOrdersComponent = (function () {
    function UserOrdersComponent(itemService) {
        this.itemService = itemService;
        this.pageAdmin = new core_1.EventEmitter();
        this.objectKeys = Object.keys;
        this.edited = false;
        this.edited2 = false;
        this.myOrders = [];
        this.statusArray = {};
        this.items = [];
        this.itemCount = 0;
        this.itemResource = new angular_4_data_table_1.DataTableResource(this.myOrders);
    }
    UserOrdersComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.itemService.getOrderStatuses().subscribe(function (res) {
            for (var i in res) {
                _this.statusArray[res[i]['id']] = res[i]['name'];
            }
        }, function (error) {
            console.log(error);
        });
        var orderObservable = this.itemService.getUserOrders();
        orderObservable.subscribe(function (res) {
            for (var i in res) {
                _this.myOrders.push(new myOrders(res[i]['dateOrder'], res[i]['sumPrice'], res[i]['orderItems'], _this.statusArray[res[i]['status']]));
            }
            var _loop_1 = function (order) {
                var _loop_2 = function (id) {
                    var orderItems = _this.itemService.getItemsFromId(id);
                    orderItems.subscribe(function (res) {
                        order.orderItems[id].name = res['name'];
                    }, function (error) {
                        console.log(error);
                    });
                };
                for (var id in order.orderItems) {
                    _loop_2(id);
                }
            };
            for (var _i = 0, _a = _this.myOrders; _i < _a.length; _i++) {
                var order = _a[_i];
                _loop_1(order);
            }
            _this.itemResource.count().then(function (count) { return _this.itemCount = count; });
            _this.reloadItems({});
        }, function (error) {
            console.log(error);
        });
    };
    UserOrdersComponent.prototype.ngAfterViewChecked = function () {
        var sidePanel = this.tableChild.nativeElement.querySelectorAll('.panel-default');
        if (sidePanel.length > 0) {
            sidePanel[0].style.backgroundColor = 'white';
        }
        var icons = this.tableChild.nativeElement.querySelectorAll('.glyphicon-refresh');
        if (icons.length > 0) {
            icons[0].style.backgroundColor = 'white';
        }
        if (!this.edited) {
            $('.refresh-button').append('<i class="fa fa-refresh" aria-hidden="true"></i>');
            $('.column-selector-button').append('<i class="fa fa-list" aria-hidden="true"></i>');
            this.edited = true;
        }
        var icons2 = this.tableChild.nativeElement.querySelectorAll('.glyphicon-triangle-right');
        if (icons2.length > 0) {
            icons2[0].style.backgroundColor = 'white';
            if (!this.edited2) {
                $('.row-expand-button').append('<i class="fa fa-caret-down" aria-hidden="true"></i>');
                this.edited2 = true;
            }
        }
    };
    UserOrdersComponent.prototype.reloadItems = function (params) {
        var _this = this;
        this.itemResource.query(params).then(function (items) { return _this.items = items; });
    };
    UserOrdersComponent.prototype.changePage = function () {
        this.pageAdmin.emit('viewPanel');
    };
    return UserOrdersComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], UserOrdersComponent.prototype, "pageAdmin", void 0);
__decorate([
    core_1.ViewChild('tableChild', { read: core_1.ElementRef }),
    __metadata("design:type", core_1.ElementRef)
], UserOrdersComponent.prototype, "tableChild", void 0);
UserOrdersComponent = __decorate([
    core_1.Component({
        selector: 'app-user-orders',
        templateUrl: './user-orders.component.html',
        styleUrls: ['./user-orders.component.css', '../../accounts.css'],
        encapsulation: core_1.ViewEncapsulation.None
    }),
    __metadata("design:paramtypes", [item_service_1.ItemService])
], UserOrdersComponent);
exports.UserOrdersComponent = UserOrdersComponent;
var myOrders = (function () {
    function myOrders(date, sumPrice, orderItems, status) {
        this.date = date;
        this.sumPrice = sumPrice;
        this.orderItems = orderItems;
        this.status = status;
    }
    return myOrders;
}());
exports.myOrders = myOrders;
//# sourceMappingURL=user-orders.component.js.map