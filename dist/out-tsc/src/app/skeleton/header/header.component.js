"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var basket_service_1 = require("./../../services/basket.service");
var item_service_1 = require("./../../services/item.service");
var HeaderComponent = (function () {
    function HeaderComponent(itemService, basketService) {
        var _this = this;
        this.itemService = itemService;
        this.basketService = basketService;
        this.pageMain = new core_1.EventEmitter();
        this.basketValue = 0;
        this.basketNumber = 0;
        this.basketService.caseNumberBasket$.subscribe(function (data) {
            if (data[0] == 'reset') {
                _this.basketValue = 0;
                _this.basketNumber = 0;
            }
            else {
                _this.getbasket();
            }
        });
        this.itemService.sale$.subscribe(function (data) {
            _this.getbasket();
        });
        this.itemService.loadedbasket$.subscribe(function (data) {
            _this.getbasket();
        });
    }
    HeaderComponent.prototype.getbasket = function () {
        var itemsBasket = this.itemService.getBasketService();
        this.basketNumber = 0;
        this.basketValue = 0;
        for (var _i = 0, itemsBasket_1 = itemsBasket; _i < itemsBasket_1.length; _i++) {
            var item = itemsBasket_1[_i];
            this.basketNumber += item.order;
            this.basketValue += item.order * item.price;
        }
    };
    HeaderComponent.prototype.ngOnInit = function () {
        this.getbasket();
    };
    HeaderComponent.prototype.changePage = function (page) {
        this.pageMain.emit(page);
    };
    return HeaderComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], HeaderComponent.prototype, "pageMain", void 0);
HeaderComponent = __decorate([
    core_1.Component({
        selector: 'app-header',
        templateUrl: './header.component.html',
        styleUrls: ['./header.component.css']
    }),
    __metadata("design:paramtypes", [item_service_1.ItemService, basket_service_1.BasketService])
], HeaderComponent);
exports.HeaderComponent = HeaderComponent;
//# sourceMappingURL=header.component.js.map