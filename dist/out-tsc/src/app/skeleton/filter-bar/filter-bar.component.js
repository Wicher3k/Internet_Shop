"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var categoryservice_service_1 = require("./../../services/categoryservice.service");
var basket_service_1 = require("./../../services/basket.service");
var user_service_1 = require("./../../services/user.service");
var item_service_1 = require("./../../services/item.service");
var itemClass_1 = require("./../../skeleton/shop-center/item/itemClass");
var FilterBarComponent = (function () {
    function FilterBarComponent(userService, basketService, categoryserviceService, itemService) {
        var _this = this;
        this.userService = userService;
        this.basketService = basketService;
        this.categoryserviceService = categoryserviceService;
        this.itemService = itemService;
        this.addOut = new core_1.EventEmitter();
        this.filters = [];
        this.categories = [];
        this.categoriesGroups = [];
        this.categoriesGroupsNames = [];
        this.saleProduct = new itemClass_1.Item();
        this.someRange2config = {
            behaviour: 'drag',
            connect: true,
            start: [0, 2000],
            margin: 1,
            keyboard: true,
            step: 1,
            range: {
                min: 0,
                max: 2000
            },
            tooltips: true,
        };
        this.itemService.sale$.subscribe(function (data) {
            _this.saleProduct = data;
        });
    }
    FilterBarComponent.prototype.onChange = function (event) {
        this.categoryserviceService.publishFilterPrice(this.someRange2);
    };
    FilterBarComponent.prototype.ngOnInit = function () {
        this.saleProduct = this.itemService.getPromotion();
        this.categories = this.categoryserviceService.categories;
        this.categoriesGroups = this.categoryserviceService.categoriesGroups;
        this.categoriesGroupsNames = this.categoryserviceService.categoriesGroupsNames;
    };
    FilterBarComponent.prototype.ngAfterViewInit = function () {
        this.setColors();
    };
    FilterBarComponent.prototype.add = function (itemObject) {
        itemObject.order += 1;
        this.addOut.emit(itemObject);
        this.basketService.publishData(itemObject.price, 1);
        this.saveBasket();
    };
    FilterBarComponent.prototype.saveBasket = function () {
        if (this.userService.ifUserLogged()) {
            var basket = this.itemService.getBasket2Save();
            this.userService.saveBasket(basket);
        }
    };
    FilterBarComponent.prototype.setColors = function () {
        var connect = this.slider.nativeElement.querySelectorAll('.noUi-connect');
        connect[0].classList.add('skyBackground');
        var connectTooltips = this.slider.nativeElement.querySelectorAll('.noUi-tooltip');
        for (var _i = 0, connectTooltips_1 = connectTooltips; _i < connectTooltips_1.length; _i++) {
            var connectTooltip = connectTooltips_1[_i];
            connectTooltip.classList.add('skyBorder');
        }
    };
    FilterBarComponent.prototype.send = function (category) {
        var index = this.filters.indexOf(category);
        if (index > -1) {
            this.filters.splice(index, 1);
        }
        else {
            this.filters.push(category);
        }
        this.categoryserviceService.publishData(this.filters);
    };
    return FilterBarComponent;
}());
__decorate([
    core_1.ViewChild('slider', { read: core_1.ElementRef }),
    __metadata("design:type", core_1.ElementRef)
], FilterBarComponent.prototype, "slider", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], FilterBarComponent.prototype, "addOut", void 0);
FilterBarComponent = __decorate([
    core_1.Component({
        selector: 'app-filter-bar',
        templateUrl: './filter-bar.component.html',
        styleUrls: ['./filter-bar.component.css']
    }),
    __metadata("design:paramtypes", [user_service_1.UserService, basket_service_1.BasketService, categoryservice_service_1.CategoryserviceService, item_service_1.ItemService])
], FilterBarComponent);
exports.FilterBarComponent = FilterBarComponent;
//# sourceMappingURL=filter-bar.component.js.map