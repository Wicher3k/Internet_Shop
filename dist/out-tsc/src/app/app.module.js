"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var ng2_nouislider_1 = require("ng2-nouislider");
var index_1 = require("angular-4-data-table/src/index");
var http_2 = require("@angular/common/http");
var sklepInternetowyMain_component_1 = require("./sklepInternetowyMain.component");
var header_component_1 = require("./skeleton/header/header.component");
var filter_bar_component_1 = require("./skeleton/filter-bar/filter-bar.component");
var shop_center_component_1 = require("./skeleton/shop-center/shop-center.component");
var nav_bar_component_1 = require("./skeleton/nav-bar/nav-bar.component");
var item_component_1 = require("./skeleton/shop-center/item/item.component");
var pagination_component_component_1 = require("./skeleton/shop-center/pagination-component/pagination-component.component");
var item_service_1 = require("./services/item.service");
var categoryservice_service_1 = require("./services/categoryservice.service");
var basket_service_1 = require("./services/basket.service");
var user_service_1 = require("./services/user.service");
var basket_center_component_1 = require("./skeleton/basket-center/basket-center.component");
var order_center_component_1 = require("./skeleton/order-center/order-center.component");
var account_component_1 = require("./skeleton/account/account.component");
var admin_start_view_component_1 = require("./skeleton/account/admin-start-view/admin-start-view.component");
var orders_component_1 = require("./skeleton/account/admin-start-view/orders/orders.component");
var products_component_1 = require("./skeleton/account/admin-start-view/products/products.component");
var products_edit_component_1 = require("./skeleton/account/admin-start-view/products/products-edit/products-edit.component");
var account_register_component_1 = require("./skeleton/account/account-register/account-register.component");
var user_start_view_component_1 = require("./skeleton/account/user-start-view/user-start-view.component");
var user_orders_component_1 = require("./skeleton/account/user-start-view/user-orders/user-orders.component");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        declarations: [
            sklepInternetowyMain_component_1.SklepInternetowyMainComponent,
            header_component_1.HeaderComponent,
            filter_bar_component_1.FilterBarComponent,
            shop_center_component_1.ShopCenterComponent,
            nav_bar_component_1.NavBarComponent,
            item_component_1.ItemComponent,
            pagination_component_component_1.PaginationComponentComponent,
            basket_center_component_1.BasketCenterComponent,
            order_center_component_1.OrderCenterComponent,
            account_component_1.AccountComponent,
            admin_start_view_component_1.AdminStartViewComponent,
            orders_component_1.OrdersComponent,
            products_component_1.ProductsComponent,
            products_edit_component_1.ProductsEditComponent,
            account_register_component_1.AccountRegisterComponent,
            user_start_view_component_1.UserStartViewComponent,
            user_orders_component_1.UserOrdersComponent,
        ],
        imports: [
            platform_browser_1.BrowserModule,
            forms_1.FormsModule,
            http_1.HttpModule,
            ng2_nouislider_1.NouisliderModule,
            index_1.DataTableModule,
            http_2.HttpClientModule
        ],
        providers: [item_service_1.ItemService, categoryservice_service_1.CategoryserviceService, basket_service_1.BasketService, user_service_1.UserService],
        bootstrap: [sklepInternetowyMain_component_1.SklepInternetowyMainComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map