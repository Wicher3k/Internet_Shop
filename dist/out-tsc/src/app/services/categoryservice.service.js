"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Subject_1 = require("rxjs/Subject");
var http_1 = require("@angular/common/http");
var CategoryserviceService = (function () {
    function CategoryserviceService(http) {
        this.http = http;
        this.caseNumber = new Subject_1.Subject();
        this.caseFilterString = new Subject_1.Subject();
        this.caseFilterPrice = new Subject_1.Subject();
        this.caseNumber$ = this.caseNumber.asObservable();
        this.caseFilterString$ = this.caseFilterString.asObservable();
        this.caseFilterPrice$ = this.caseFilterPrice.asObservable();
        this.categories = [];
        this.categoriesGroups = [];
        this.categoriesGroupsNames = [];
        this.baseUrlCategories = "/categories";
        this.getCategories();
    }
    CategoryserviceService.prototype.publishData = function (data) {
        this.caseNumber.next(data);
    };
    CategoryserviceService.prototype.publishDataCategoryString = function (data) {
        this.caseFilterString.next(data);
    };
    CategoryserviceService.prototype.publishFilterPrice = function (data) {
        this.caseFilterPrice.next(data);
    };
    CategoryserviceService.prototype.getCategories = function () {
        var _this = this;
        this.http.get(this.baseUrlCategories, { withCredentials: true }).subscribe(function (res) {
            for (var i in res) {
                if (res[i]['group'] == undefined) {
                    _this.categories.push(res[i]['name']);
                }
                else {
                    var index = _this.categoriesGroupsNames.indexOf(res[i]['group']);
                    if (index > -1) {
                        _this.categoriesGroups[index].push(res[i]['name']);
                    }
                    else {
                        _this.categoriesGroupsNames.push(res[i]['group']);
                        _this.categoriesGroups.push([res[i]['name']]);
                    }
                }
            }
        }, function (error) {
            console.log(error);
        });
    };
    return CategoryserviceService;
}());
CategoryserviceService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.HttpClient])
], CategoryserviceService);
exports.CategoryserviceService = CategoryserviceService;
//# sourceMappingURL=categoryservice.service.js.map