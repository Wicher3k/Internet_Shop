"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var basket_service_1 = require("./basket.service");
describe('BasketService', function () {
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            providers: [basket_service_1.BasketService]
        });
    });
    it('should be created', testing_1.inject([basket_service_1.BasketService], function (service) {
        expect(service).toBeTruthy();
    }));
});
//# sourceMappingURL=basket.service.spec.js.map