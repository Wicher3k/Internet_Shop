"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var categoryservice_service_1 = require("./categoryservice.service");
describe('CategoryserviceService', function () {
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            providers: [categoryservice_service_1.CategoryserviceService]
        });
    });
    it('should be created', testing_1.inject([categoryservice_service_1.CategoryserviceService], function (service) {
        expect(service).toBeTruthy();
    }));
});
//# sourceMappingURL=categoryservice.service.spec.js.map