"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var itemClass_1 = require("./../skeleton/shop-center/item/itemClass");
var user_service_1 = require("./user.service");
var http_1 = require("@angular/http");
var Subject_1 = require("rxjs/Subject");
require("rxjs/add/operator/map");
var http_2 = require("@angular/common/http");
var ItemService = (function () {
    function ItemService(http, http2, userService) {
        this.http = http;
        this.http2 = http2;
        this.userService = userService;
        this.promocjaProduct = new itemClass_1.Item();
        this.salesIdArray = [];
        this.items = [];
        this.itemsReturn = [];
        this.countItems = 0;
        this.baseUrlProducts = '/products';
        this.baseUrlOrders = '/orders';
        this.baseUrlAuthentication = '/users/login';
        this.baseUrlOrderStatuses = '/orderstatuses';
        this.endInitialization = new Subject_1.Subject();
        this.endInitialization$ = this.endInitialization.asObservable();
        this.sale = new Subject_1.Subject();
        this.sale$ = this.sale.asObservable();
        this.loadedbasket = new Subject_1.Subject();
        this.loadedbasket$ = this.loadedbasket.asObservable();
        this.socket = io.connect('http://localhost:3000');
        //var socket = io.connect('https://peaceful-ocean-60282.herokuapp.com' );
        this.getProductJsonAndSetItems(true);
        this.anotherSockets();
    }
    ;
    ItemService.prototype.getProductJsonAndSetItems = function (connectServer) {
        var _this = this;
        if (connectServer === void 0) { connectServer = false; }
        this.items = [];
        this.http.get(this.baseUrlProducts).map(function (res) { return res.json(); }).subscribe(function (res) {
            for (var _i = 0, res_1 = res; _i < res_1.length; _i++) {
                var row = res_1[_i];
                _this.items.push(new itemClass_1.Item(row['_id'], row['name'], row['description'], row['price'], row['category'], 0, row['imageExist'], false));
            }
            _this.endInitialization.next();
            if (connectServer = true) {
                var klasa = _this;
                _this.socket.on('sales', function (data) {
                    for (var k in data.promocja) {
                        for (var i in klasa.items) {
                            if (data.promocja[k] == klasa.items[i].id) {
                                var j = parseInt(i);
                                klasa.items[j].priceOld = klasa.items[j].price;
                                klasa.items[j].price = klasa.items[j].price * (1 - data.saleValue[k]);
                                klasa.items[j].sale = true;
                                klasa.promocjaProduct = klasa.items[j];
                                klasa.salesIdArray.push(data.promocja[k]);
                                break;
                            }
                        }
                    }
                    klasa.sale.next(klasa.promocjaProduct);
                });
                _this.socket.on('salesEnd', function (data) {
                    for (var i in klasa.items) {
                        if (data.promocja == klasa.items[i].id) {
                            var j = parseInt(i);
                            klasa.items[j].sale = false;
                            klasa.items[j].price = klasa.items[j].priceOld;
                            break;
                        }
                    }
                    var index = klasa.salesIdArray.indexOf(data.promocja);
                    if (index > -1) {
                        klasa.salesIdArray.splice(index, 1);
                    }
                    if (klasa.salesIdArray.length < 1) {
                        klasa.promocjaProduct = new itemClass_1.Item();
                    }
                    else {
                        klasa.promocjaProduct = klasa.searchProductByid(klasa.salesIdArray[klasa.salesIdArray.length - 1]);
                    }
                    klasa.sale.next(klasa.promocjaProduct);
                });
            }
        }, function (error) {
            _this.endInitialization.next("error : " + error);
        });
    };
    ItemService.prototype.searchProductByid = function (idProduct) {
        for (var i in this.items) {
            if (this.items[i].id == idProduct) {
                return this.items[i];
            }
        }
        return new itemClass_1.Item();
    };
    ItemService.prototype.ifProductSale = function (idProduct) {
        var index = this.salesIdArray.indexOf(idProduct);
        if (index > -1) {
            return true;
        }
        else {
            return false;
        }
    };
    ItemService.prototype.getItemsService = function (option) {
        var itemsfiltred = this.filterItems(option['filter'], option['filterRe'], option['filterPrice']);
        this.itemsReturn = [];
        for (var i = 0; i < option['limit']; i++) {
            if (i + (option['page'] - 1) * option['limit'] < this.countItems)
                this.itemsReturn.push(itemsfiltred[i + (option['page'] - 1) * option['limit']]);
        }
        return { itemsObjects: this.itemsReturn, total: this.countItems };
    };
    ItemService.prototype.getBasketService = function () {
        var itemsReturn = [];
        for (var _i = 0, _a = this.items; _i < _a.length; _i++) {
            var item = _a[_i];
            if (item.order > 0) {
                itemsReturn.push(item);
            }
        }
        return itemsReturn;
    };
    ItemService.prototype.getBasket2Save = function () {
        var itemsReturn = {};
        for (var _i = 0, _a = this.items; _i < _a.length; _i++) {
            var item = _a[_i];
            if (item.order > 0) {
                itemsReturn[item.id] = item.order;
            }
        }
        return itemsReturn;
    };
    ItemService.prototype.filterItems = function (filtersCategoryChecked, filterRe, filterPrice) {
        this.countItems = 0;
        var itemsfiltred = [], itemsfiltredReturn = [];
        if (filtersCategoryChecked.length > 0) {
            for (var _i = 0, filtersCategoryChecked_1 = filtersCategoryChecked; _i < filtersCategoryChecked_1.length; _i++) {
                var filter = filtersCategoryChecked_1[_i];
                for (var _a = 0, _b = this.items; _a < _b.length; _a++) {
                    var item = _b[_a];
                    if (filtersCategoryChecked.length > 0 && item.category == filter && itemsfiltred.indexOf(item) < 0) {
                        itemsfiltred.push(item);
                    }
                }
            }
        }
        else {
            itemsfiltred = this.items;
        }
        if (filterRe.length > 0) {
            var regexp = new RegExp('.*' + filterRe.toLowerCase() + '.*');
            for (var _c = 0, itemsfiltred_1 = itemsfiltred; _c < itemsfiltred_1.length; _c++) {
                var item = itemsfiltred_1[_c];
                if (regexp.test(item.name.toLowerCase())) {
                    itemsfiltredReturn.push(item);
                }
            }
        }
        else {
            itemsfiltredReturn = itemsfiltred;
        }
        itemsfiltred = itemsfiltredReturn;
        itemsfiltredReturn = [];
        for (var _d = 0, itemsfiltred_2 = itemsfiltred; _d < itemsfiltred_2.length; _d++) {
            var item = itemsfiltred_2[_d];
            if (item.price >= filterPrice[0] && item.price <= filterPrice[1]) {
                itemsfiltredReturn.push(item);
            }
        }
        this.countItems = itemsfiltredReturn.length;
        return itemsfiltredReturn;
    };
    ItemService.prototype.getPromotion = function () {
        return this.promocjaProduct;
    };
    ItemService.prototype.anotherSockets = function () {
        var klasa = this;
        this.socket.on('deleteProduct', function (idProd) {
            klasa.deleteProductView(idProd);
            klasa.reloadSiteHeader();
            klasa.endInitialization.next();
        });
    };
    ItemService.prototype.resetShopContent = function () {
        this.getProductJsonAndSetItems();
    };
    ItemService.prototype.setBasket = function (basket) {
        for (var id in basket) {
            for (var _i = 0, _a = this.items; _i < _a.length; _i++) {
                var item = _a[_i];
                if (id == item.id) {
                    item.order = basket[id];
                    break;
                }
            }
        }
        this.loadedbasket.next();
    };
    ItemService.prototype.reloadSiteHeader = function () {
        this.loadedbasket.next();
    };
    ItemService.prototype.getHeader = function () {
        var token = this.userService.getToken();
        return new http_2.HttpHeaders({ 'x-access-token': token });
    };
    ItemService.prototype.deleteProductView = function (idProd) {
        for (var i in this.items) {
            if (this.items[i].id == idProd.massage) {
                this.items.splice(parseInt(i), 1);
                break;
            }
        }
    };
    //CRUD
    //orders
    ItemService.prototype.getOrders = function () {
        var myHeaders = this.getHeader();
        return this.http2.get(this.baseUrlOrders, { headers: myHeaders, withCredentials: true });
    };
    ItemService.prototype.getUserOrders = function () {
        var myHeaders = this.getHeader();
        var userLoggedId = this.userService.getUserLoggedId();
        return this.http2.get(this.baseUrlOrders + '/user/' + userLoggedId, { headers: myHeaders, withCredentials: true });
    };
    ItemService.prototype.sendOrder = function (model, itemsSum, itemsOrder) {
        var userLoggedId = this.userService.getUserLoggedId();
        var d = new Date, dformat = [d.getMonth() + 1,
            d.getDate(),
            d.getFullYear()].join('/') + ' ' +
            [d.getHours(),
                d.getMinutes(),
                d.getSeconds()].join(':');
        return this.http2.post(this.baseUrlOrders, { "name": model.name, "street": model.street, "sumPrice": itemsSum, "orderItems": itemsOrder, "userId": userLoggedId, "dateOrder": dformat, "status": 0 }, { withCredentials: true });
    };
    ItemService.prototype.putOrder = function (id, statusId) {
        var myHeaders = this.getHeader();
        return this.http2.put(this.baseUrlOrders + '/' + id, { "status": statusId,
        }, { headers: myHeaders, withCredentials: true });
    };
    ItemService.prototype.getOrderStatuses = function () {
        var myHeaders = this.getHeader();
        return this.http2.get(this.baseUrlOrderStatuses, { headers: myHeaders, withCredentials: true });
    };
    // products
    ItemService.prototype.getProducts = function () {
        return this.http.get(this.baseUrlProducts).map(function (res) { return res.json(); });
    };
    ItemService.prototype.deleteProduct = function (id) {
        var myHeaders = this.getHeader();
        return this.http2.delete(this.baseUrlProducts + '/' + id, { headers: myHeaders, withCredentials: true });
    };
    ItemService.prototype.addOrEditProduct = function (product, option, image) {
        if (option == 'Dodaj') {
            return this.addProduct(product, image);
        }
        else if (option == 'Edytuj') {
            return this.editProduct(product, image);
        }
    };
    ItemService.prototype.addProduct = function (product, image) {
        if (image != undefined) {
            var imageExist = true;
        }
        var myHeaders = this.getHeader();
        myHeaders.append('Content-Type', 'multipart/form-data');
        return this.http2.post(this.baseUrlProducts, { "name": product.name,
            "description": product.description,
            "price": product.price,
            "category": product.category,
            "imageExist": imageExist,
            "image": image,
            "sale": product.sale,
            "optionPromotion": { saleTime: product.saleTime, saleValue: product.saleValue } }, { headers: myHeaders, withCredentials: true });
    };
    ItemService.prototype.editProduct = function (product, image) {
        if (image != undefined) {
            var imageExist = true;
        }
        var myHeaders = this.getHeader();
        return this.http2.put(this.baseUrlProducts + '/' + product.id, { "name": product.name,
            "description": product.description,
            "price": product.price,
            "category": product.category,
            "imageExist": imageExist,
            "image": image,
            "sale": product.sale,
            "optionPromotion": { saleTime: product.saleTime, saleValue: product.saleValue } }, { headers: myHeaders, withCredentials: true });
    };
    ItemService.prototype.getPhoto = function (idProduct) {
        var myHeaders = this.getHeader();
        return this.http2.get(this.baseUrlProducts + '/photo/' + idProduct, { headers: myHeaders, withCredentials: true });
    };
    ItemService.prototype.getItemsFromId = function (id) {
        var myHeaders = this.getHeader();
        return this.http2.get(this.baseUrlProducts + '/' + id, { headers: myHeaders, withCredentials: true });
    };
    return ItemService;
}());
ItemService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http, http_2.HttpClient, user_service_1.UserService])
], ItemService);
exports.ItemService = ItemService;
var UserAuthenticate = (function () {
    function UserAuthenticate(username, password) {
        this.username = username;
        this.password = password;
    }
    return UserAuthenticate;
}());
exports.UserAuthenticate = UserAuthenticate;
//# sourceMappingURL=item.service.js.map