"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var UserService = (function () {
    function UserService(http) {
        this.http = http;
        this.userLogged = new ModelAuthenticate();
        this.token = "";
        this.baseUrlUser = '/users';
        this.baseUrlAuthentication = '/users/login';
        this.baseUrlLogout = '/users/logout';
    }
    UserService.prototype.setUser = function (model, functionAccount, token, userId) {
        this.userLogged = model;
        this.userLogged.userId = userId;
        this.userLogged.functionAccount = functionAccount;
        this.token = token;
    };
    UserService.prototype.register = function (userModel) {
        return this.http.post(this.baseUrlUser, { "username": userModel.name, "password": userModel.password, 'function': 1 }, { withCredentials: true });
    };
    UserService.prototype.authenticate = function (userModel) {
        return this.http.post(this.baseUrlAuthentication, { "username": userModel.username, "password": userModel.password }, { withCredentials: true });
    };
    UserService.prototype.logout = function () {
        this.token = '';
        this.userLogged = new ModelAuthenticate();
        return this.http.post(this.baseUrlLogout, '', { withCredentials: true });
    };
    UserService.prototype.getHeader = function () {
        var token = this.token;
        return new http_1.HttpHeaders({ 'x-access-token': token });
    };
    UserService.prototype.getToken = function () {
        return this.token;
    };
    UserService.prototype.getUser = function () {
        return this.userLogged;
    };
    UserService.prototype.getUserLoggedId = function () {
        return this.userLogged.userId;
    };
    UserService.prototype.ifUserLogged = function () {
        return this.userLogged.userId != '';
    };
    UserService.prototype.saveBasket = function (basket) {
        var myHeaders = this.getHeader();
        this.http.put(this.baseUrlUser + '/basket/' + this.userLogged.userId, { "basketSaved": basket,
        }, { headers: myHeaders, withCredentials: true }).subscribe(function (res) { });
    };
    return UserService;
}());
UserService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.HttpClient])
], UserService);
exports.UserService = UserService;
var ModelAuthenticate = (function () {
    function ModelAuthenticate(userId, username, password, functionAccount) {
        if (userId === void 0) { userId = ''; }
        if (username === void 0) { username = ''; }
        if (password === void 0) { password = ''; }
        if (functionAccount === void 0) { functionAccount = 0; }
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.functionAccount = functionAccount;
    }
    return ModelAuthenticate;
}());
exports.ModelAuthenticate = ModelAuthenticate;
//# sourceMappingURL=user.service.js.map