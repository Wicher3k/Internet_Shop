const express = require('express');
const router = express.Router();
const app = express();

var mongoose = require('mongoose');
mongoose.connect('mongodb://Admin:secret@ds121726.mlab.com:21726/sklep');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'błąd połączenia...'));
db.once('open', function() {
    // console.log('ok');
    // połączenie udane!
});

var Schema = mongoose.Schema;

// schema Productow
var products = new Schema({
    name: {type: String,required: true},
    description: {type: String,required: true},
    price: {type:Number, min:0,required: true},
    category: String,
    imageExist: Boolean,
    image: String,
});
ProductModel = mongoose.model('Product', products);

// schema Kategorii
var categories = new Schema({
    name: {type: String,required: true},
    group: String,
});
CategoryModel = mongoose.model('Category', categories);

// schema orderow 
var orders = new Schema({
    name: {type: String,required: true},
    street: {type: String,required: true},
    sumPrice: {type: Number, min:0, required: true},
    orderItems: {type: Object ,required: true},
    userId:{type: String},
    dateOrder:{type: String},
    status: Number,
});
OrdersModel = mongoose.model('Order', orders);

// schema statusow orderow 
var orderStatus = new Schema({
    id: {type: Number},
    name: {type: String},
});
OrderStatusModel = mongoose.model('orderStatus', orderStatus);

// schema Userow
var bcrypt = require('bcrypt'),
SALT_WORK_FACTOR = 10;

var users = new Schema({
    username: { type: String, required: true, index: { unique: true } },
    password: { type: String, required: true },
    function: { type: Number, required: true },
    basketSaved: { type:Object, default: {} }

});

users.pre('save', function(next) {
    var user = this;

// only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});
users.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};


UsersModel = mongoose.model('Users', users);
// // create a user a new user
// var testUser = new UsersModel({
//     username: 'Admin',
//     password: 'secret'
// });

// // save user to database
// testUser.save(function(err) {
//     if (err) throw err;
// });



// var x = new OrderStatusModel({
//     id: 1,
//     name: 'Zatwierdzone'
// });

// x.save(function(err) {
//     if (err) throw err;
// });


module.exports = router;