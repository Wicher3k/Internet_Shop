export class Item{
    constructor (public id: number =0, public name: string ="", public description: string="", public price: number=0, public category: string="", public order: number=0,public imageExist: Boolean=false,public sale:Boolean=false,public oldPrice: number=0) { }
}