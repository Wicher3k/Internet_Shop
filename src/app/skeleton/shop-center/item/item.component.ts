import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Item } from './itemClass';
import { BasketService } from './../../../services/basket.service';
declare var $ :any;

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  @Input() itemObject: Item;
  @Output() addOut = new EventEmitter <Item>();
  @Output() subtractOut = new EventEmitter <Item>();
  @Output() openWindow = new EventEmitter <Item>();

  constructor(private basketService: BasketService) { }

  ngOnInit() {
  }

  add(Item:Item){
    this.addOut.emit(Item);
    this.blink();
    this.basketService.publishData(Item.price,1);
  }

  blink(){
    for(let i=0;i<2;i++) {
      $('.itemOrderNr').fadeTo('fast', 0.5).fadeTo('fast', 1.0);
    }
  }
  
  subtract(Item:Item){
    if(Item.order>0){
      this.subtractOut.emit(Item);
      this.blink();
      this.basketService.publishData(Item.price,-1);
    }
  }

  openBox(data){
    this.openWindow.emit(data);
  }
}
