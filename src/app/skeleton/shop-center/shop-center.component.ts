import { Component, OnInit } from '@angular/core';
import { Item } from './item/itemClass';
import { ItemService } from './../../services/item.service';
import { CategoryserviceService } from './../../services/categoryservice.service';
import { UserService } from './../../services/user.service';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';
declare var $ :any;

@Component({
  selector: 'app-shop-center',
  templateUrl: './shop-center.component.html',
  styleUrls: ['./shop-center.component.css']
})
export class ShopCenterComponent implements OnInit {
  public itemsObjects=[];
  public total = 0;
  public page = 1;
  public limit = 3;
  private filterString='';
  private filter=[];
  private filterPrice=[0,1000000];
  public errorShow='';
  public image;
  public nameProduct;
  
  constructor(private userService: UserService,private itemService: ItemService, private categoryserviceService: CategoryserviceService) {
    this.categoryserviceService.caseNumber$.subscribe(
      data => {
        this.page = 1
        this.filter=data;
        this.getItems();
    });
    this.categoryserviceService.caseFilterString$.subscribe(
      data => {
        this.page = 1
        this.filterString=data;
        this.getItems();
    });
    this.categoryserviceService.caseFilterPrice$.subscribe(
      data => {
        this.page = 1;
        this.filterPrice=data;
        this.getItems();
    });
    this.itemService.endInitialization$.subscribe(
      dataError => {
        this.page = 1
        this.getItems();
        if(dataError){
          this.errorShow="Błąd serwera";
        }
    });
  }

  ngOnInit() {
    this.getItems();


  }

  getItems(){
    let itemsObjectsTemp = this.itemService.getItemsService({ page: this.page, limit: this.limit, filter:this.filter,filterRe:this.filterString,filterPrice:this.filterPrice});
    this.itemsObjects = itemsObjectsTemp['itemsObjects'];
    this.total = itemsObjectsTemp['total']; // liczba znalezionych produktów pasujących
  }

  goToPage(n: number): void {
    this.page = n;
    this.getItems();
  }

  onNext(): void {
    this.page++;
    this.getItems();
  }

  onPrev(): void {
    this.page--;
    this.getItems();
  }

  add(itemObject:Item){
    itemObject.order+=1;
    this.saveBasket();
  }

  subtract(itemObject:Item){
    itemObject.order-=1;
    this.saveBasket();
  }

  saveBasket(){
    if (this.userService.ifUserLogged()){
      let basket = this.itemService.getBasket2Save();
      this.userService.saveBasket(basket);
    }
  }

  open(event){
    this.nameProduct=event['name'];
    $('#myModal').show();
    this.itemService.getPhoto(event['id']).subscribe(
      data => {
       
        this.image=data['image'];
    });
  }

  close(){
    $('#myModal').hide();
  }
  
}
