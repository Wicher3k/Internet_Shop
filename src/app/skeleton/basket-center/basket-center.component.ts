import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { ItemService } from './../../services/item.service';
import { BasketService } from './../../services/basket.service';
import { UserService } from './../../services/user.service';
@Component({
  selector: 'app-basket-center',
  templateUrl: './basket-center.component.html',
  styleUrls: ['./basket-center.component.css']
})
export class BasketCenterComponent implements OnInit {
  @Output() pageMain = new EventEmitter <string>();
  public itemsBasket;
  public itemsSum=0;

  constructor(private userService: UserService, private itemService: ItemService,private basketService: BasketService) {
    this.itemService.sale$.subscribe(
      data => {
        this.getbasket();
    });
  }

  ngOnInit() {
    this.getbasket();
  }
  
  getbasket(){
    this.itemsBasket = this.itemService.getBasketService();
    for(let item of this.itemsBasket){
      this.itemsSum+=item.order*item.price;
    }
    this.saveBasket();
  }

  deleteItemInBasket(itemBasket){
    this.basketService.publishData(itemBasket.price,-itemBasket.order);
    this.itemsSum-=itemBasket.order*itemBasket.price;
    itemBasket.order=0;
    this.itemsBasket.splice(this.itemsBasket.indexOf(itemBasket), 1);
    this.itemService.reloadSiteHeader();
  }

  changePage(page:string){
    this.pageMain.emit(page);
  }

  saveBasket(){
    if (this.userService.ifUserLogged()){
      let basket = this.itemService.getBasket2Save();
      this.userService.saveBasket(basket);
    }
  }

}
