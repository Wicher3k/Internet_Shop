import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasketCenterComponent } from './basket-center.component';

describe('BasketCenterComponent', () => {
  let component: BasketCenterComponent;
  let fixture: ComponentFixture<BasketCenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasketCenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasketCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
