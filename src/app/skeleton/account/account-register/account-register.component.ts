import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { UserService } from './../../../services/user.service';

@Component({
  selector: 'app-account-register',
  templateUrl: './account-register.component.html',
  styleUrls: ['./account-register.component.css']
})
export class AccountRegisterComponent implements OnInit {
  public submitted = false;
  public model = new ModelUser('','');
  @Output() mainPage = new EventEmitter <string>();

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  onSubmit() { 
    this.submitted = true; 
    this.userService.register(this.model).subscribe(
      (res)=>{
        this.mainPage.emit('Stworzono pomyślnie użytkownika');
      },
      (error)=>{
        console.log(error);
      }
    );
  }
}

export class ModelUser{
  constructor (public name: string, public password: string){}
}
