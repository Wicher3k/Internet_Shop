import { Component, OnInit,Output,EventEmitter,ViewChild,ElementRef,ViewEncapsulation } from '@angular/core';
import { DataTable, DataTableResource  } from 'angular-4-data-table';
import { ItemService } from './../../../../services/item.service';
import { Item } from './../../../shop-center/item/itemClass';
declare var $ :any;

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css','../../accounts.css'],
  encapsulation: ViewEncapsulation.None // css dla potomków
})
export class OrdersComponent implements OnInit {
  @Output() pageAdmin = new EventEmitter <string>();
  @ViewChild('tableChild', { read: ElementRef }) tableChild: ElementRef;
  
  private items = [];
  private itemCount = 0;
  private edited = false;
  private objectKeys = Object.keys;
  private statusArray = {}; // poprawić  i w urzytkowniku
  public orders = [];
  public orderActive;
  public productsOrder;
  public displayMoreInfo=false;
  private chosenOptionStatus;
  private information='';
  
  itemResource = new DataTableResource(this.orders);
  
  constructor(private itemService: ItemService){}

  ngOnInit() {
    this.itemService.getOrderStatuses().subscribe(
      (res) => {
        for(let i in res){
          this.statusArray[res[i]['id']]=res[i]['name'];
        }
      },
      (error) => {console.log(error);
      }
    );
    let orderObservable = this.itemService.getOrders();
    orderObservable.subscribe(
      (res) => {
        for(let i in res){
            this.orders.push(new Order(res[i]['_id'],res[i]['name'],res[i]['street'],res[i]['sumPrice'],res[i]['orderItems'],this.statusArray[res[i]['status']],res[i]['status'],res[i]['dateOrder']));
          }
          this.itemResource.count().then(count => this.itemCount = count);
          this.reloadItems({sortBy: "name", sortAsc: true,offset: 0, limit: 10});
      },
      (error) => {console.log(error);
      }
    );
  }

  ngAfterViewChecked(){

    let sidePanel = this.tableChild.nativeElement.querySelectorAll('.panel-default');
    if(sidePanel.length>0){
      sidePanel[0].style.backgroundColor='white';
    }

    let icons = this.tableChild.nativeElement.querySelectorAll('.glyphicon-refresh');
    if(icons.length>0){
      icons[0].style.backgroundColor='white';
    }
    if(!this.edited){
      $('.refresh-button').append('<i class="fa fa-refresh" aria-hidden="true"></i>');
      $('.column-selector-button').append('<i class="fa fa-list" aria-hidden="true"></i>');
      this.edited=true;
    }
  }

  showMoreInfo(order){
    this.displayMoreInfo=false;
    this.orderActive=undefined;
    let index = 0;
    var count = Object.keys(order.orderItems).length;
    this.productsOrder={};
    for(let id in order.orderItems){
      let orderItems= this.itemService.getItemsFromId(id);
      orderItems.subscribe(
        (res) => {
          this.productsOrder[res['_id']] =new Item(res['_id'],res['name'],res['description'],res['price'],res['category'],0,res['imageExist']);
          index++;
          if (index==count){
            this.displayMoreInfo=true;
          }
        },
        (error) => {console.log(error);
      });
    }
    this.chosenOptionStatus=order.statusId;
    this.orderActive=order;
    window.scrollTo(0,document.body.scrollHeight+100);
  }
  changePage(){
    this.pageAdmin.emit('viewAdmin');
  }


  reloadItems(params) {
    this.itemResource.query(params).then(items => this.items = items);
  } 

  changeStatus(orderId){
    this.itemService.putOrder(orderId,this.chosenOptionStatus).subscribe(
      (res) => {
        //this.pageReturn.emit('Proces zakończono pomyślnie');
        $('html,body').scrollTop(0);
        $('.alert.fadeBlock').show();
        this.information='Proces zakończono pomyślnie';
        $('.alert.fadeBlock').fadeOut(4000);
      },
      (error) => {console.log(error);}
    );

    this.orderActive.status=this.statusArray[this.chosenOptionStatus];

  }
  
}

export class Order{
  constructor (public id: number, public name: string, public street: string, public sumPrice: number,  public orderItems:any, public status:string,public statusId:string,public dateOrder:string) { }
}

