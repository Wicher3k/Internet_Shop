import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminStartViewComponent } from './admin-start-view.component';

describe('AdminStartViewComponent', () => {
  let component: AdminStartViewComponent;
  let fixture: ComponentFixture<AdminStartViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminStartViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminStartViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
