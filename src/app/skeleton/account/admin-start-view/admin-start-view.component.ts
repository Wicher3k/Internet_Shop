import { Component, OnInit,EventEmitter,Output } from '@angular/core';
import { UserService } from './../../../services/user.service';

@Component({
  selector: 'app-admin-start-view',
  templateUrl: './admin-start-view.component.html',
  styleUrls: ['./admin-start-view.component.css','../accounts.css']
})

export class AdminStartViewComponent implements OnInit {
  @Output() logoutPage = new EventEmitter <string>();
  public pageAdmin="viewAdmin";
  constructor(private userService: UserService) { }

  ngOnInit() {}

  changePage(page){
    this.pageAdmin=page;
  }

  logout(){
    this.userService.logout().subscribe(
      (res)=>{
        this.logoutPage.emit();
      },
      (error)=>{
        console.log(error);
      }
    );
  }
}
