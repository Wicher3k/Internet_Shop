import { Component, OnInit, Input, Output, EventEmitter,ViewChild,ElementRef} from '@angular/core';
import { FormGroup,FormControl,Validators }   from '@angular/forms';
import { ItemService } from './../../../../../services/item.service';
import { CategoryserviceService } from './../../../../../services/categoryservice.service';

@Component({
  selector: 'app-products-edit',
  templateUrl: './products-edit.component.html',
  styleUrls: ['./products-edit.component.css']
})
export class ProductsEditComponent implements OnInit {
  @Input() product;
  @Output() pageReturn = new EventEmitter <string>();
  @ViewChild('fileInput') fileInput: ElementRef;;

  public categories=[];
  public productForm;
  private buttonName="Dodaj";
  public imageSrc='';
  public image='';

  constructor(private itemService: ItemService,private categoryserviceService: CategoryserviceService) {
    this.categories=[];
    this.categories = this.categoryserviceService.categories.slice();
    for(let categoriesArray of this.categoryserviceService.categoriesGroups){
      for(let category of categoriesArray){
        this.categories.push(category);
      }
    }
  }
  

  ngOnInit() {
    this.product.saleTime=0;
    this.product.saleValue=0;
    this.product.sale=this.itemService.ifProductSale(this.product.id);
    if(this.product.name!=''){
      this.buttonName="Edytuj";
    }
    this.itemService.getPhoto(this.product.id).subscribe(
      data => {
        this.image=data['image'];
    });

    this.productForm= new FormGroup({
      'name': new FormControl(this.product.name, [
        Validators.required,
      ]),
      'description': new FormControl(this.product.description, Validators.required),
      'price': new FormControl(this.product.price, Validators.required),
      'category': new FormControl(this.product.category, Validators.required)
    });
  }

  onSubmit(){
    if(this.product.saleValue>100){
      this.product.saleValue=100;
    }else if(this.product.saleValue<0 || this.product.saleValue==undefined){
      this.product.saleValue=0;
    }
    if(this.product.saleTime<0 || this.product.saleTime==undefined ){
      this.product.saleTime=0;
    }
    
    let reader = new FileReader();
    let fileBrowser = this.fileInput.nativeElement;
    if (fileBrowser.files && fileBrowser.files[0]) {
      reader.readAsDataURL(fileBrowser.files[0]);
      reader.onload = () => {
        this.imageSrc =reader.result;
        this.itemService.addOrEditProduct(this.product,this.buttonName,this.imageSrc).subscribe(
          (res) => {
            this.pageReturn.emit('Proces zakończono pomyślnie');
          },
          (error) => {console.log(error);}
        );
      }
    }else{
      this.itemService.addOrEditProduct(this.product,this.buttonName,undefined).subscribe(
        (res) => {
          this.pageReturn.emit('Proces zakończono pomyślnie');
        },
        (error) => {console.log(error);}
      );
    }
  }

  goBack(){
    this.pageReturn.emit();
  }
}
