import { Component, OnInit,Output,EventEmitter,ViewChild,ElementRef,ViewEncapsulation } from '@angular/core';
import { Item } from './../../../shop-center/item/itemClass';
import { ItemService } from './../../../../services/item.service';
import { DataTable, DataTableResource  } from 'angular-4-data-table/src/index';
declare var $ :any;
declare var jquery:any;

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css','../../accounts.css'],
  encapsulation: ViewEncapsulation.None // css dla potomków
})
export class ProductsComponent implements OnInit {
  @Output() pageAdmin = new EventEmitter <string>();
  @ViewChild(DataTable) productTable: DataTable;
  @ViewChild('tableChild', { read: ElementRef }) tableChild: ElementRef;

  public items = [];
  public itemCount = 0;
  public subPageAdmin='table';
  private information='';
  private editProduct;
  private edited = false;
  public products : Array<Item> = [];
  private itemResource = new DataTableResource(this.products);
  
  constructor(private itemService: ItemService) { }
 

  ngOnInit() {
    let productObservable = this.itemService.getProducts();
    this.itemResource = new DataTableResource(this.products);
    productObservable.subscribe(
      (res) => {
          for(let row of res){
            this.products.push(new Item(row['_id'],row['name'],row['description'],row['price'],row['category'],0,row['imageExist']));
          }
          this.itemResource.count().then(count => this.itemCount = count);
          this.reloadItems({sortBy: "name", sortAsc: true,offset: 0, limit: 10});
      },
      (error) => {console.log(error);
      }
    );
  }

  ngAfterViewChecked(){
    let sidePanel = this.tableChild.nativeElement.querySelectorAll('.panel-default');
    if(sidePanel.length>0){
      sidePanel[0].style.backgroundColor='white';
    }

    let icons = this.tableChild.nativeElement.querySelectorAll('.glyphicon-refresh');
    if(icons.length>0){
      icons[0].style.backgroundColor='white';
    }
    if(!this.edited){
      $('.refresh-button').append('<i class="fa fa-refresh" aria-hidden="true"></i>');
      $('.column-selector-button').append('<i class="fa fa-list" aria-hidden="true"></i>');
      this.edited=true;
    }
  }
  
  changePage(){
    this.pageAdmin.emit('viewAdmin');
  }

  showInfoAndReload(event){
    if(event!='' && event != undefined){
      $('.alert.fadeBlock').show();
      this.information=event;
      this.subPageAdmin='table';
      $('.alert.fadeBlock').fadeOut(4000);
    }
    this.subPageAdmin='table';
    this.products=[];
    this.ngOnInit();
  }

  reloadItems(params) {
    this.itemResource.query(params).then(items => this.items = items);
  } 

  productEdit(product){
    this.subPageAdmin="edit";
    this.editProduct=product;
  }

  productNew(){
    this.subPageAdmin="edit";
    this.editProduct=new Item(0,'','',0,'',0,false)
  }

  deleteChecked(){
    let idDeleteArray=[];
    for(let selected of this.productTable.selectedRows){
      idDeleteArray.push(selected.item.id);
    }
    let arrlen=idDeleteArray.length;
    let index=0;
    if(idDeleteArray.length<1){
      return;
    }
    for(let row of idDeleteArray){
       var observer = this.itemService.deleteProduct(row).subscribe(
        (res) => {
          index++
          if(arrlen==index){
            this.showInfoAndReload('Proces usuwania zakończono pomyślnie');
          }
        },
        (error) => {console.log(error);
        }
      );
    }
  }
}
