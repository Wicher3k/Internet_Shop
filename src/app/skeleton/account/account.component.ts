import { Component, OnInit,EventEmitter,Output } from '@angular/core';
import { ItemService } from './../../services/item.service';
import { UserService,ModelAuthenticate } from './../../services/user.service';
declare var $ :any;

@Component({
  selector: 'app-administrator',
  //providers:[ItemService],
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  private model = new ModelAuthenticate('','');
  private error = false;
  private information;
  public login = 'login';

  constructor(private itemService: ItemService, private userService: UserService) { 
  }

  ngOnInit() {
    this.model=this.userService.getUser();
    if(this.model.username != '' && this.model.functionAccount > 0){
      if (this.model.functionAccount < 10){
        this.login='loggedUser'; 
      }else{
        this.login='loggedAdmin'; 
      }
    }
  } 

  onSubmit() { 
    let ifAutenticated = this.userService.authenticate(this.model);
    ifAutenticated.subscribe(
      data => {
        this.userService.setUser(this.model,data['function'],data['token'],data['userId']);
        this.itemService.setBasket(data['basketSaved']);
        if(data['function']<10){
          this.login='loggedUser'; 
        }else{
          this.login='loggedAdmin'; 
        }
      },
      err => {this.error=true;
        console.log("error",err)
      }
    );
  }

  displayFormPage(event){
    this.login='login';
    if(event!=undefined){
      this.information=event;
      $('.fixedAlert.success').show();
      $('.fixedAlert.success').fadeOut(4000);
    }
    
  }

  registerPage(){
    this.login='register';
  }

}


