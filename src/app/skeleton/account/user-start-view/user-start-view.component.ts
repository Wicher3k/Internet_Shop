import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { UserService } from './../../../services/user.service';

@Component({
  selector: 'app-user-start-view',
  templateUrl: './user-start-view.component.html',
  styleUrls: ['./user-start-view.component.css','../accounts.css']
})
export class UserStartViewComponent implements OnInit {
  
  @Output() logoutPage = new EventEmitter <string>();
  public pageUser="viewPanel";
  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  changePage(page){
    this.pageUser=page;
  }

  logout(){
    this.userService.logout().subscribe(
      (res)=>{
        this.logoutPage.emit();
      },
      (error)=>{
        console.log(error);
      }
    );
  }

}

