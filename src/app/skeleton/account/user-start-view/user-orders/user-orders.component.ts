import { Component, OnInit,ViewEncapsulation,ElementRef,ViewChild,Output,EventEmitter } from '@angular/core';
import { ItemService } from './../../../../services/item.service';
import { DataTable, DataTableResource  } from 'angular-4-data-table';
declare var $ :any;

@Component({
  selector: 'app-user-orders',
  templateUrl: './user-orders.component.html',
  styleUrls: ['./user-orders.component.css','../../accounts.css'],
  encapsulation: ViewEncapsulation.None
})
export class UserOrdersComponent implements OnInit {
  @Output() pageAdmin = new EventEmitter <string>();
  @ViewChild('tableChild', { read: ElementRef }) tableChild: ElementRef;

  private objectKeys = Object.keys;
  private edited = false;
  private edited2 = false;
  public myOrders = [];
  private statusArray = {};
  private items = [];
  private itemCount = 0;
  itemResource = new DataTableResource(this.myOrders);

  constructor(private itemService: ItemService) { }

  ngOnInit() {
    this.itemService.getOrderStatuses().subscribe(
      (res) => {
        for(let i in res){
          this.statusArray[res[i]['id']]=res[i]['name'];
        }
      },
      (error) => {console.log(error);
      }
    );
  
    let orderObservable = this.itemService.getUserOrders();
    orderObservable.subscribe(
      (res) => {
        for(let i in res){
            this.myOrders.push(new myOrders(res[i]['dateOrder'],res[i]['sumPrice'],res[i]['orderItems'],this.statusArray[res[i]['status']]));
        }
        for(let order of this.myOrders){
            for(let id in order.orderItems){
              let orderItems= this.itemService.getItemsFromId(id);
              orderItems.subscribe(
                (res) => {
                  order.orderItems[id].name =res['name'];
                },
                (error) => {console.log(error);
              });
            }
        }
        this.itemResource.count().then(count => this.itemCount = count);
        this.reloadItems({});
      },
      (error) => {console.log(error);
      }
    );
  }

  ngAfterViewChecked(){

    let sidePanel = this.tableChild.nativeElement.querySelectorAll('.panel-default');
    if(sidePanel.length>0){
      sidePanel[0].style.backgroundColor='white';
    }

    let icons = this.tableChild.nativeElement.querySelectorAll('.glyphicon-refresh');
    if(icons.length>0){
      icons[0].style.backgroundColor='white';
    }
    if(!this.edited){
      $('.refresh-button').append('<i class="fa fa-refresh" aria-hidden="true"></i>');
      $('.column-selector-button').append('<i class="fa fa-list" aria-hidden="true"></i>');
      this.edited=true;
    }

    let icons2 = this.tableChild.nativeElement.querySelectorAll('.glyphicon-triangle-right');
    if(icons2.length>0){
      icons2[0].style.backgroundColor='white';
      if(!this.edited2){
        $('.row-expand-button').append('<i class="fa fa-caret-down" aria-hidden="true"></i>');
        this.edited2=true;
      }
    }
  }

  reloadItems(params) {
    this.itemResource.query(params).then(items => this.items = items);
  } 

  changePage(){
    this.pageAdmin.emit('viewPanel');
  }

}

export class myOrders{
  constructor (public date: Date, public sumPrice: number,  public orderItems:any, public status:string) { }
}