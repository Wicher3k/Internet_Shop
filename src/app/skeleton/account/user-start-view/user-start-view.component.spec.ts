import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserStartViewComponent } from './user-start-view.component';

describe('UserStartViewComponent', () => {
  let component: UserStartViewComponent;
  let fixture: ComponentFixture<UserStartViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserStartViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserStartViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
