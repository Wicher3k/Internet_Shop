import { Component, OnInit,Input, Output,EventEmitter } from '@angular/core';
import { CategoryserviceService } from './../../services/categoryservice.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  @Output() pageMain = new EventEmitter <string>();
  @Input() page: string;

  constructor(public categoryserviceService: CategoryserviceService) { }

  ngOnInit() {}

  categorySend(event: any){
    this.categoryserviceService.publishDataCategoryString(event.target.value);
  }

  changePage(page:string){
    this.pageMain.emit(page);
  }

}
