import { Component, OnInit,Input ,ViewChild,ElementRef,Output,EventEmitter} from '@angular/core';
import { CategoryserviceService } from './../../services/categoryservice.service';
import { BasketService } from './../../services/basket.service';
import { UserService } from './../../services/user.service';
import { ItemService } from './../../services/item.service';
import { NouisliderModule } from 'ng2-nouislider';
import { Item } from './../../skeleton/shop-center/item/itemClass';

declare var $ :any;

@Component({
  selector: 'app-filter-bar',
  templateUrl: './filter-bar.component.html',
  styleUrls: ['./filter-bar.component.css']
})
export class FilterBarComponent implements OnInit {
  @ViewChild('slider', { read: ElementRef }) slider: ElementRef;
  @Output() addOut = new EventEmitter <Item>();
  public someRange2;
  public filters=[]
  public categories=[]
  public categoriesGroups=[]
  private categoriesGroupsNames=[]
  public saleProduct=new Item();
  public someRange2config: any = {
    behaviour: 'drag',
    connect: true,
    start: [0, 2000],
    margin: 1,
    keyboard: true, 
    step: 1,
    range: {
      min: 0,
      max: 2000
    },
    tooltips:true,
  };
  
  constructor(private userService: UserService, private basketService: BasketService, private categoryserviceService: CategoryserviceService, private itemService:ItemService) { 
    this.itemService.sale$.subscribe(
      data => {
        this.saleProduct =data;
    });
  }

  onChange(event){
    this.categoryserviceService.publishFilterPrice(this.someRange2);
  }

  ngOnInit() {
    this.saleProduct = this.itemService.getPromotion();
    this.categories = this.categoryserviceService.categories;
    this.categoriesGroups = this.categoryserviceService.categoriesGroups;
    this.categoriesGroupsNames = this.categoryserviceService.categoriesGroupsNames;
  }

  ngAfterViewInit() {
    this.setColors();
  }

  add(itemObject:Item){
    itemObject.order+=1;
    this.addOut.emit(itemObject);
    this.basketService.publishData(itemObject.price,1);
    this.saveBasket();
  }

  saveBasket(){
    if (this.userService.ifUserLogged()){
      let basket = this.itemService.getBasket2Save();
      this.userService.saveBasket(basket);
    }
  }

  setColors(){
    const connect = this.slider.nativeElement.querySelectorAll('.noUi-connect');
    connect[0].classList.add('skyBackground');

    const connectTooltips = this.slider.nativeElement.querySelectorAll('.noUi-tooltip');
    for(let connectTooltip of connectTooltips){
      connectTooltip.classList.add('skyBorder');
    }
  }

  send(category:Array<string>){
    let index = this.filters.indexOf(category);
    if(index>-1){
      this.filters.splice(index, 1);
    }else{
      this.filters.push(category);
    }
    this.categoryserviceService.publishData(this.filters);
  }
}
