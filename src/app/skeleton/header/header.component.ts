import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BasketService } from './../../services/basket.service';
import { ItemService } from './../../services/item.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Output() pageMain = new EventEmitter <string>();
  public basketValue=0;
  public basketNumber=0;

  constructor(private itemService: ItemService, private basketService: BasketService) {
    this.basketService.caseNumberBasket$.subscribe(
      data => {
        if(data[0]=='reset'){
          this.basketValue=0;
          this.basketNumber=0;
        }else{
          this.getbasket();
        }
    });
    this.itemService.sale$.subscribe(
      data => {
        this.getbasket();
    });
    this.itemService.loadedbasket$.subscribe(
      data => {
        this.getbasket();
    });
  }

  getbasket(){
    let itemsBasket = this.itemService.getBasketService();
    this.basketNumber=0;
    this.basketValue=0;
    for(let item of itemsBasket){
      this.basketNumber+=item.order;
      this.basketValue+=item.order*item.price;
    }
  }

  ngOnInit() {
    this.getbasket()
  }

  changePage(page){
    this.pageMain.emit(page);
  }
}
