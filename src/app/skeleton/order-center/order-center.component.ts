import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { ItemService } from './../../services/item.service';
import { BasketService } from './../../services/basket.service';
import { Http,Response, Jsonp, RequestOptions } from "@angular/http";


@Component({
  selector: 'app-order-center',
  templateUrl: './order-center.component.html',
  styleUrls: ['./order-center.component.css']
})
export class OrderCenterComponent implements OnInit {
  @Output() pageMain = new EventEmitter <string>();
  public submitted = false;
  public model = new Model('','');
  public itemsBasket;
  public itemsOrder={};
  public itemsSum=0;
  constructor(private itemService: ItemService, private basketService: BasketService) {}

  ngOnInit() {
    this.getbasket();
  }

  onSubmit() { 
    this.submitted = true; 
    this.itemService.sendOrder(this.model,this.itemsSum,this.itemsOrder).subscribe(
      (res)=>{
          this.itemService.resetShopContent();
          this.basketService.publishData('reset');
      },
      (error)=>{
        console.log(error);
      }
    );
  }

  getbasket(){
    this.itemsBasket = this.itemService.getBasketService();
    for(let item of this.itemsBasket){
      this.itemsOrder[item.id.toString()]={};
      this.itemsOrder[item.id.toString()]['number']=item.order;
      this.itemsOrder[item.id.toString()]['price']=item.price;
      this.itemsSum+=item.order*item.price;
    }
  }

  backPage(){
    this.pageMain.emit('shop');
  }
    
}

export class Model{
  constructor (public name: string, public street: string){}
}


  
  