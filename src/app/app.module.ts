import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { HttpModule }   from '@angular/http';
import { NouisliderModule } from 'ng2-nouislider';
import { DataTableModule } from 'angular-4-data-table/src/index';
import { HttpClientModule } from '@angular/common/http';

import { SklepInternetowyMainComponent } from './sklepInternetowyMain.component';
import { HeaderComponent } from './skeleton/header/header.component';
import { FilterBarComponent } from './skeleton/filter-bar/filter-bar.component';
import { ShopCenterComponent } from './skeleton/shop-center/shop-center.component';
import { NavBarComponent } from './skeleton/nav-bar/nav-bar.component';
import { ItemComponent } from './skeleton/shop-center/item/item.component';
import { PaginationComponentComponent } from './skeleton/shop-center/pagination-component/pagination-component.component';

import { ItemService } from './services/item.service';
import { CategoryserviceService } from './services/categoryservice.service';
import { BasketService } from './services/basket.service';
import { UserService } from './services/user.service';


import { BasketCenterComponent } from './skeleton/basket-center/basket-center.component';
import { OrderCenterComponent } from './skeleton/order-center/order-center.component';
import { AccountComponent } from './skeleton/account/account.component';
import { AdminStartViewComponent } from './skeleton/account/admin-start-view/admin-start-view.component';
import { OrdersComponent } from './skeleton/account/admin-start-view/orders/orders.component';
import { ProductsComponent } from './skeleton/account/admin-start-view/products/products.component';
import { ProductsEditComponent } from './skeleton/account/admin-start-view/products/products-edit/products-edit.component';
import { AccountRegisterComponent } from './skeleton/account/account-register/account-register.component';
import { UserStartViewComponent } from './skeleton/account/user-start-view/user-start-view.component';
import { UserOrdersComponent } from './skeleton/account/user-start-view/user-orders/user-orders.component';


@NgModule({
  declarations: [
    SklepInternetowyMainComponent,
    HeaderComponent,
    FilterBarComponent,
    ShopCenterComponent,
    NavBarComponent,
    ItemComponent,
    PaginationComponentComponent,
    BasketCenterComponent,
    OrderCenterComponent,
    AccountComponent,
    AdminStartViewComponent,
    OrdersComponent,
    ProductsComponent,
    ProductsEditComponent,
    AccountRegisterComponent,
    UserStartViewComponent,
    UserOrdersComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NouisliderModule,
    DataTableModule,
    HttpClientModule
  ],
  providers: [ItemService, CategoryserviceService,BasketService,UserService],
  bootstrap: [SklepInternetowyMainComponent]
})
export class AppModule { }
