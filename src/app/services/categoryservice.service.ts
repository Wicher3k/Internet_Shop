import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CategoryserviceService {

  public caseNumber = new Subject<any>();
  public caseFilterString = new Subject<any>();
  public caseFilterPrice = new Subject<any>();
  
  caseNumber$ = this.caseNumber.asObservable();
  caseFilterString$ = this.caseFilterString.asObservable();
  caseFilterPrice$ = this.caseFilterPrice.asObservable();

  categories=[];
  categoriesGroups=[];
  categoriesGroupsNames=[];

  baseUrlCategories="/categories";

  constructor(private http: HttpClient) {
    this.getCategories();
  }

  publishData(data: any) {
    this.caseNumber.next(data);
  }

  publishDataCategoryString(data: any) {
    this.caseFilterString.next(data);
  }

  publishFilterPrice(data: any){
    this.caseFilterPrice.next(data);
  }

  getCategories(){
    this.http.get(this.baseUrlCategories,{withCredentials: true}).subscribe(
      (res)=>{
        for(let i in res){
          if(res[i]['group']==undefined){
            this.categories.push(res[i]['name']);
          }else{
            let index = this.categoriesGroupsNames.indexOf(res[i]['group']);
            if(index>-1){
              this.categoriesGroups[index].push(res[i]['name'])
            }else{
              this.categoriesGroupsNames.push(res[i]['group'])
              this.categoriesGroups.push([res[i]['name']])
            }
          }
        }
          
      },
      (error)=>{
        console.log(error);
      }
    );
  }

}
