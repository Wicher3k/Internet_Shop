import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';

@Injectable()
export class BasketService {

  public caseNumberBasket = new Subject<any>();
  caseNumberBasket$ = this.caseNumberBasket.asObservable();

  constructor() {}
  
  publishData(data: any, option: number = 0) {
    this.caseNumberBasket.next([data,option]);
  }
}
