import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Injectable()
export class UserService {
  private userLogged = new ModelAuthenticate()
  private token="";

  private baseUrlUser='/users';  
  private baseUrlAuthentication= '/users/login';
  private baseUrlLogout= '/users/logout';  

  constructor( private http: HttpClient) {}

  setUser(model,functionAccount,token,userId){
    this.userLogged=model;
    this.userLogged.userId=userId;
    this.userLogged.functionAccount=functionAccount;
    this.token=token;
  }

  register(userModel){
    return this.http.post(this.baseUrlUser,
      {"username":userModel.name,"password":userModel.password,'function':1}
      ,{withCredentials: true}
      );
  }

  authenticate(userModel){
    return this.http.post(this.baseUrlAuthentication,
      {"username":userModel.username,"password":userModel.password}
      ,{withCredentials: true}
      );
  }

  logout(){
    this.token='';
    this.userLogged=new ModelAuthenticate();
    return this.http.post(this.baseUrlLogout,''
      ,{withCredentials: true}
      );
  }

  getHeader(){
    var token = this.token;
    return new HttpHeaders ({'x-access-token': token});
  }

  getToken(){
    return this.token;
  }

  getUser(){
    return this.userLogged;
  }

  getUserLoggedId(){
    return this.userLogged.userId;
  }

  ifUserLogged(){ 
    return this.userLogged.userId!='';
  }

  saveBasket(basket){
    let myHeaders = this.getHeader();
    
    this.http.put(this.baseUrlUser+'/basket/'+this.userLogged.userId,
    {"basketSaved":basket,
    },
    {headers: myHeaders, withCredentials: true }).subscribe(
      (res) => {}
    );
}

}

export class ModelAuthenticate{
  constructor (public userId:string='', public username: string='', private password: string='', public functionAccount: Number = 0){}
}