import { Injectable } from '@angular/core';
import { Item } from './../skeleton/shop-center/item/itemClass';
import { UserService } from './user.service';


import { Http,Response, Jsonp, RequestOptions,Headers } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { Subject }    from 'rxjs/Subject';
import 'rxjs/add/operator/map';
declare var io :any;

import { HttpClient,HttpHeaders } from '@angular/common/http';
@Injectable()
export class ItemService {

  public promocjaProduct=new Item();
  public salesIdArray=[];
  public items=[];
  public socket;
  private itemsReturn=[];
  private countItems=0;
  private baseUrlProducts = '/products';  
  private baseUrlOrders = '/orders';
  private baseUrlAuthentication= '/users/login';  
  private baseUrlOrderStatuses= '/orderstatuses';  

  public endInitialization = new Subject<any>();
  endInitialization$ = this.endInitialization.asObservable();

  public sale = new Subject<any>();
  sale$ = this.sale.asObservable();

  public loadedbasket = new Subject<any>();
  loadedbasket$ = this.loadedbasket.asObservable();


  constructor(private http: Http, private http2: HttpClient, private userService:UserService) { 
    //this.socket = io.connect('http://localhost:3000' );
    this.socket = io.connect('https://peaceful-ocean-60282.herokuapp.com' );
    this.getProductJsonAndSetItems(true);
    this.anotherSockets();
  };
  

  getProductJsonAndSetItems(connectServer=false){
    
    this.items=[];
    this.http.get(this.baseUrlProducts).map(res => res.json()).subscribe(
      (res) => {
          for(let row of res){
            this.items.push(new Item(row['_id'],row['name'],row['description'],row['price'],row['category'],0,row['imageExist'],false));
          }
          this.endInitialization.next();

          if(connectServer=true){
            var klasa = this;

            this.socket.on('sales' , function (data) { 
              for (let k in data.promocja){  
                for(let i in klasa.items){
                    if(data.promocja[k]==klasa.items[i].id){
                      let j = parseInt(i);
                      klasa.items[j].priceOld=klasa.items[j].price;
                      klasa.items[j].price = klasa.items[j].price*(1-data.saleValue[k]);
                      klasa.items[j].sale = true;
                      klasa.promocjaProduct=klasa.items[j];

                      klasa.salesIdArray.push(data.promocja[k]);
                      break;
                    }
                }
              }
              klasa.sale.next(klasa.promocjaProduct);
            });

            this.socket.on('salesEnd' , function (data) {

              for(let i in klasa.items){
                if(data.promocja==klasa.items[i].id){
                  let j = parseInt(i);
                  klasa.items[j].sale=false;
                  klasa.items[j].price =klasa.items[j].priceOld;
                  break;
                }
              }

              
              let index = klasa.salesIdArray.indexOf(data.promocja)
              if (index > -1) {
                klasa.salesIdArray.splice(index, 1);
              }
              if(klasa.salesIdArray.length<1){
                klasa.promocjaProduct=new Item();
              }else{
                klasa.promocjaProduct = klasa.searchProductByid(klasa.salesIdArray[klasa.salesIdArray.length-1]);
              }
              klasa.sale.next(klasa.promocjaProduct);

            });

          }
      },
      (error) => {
        this.endInitialization.next("error : " + error);
      }
    );
  }

  searchProductByid(idProduct){
    for(let i in this.items ){
      if(this.items[i].id==idProduct){
        return this.items[i];
      }
    }
    return new Item();
  }

  ifProductSale(idProduct){
    let index = this.salesIdArray.indexOf(idProduct)
    if (index > -1) {
      return true;
    }else{
      return false;
    }
  }

  getItemsService(option: any){
    let itemsfiltred = this.filterItems(option['filter'],option['filterRe'],option['filterPrice']);
    this.itemsReturn=[];
    for(let i=0;i<option['limit'];i++){
      if(i+(option['page']-1)*option['limit']<this.countItems)
        this.itemsReturn.push(itemsfiltred[i+(option['page']-1)*option['limit']]);
    }
    return {itemsObjects: this.itemsReturn, total: this.countItems};
  }

  getBasketService(){
    let itemsReturn=[];
    for(let item of this.items){
      if(item.order>0){
        itemsReturn.push(item);
      }
    }
    return itemsReturn;
  }
  
  getBasket2Save(){
    let itemsReturn={};
    for(let item of this.items){
      if(item.order>0){
        itemsReturn[item.id]=item.order;
      }
    }
    return itemsReturn;
  }

  filterItems(filtersCategoryChecked:Array<string>,filterRe:string,filterPrice:Array<number>){
    this.countItems=0;
    let itemsfiltred=[],itemsfiltredReturn=[];
    if(filtersCategoryChecked.length>0){
      for(let filter of filtersCategoryChecked){
          for(let item of this.items){
            if(filtersCategoryChecked.length>0 && item.category == filter && itemsfiltred.indexOf(item)<0){
                itemsfiltred.push(item);
            }
          }
      }
    }else{
      itemsfiltred=this.items;
    }
    if(filterRe.length>0){
      let regexp = new RegExp('.*'+filterRe.toLowerCase()+'.*');
      for(let item of itemsfiltred){
        if(regexp.test(item.name.toLowerCase())) {
          itemsfiltredReturn.push(item);
        }
      }
    }else{
      itemsfiltredReturn=itemsfiltred;
    }

    itemsfiltred=itemsfiltredReturn;
    itemsfiltredReturn=[];
    for(let item of itemsfiltred){
      if(item.price>=filterPrice[0] && item.price<=filterPrice[1] ) {
        itemsfiltredReturn.push(item);
      }
    }

    this.countItems=itemsfiltredReturn.length;
    return itemsfiltredReturn;
  }
  
  getPromotion(){
    return this.promocjaProduct;
  }

  anotherSockets(){
    var klasa = this;
    this.socket.on('deleteProduct' , function (idProd) {
      klasa.deleteProductView(idProd);
      klasa.reloadSiteHeader();
      klasa.endInitialization.next();
    });
  }


  resetShopContent(){
    this.getProductJsonAndSetItems();
  }

  setBasket(basket){
    for(let id in basket){
      for(let item of this.items){
        if (id == item.id){
          item.order=basket[id];
          break;
        } 
      }
    }
    this.loadedbasket.next();
  }
  
  reloadSiteHeader(){
    this.loadedbasket.next();
  }

  getHeader(){
    var token = this.userService.getToken();
    return new HttpHeaders ({'x-access-token': token});
  }

  deleteProductView(idProd){
    for(let i in this.items){
      if(this.items[i].id==idProd.massage){
        this.items.splice(parseInt(i), 1);
        break;
      }
    }
  }

  //CRUD
  
  //orders

  getOrders(){
    let myHeaders = this.getHeader();
    return this.http2.get(this.baseUrlOrders,{headers: myHeaders, withCredentials: true});
  }
  getUserOrders(){
    let myHeaders = this.getHeader();
    var userLoggedId = this.userService.getUserLoggedId();
    return this.http2.get(this.baseUrlOrders+'/user/'+userLoggedId,{headers: myHeaders, withCredentials: true});
  }

  sendOrder(model,itemsSum,itemsOrder){
    var userLoggedId = this.userService.getUserLoggedId();
    var d = new Date,
    dformat = [d.getMonth()+1,
               d.getDate(),
               d.getFullYear()].join('/')+' '+
              [d.getHours(),
               d.getMinutes(),
               d.getSeconds()].join(':');
    return this.http2.post(this.baseUrlOrders,
      {"name":model.name,"street":model.street,"sumPrice":itemsSum,"orderItems":itemsOrder,"userId":userLoggedId,"dateOrder":dformat,"status":0},
      { withCredentials: true }
    );
  }

  putOrder(id:string,statusId:Number){
    let myHeaders = this.getHeader();
    return this.http2.put(this.baseUrlOrders+'/'+id,
      {"status":statusId,
      },
      {headers: myHeaders, withCredentials: true });
  }

  getOrderStatuses(){
    let myHeaders = this.getHeader();
    return this.http2.get(this.baseUrlOrderStatuses,{headers: myHeaders, withCredentials: true});
  }

  // products

  getProducts(){
    return this.http.get(this.baseUrlProducts).map(res => res.json());
  }

  deleteProduct(id:string){
    let myHeaders = this.getHeader();
    return this.http2.delete(this.baseUrlProducts + '/'+id,{headers: myHeaders, withCredentials: true});
  }

  addOrEditProduct(product,option,image){
    if(option == 'Dodaj'){
      return this.addProduct(product,image);
    }else if(option == 'Edytuj'){
      return this.editProduct(product,image);
    }
  }
  
  addProduct(product,image){
    if(image!=undefined){
      var imageExist = true;
    }
    let myHeaders = this.getHeader();
    myHeaders.append('Content-Type', 'multipart/form-data');
    return this.http2.post(this.baseUrlProducts,
      {"name":product.name,
      "description":product.description,
      "price":product.price,
      "category":product.category,
      "imageExist":imageExist,
      "image":image,
      "sale":product.sale,
      "optionPromotion":{saleTime:product.saleTime,saleValue:product.saleValue}},
      {headers: myHeaders, withCredentials: true });
  }

  editProduct(product,image){
    if(image!=undefined){
      var imageExist = true;
    }
    let myHeaders = this.getHeader();
    return this.http2.put(this.baseUrlProducts+'/'+product.id,
      {"name":product.name,
      "description":product.description,
      "price":product.price,
      "category":product.category,
      "imageExist":imageExist,
      "image":image,
      "sale":product.sale,
      "optionPromotion":{saleTime:product.saleTime,saleValue:product.saleValue}},
      {headers: myHeaders, withCredentials: true });
  }

  getPhoto(idProduct){
    let myHeaders = this.getHeader();
    return this.http2.get(this.baseUrlProducts+'/photo/'+idProduct,{headers: myHeaders, withCredentials: true});
  }

  getItemsFromId(id:string){
      let myHeaders = this.getHeader();
      return this.http2.get(this.baseUrlProducts+'/'+id,{headers: myHeaders,withCredentials: true})
  }

}

export class UserAuthenticate{
  constructor (public username: string, public password: string){}
}
