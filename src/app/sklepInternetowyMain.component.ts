import { Component } from '@angular/core';



@Component({
  selector: 'main',
  templateUrl: './sklepInternetowyMain.component.html',
  styleUrls: ['./sklepInternetowyMain.component.css']
})
export class SklepInternetowyMainComponent {
  pageMain = 'shop';
  //pageMain = 'ordering';
  //pageMain = 'basket';
  //pageMain = 'admin';
  constructor(){
    
  }
  changePage(page){
    this.pageMain=page;
  }
}
